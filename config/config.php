<?php
return new \Phalcon\Config(array(
    'database' => array(
        // 'host'        => 'localhost',
        // 'dbname'      => 'house_database',
        'host'        => '127.0.0.1',
        'dbname'      => 'house_database',
        'username'    => 'sanslave',
        'password'    => 'omge0954',
        'adapter'     => 'Mysql',
        'charset'     => 'utf8',
    ),
    'personalwork' => array(
        'projectname' => '好多房'
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../app/controllers/',
        'modelsDir'      => __DIR__ . '/../app/models/',
        'formsDir'       => __DIR__ . '/../app/forms/',
        'pluginsDir'     => __DIR__ . '/../app/plugins/',
        'viewsDir'       => __DIR__ . '/../app/views/',
        'libraryDir'     => __DIR__ . '/../app/library/',
        'cacheDir'       => __DIR__ . '/../data/cache/',
        'baseUri'        => '/',
    ),
    'misc' => array(
        'headerStyle' => array(
                '/misc/css/reset.css',
                '/misc/css/style.css',
                '/misc/css/headfoot.css',
                // '/misc/css/swiper.min.css',
                '/misc/css/combo.select.css',
                '/misc/css/pk-style.css',
            ),
        'headerScript' => array(
                '/misc/js/jquery-latest.min.js',
                '/misc/js/jquery.popupoverlay.js',
                '/misc/js/swiper.min.js',
                '/misc/js/jquery.combo.select.js',
                '/misc/js/tab.js',
                '/misc/bower_component/browser-detection/src/browser-detection.js',
            ),
        'footerScript' => array(
                '/misc/js/setting.js',
                '/misc/js/form-elms.js',
                '/misc/js/pkglobal.js',
            ),
    ),
    'google' => array(
        'account'=> 'san.personalwork@gmail.com',
        'apikey_js' => 'AIzaSyDzWTypEByd5b1sd5J8Wbqg_SL6JEAnMM4',
        'maps_geocoding' => 'https://maps.googleapis.com/maps/api/geocode/'
    ),
    'auth' => array(
        'facebook'  => array(
                            "app_id" => "1347796121906021",
                            "app_secret" => "7548bb2c73e2ac0a16af9e6297b10e8b",
                            "default_graph_version" => "v2.7",
                            "redirect_url" => (!empty($_SERVER['HTTP_HOST']))?'http://'.$_SERVER['HTTP_HOST'].'/auth/facebook' : null
                        ),
        'line'      => array(
                            "Channel_ID" => "1482538055",
                            "Channel_secret" => "8ab9d903701d52289f221772ad1d4f17",
                            "Callback_URL" => (!empty($_SERVER['HTTP_HOST']))?'http://'.$_SERVER['HTTP_HOST'].'/auth/line' : null,
                            "AUTH_URL" => "https://access.line.me/dialog/oauth/weblogin",
                            "Endpoint_URL" => "https://api.line.me/v1/oauth/accessToken"
                        ),
        /* @see https://github.com/yahoo/yos-social-php5 */
        'yahoo'     => array(
                            "OAUTH_CONSUMER_KEY" => "dj0yJmk9TjdiNUc0bjhDdDRmJmQ9WVdrOVVrMHlhRzExTlRBbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0xMw--",
                            "OAUTH_CONSUMER_SECRET" => "754c480546d220ae38b3c6fd9285caa4b1293d87",
                            "OAUTH_DOMAIN" => "housefront.personalwork.tw",
                            "OAUTH_APP_ID" => "Housenrich.Dev",
                            "OAUTH_CALLBACK_URL" => (!empty($_SERVER['HTTP_HOST']))?'http://'.$_SERVER['HTTP_HOST'].'/auth/yahoo' : null,
                        ),
    )
));
