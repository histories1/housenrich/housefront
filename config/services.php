<?php
/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config $config
 */

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;

$di = new FactoryDefault();

/**
 * Registering a global config
 */
$di['config'] = include PPS_APP_APPSPATH . "/../config/config.php";

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader
->registerNamespaces(
    array(
        'Housefront\Controllers' => $di->get('config')->application->controllersDir,
        'Houserich\Models'       => $di->get('config')->application->modelsDir,
        'Housefront\Plugins'     => $di->get('config')->application->pluginsDir,
        'Housefront\Forms'       => $di->get('config')->application->formsDir,
        'Housefront\Forms\Decorators'=> $di->get('config')->application->formsDir.'decorators/',
        'Housefront\Forms\Elements'=> $di->get('config')->application->formsDir.'elements/',
        'OpenID'                 => $di->get('config')->application->libraryDir."/OpenID/",
        'OpenSocial'             => $di->get('config')->application->libraryDir."/OpenSocial/",
        'Yahoo'                  => $di->get('config')->application->libraryDir."/Yahoo/"
    )
)
->registerDirs(
    array(
        $di->get('config')->application->libraryDir."/OAuth/",
    )
)
->register();

if (!file_exists(PPS_APP_APPSPATH . '/../vendor/autoload.php')) {
    echo "The 'vendor' folder is missing. You must run 'composer update' to resolve application dependencies.\nPlease see the README for more information.\n";
    exit(1);
}
require_once PPS_APP_APPSPATH . '/../vendor/autoload.php';

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $url = new UrlResolver();
    $url->setBaseUri($this->get('config')->application->baseUri);
    return $url;
});


$di->setShared('router', function () {
    // $router = new \Phalcon\Mvc\Router(FALSE);
    $router = new \Phalcon\Mvc\Router\Annotations(false);
    $router->setUriSource(\Phalcon\Mvc\Router::URI_SOURCE_SERVER_REQUEST_URI);

    $router
        ->addResource("Housefront\Controllers\House\Default", "/demo")
        ->addResource("Housefront\Controllers\Index", "/")
        // 註冊登入登出
        ->addResource("Housefront\Controllers\Member\Auth", "/auth")
        // 會員中心
        ->addResource("Housefront\Controllers\Member\Profile", "/member")
        // 會員中心-帳戶管理
        ->addResource("Housefront\Controllers\Member\Account", "/member/account")
        // 會員中心-收藏管理
        ->addResource("Housefront\Controllers\Member\Favorite", "/member/favorite")
        // 會員中心-刊登管理
        ->addResource("Housefront\Controllers\Member\Post", "/member/post")
        // 會員中心-看屋管理
        ->addResource("Housefront\Controllers\Member\Buy", "/member/buy")
        // 會員中心-通知中心
        ->addResource("Housefront\Controllers\Member\Notification", "/member/notification")
        // 會員中心-通知中心（降價通知）
        ->addResource("Housefront\Controllers\Member\Notification", "/member/reduction")
        // 會員中心-議價管理
        ->addResource("Housefront\Controllers\Member\Negotiation", "/member/negotiation")
        // 會員中心-找房條件
        ->addResource("Housefront\Controllers\Member\Findhouse", "/member/findhouse")
        // 會員中心-看屋管理
        ->addResource("Housefront\Controllers\Member\Viewhouse", "/member/viewhouse")
        // 好顧問幫幫我
        ->addResource("Housefront\Controllers\Member\Service", "/service")
        // 買屋
        // ->addResource("Housefront\Controllers\House\Buy", "/search")
        // 買屋2
        ->addResource("Housefront\Controllers\House\Map", "/map")
        // 預約看屋
        ->addResource("Housefront\Controllers\House\Reserve", "/reserve")
        // 我要賣房
        ->addResource("Housefront\Controllers\House\Sale", "/saler")
        // 房市新聞
        ->addResource("Housefront\Controllers\Index", "/news")
        // 行情(地圖)
        ->addResource("Housefront\Controllers\Price\Map", "/caseprice")
        // 估價
        ->addResource("Housefront\Controllers\Assess\Default", "/assess")
        // 媒體檔案存取處理
        ->addResource("Housefront\Controllers\Index", "/file/access")
        ;

    $router
        ->notFound(
            [
                "namespace"  => "Housefront\Controllers",
                "controller" => "index",
                "action"     => "notfound"
            ]
        )
        ->removeExtraSlashes(true);
    return $router;
});

/**
 * 附加AnnotationsToVolt plugin
 */
$di->setShared('dispatcher', function() {
    $eventsManager = $this->getShared('eventsManager');
    $dispatcher = new Phalcon\Mvc\Dispatcher();

    $eventsManager->attach("dispatch", new Housefront\Plugins\AnnotationsLoadAssets($di) );
    $eventsManager->attach("dispatch", new Housefront\Plugins\AnnotationsToVolt($di) );

    $dispatcher->setEventsManager($eventsManager);
    return $dispatcher;
});


/**
 * Register custom filter
 * @see https://forum.phalconphp.com/discussion/9693/how-to-add-a-custom-filter-to-a-form
 * */
$di->setShared('filter', function() {

  $filter = new \Phalcon\Filter();

  $filter->add('emptytonull', new \Personalwork\Filter\Emptytonull());
  $filter->add('emptytozero', new \Personalwork\Filter\Emptytozero());
  $filter->add('zerotonull', new \Personalwork\Filter\Zerotonull());

  return $filter;

});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {

    $view = new View();

    $view->setViewsDir($this->get('config')->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $this->get('config')->application->cacheDir,
                'compiledSeparator' => '_',
                'stat' => true,
                'compileAlways' => true
            ));

            $compiler = $volt->getCompiler();
            //This binds the function name 'noformat' in Volt to the PHP function 'round'
            $compiler->addFunction('numformat', 'round');

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $dbConfig = $this->get('config')->database->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

$di->set(
    "dbCrawler",
    function () {
        $dbConfig = $this->get('config')->database->toArray();
        $adapter = $dbConfig['adapter'];
        unset($dbConfig['adapter']);
        $dbConfig['dbname'] = "crawler_house";
        return new Phalcon\Db\Adapter\Pdo\Mysql($dbConfig);
    }
);

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flashSession', function () {
    return new \Phalcon\Flash\Session(array(
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ));
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});


/**
 * Phalcon cookie
 * 若使用加密的cookie必須先設定global encrypt key
 * get()
 *
 * Sets a cookie to be sent at the end of the request This method overrides any cookie set before with the same name
 * set (mixed $name, [mixed $value], [mixed $expire], [mixed $path], [mixed $secure], [mixed $domain], [mixed $httpOnly])
 *
 * has()
 * delete()
 * send()
 * reset()
 */
$di->set( "crypt", function () {
    $crypt = new \Phalcon\Crypt();
    $crypt->setKey('@$d3gsfq21A341'); // Use your own key!
    return $crypt;
});
$di->setShared('cookies', function () {
    $cookies = new \Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(true);
    return $cookies;
});