<div id="gmap" style="position:relative;width:100%;"></div>

<script type="text/javascript">
function _initMap() {
    // 配置高度
    var height = window.innerHeight ||
                 document.documentElement.clientHeight ||
                 document.body.clientHeight;
    var footerHt = 0;

    topHeight=$("#pageHeader").height()+$("#funcBar").height();

    mapHt = parseInt(height-topHeight);
    //預先配置地圖顯示高度
    $("#gmap").css({'height':mapHt,'top':topHeight});

    gMapObj = new google.maps.Map(
      document.getElementById('gmap'),
      {
        center: new google.maps.LatLng(25.04674, 121.54168),
        zoom: 13,
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );

    $.getJSON('http://housefront.pro.local/demo' , function( json ){
        $.each(json, function(inx, elm){
            // console.log(elm[0]);
            var latLng = new google.maps.LatLng(elm[0] , elm[1]);
            var marker = new google.maps.Marker({ position: latLng });
            GMAP_MARKERS.push(marker);
        });
        // 經緯度取法：GMAP_MARKERS[].getPosition().lat() or .lng()
        var markerCluster = new MarkerClusterer(gMapObj, GMAP_MARKERS,{ imagePath: '/misc/images/markers/m' });
    });

    // gMapObj.data.loadGeoJson('http://housefront.pro.local/demo' , null, function (locations) {
    //     var markers = locations.map(function (house, i) {
    //         console.log(i+': ');
    //         console.log(house.getGeometry().get());
    //         return ;
    //         // var latLng = new google.maps.LatLng(mapval.lat , mapval.lng);
    //         // var marker = new google.maps.Marker({ 'position': g.get(0) });
    //         var latLng = house.getGeometry().get(0);
    //         var marker = new google.maps.Marker({ position: latLng });
    //         // 經緯度取法：GMAP_MARKERS[].getPosition().lat() or .lng()
    //         GMAP_MARKERS.push(marker);
    //         return marker;
    //     });
    //     // console.log( GMAP_MARKERS );
    //     var markerCluster = new MarkerClusterer(gMapObj, GMAP_MARKERS,{ imagePath: '/misc/images/markers/m' });
    // });

    // act2. Googlemap項目更新處理！
    // _refreshMarkers();

}

/**
 * 處理載入地圖座標點
 * 1. 更新GMAP_MARKERS參數
 * 2. 如何標示點位！
function _refreshMarkers() {
    // Create an array of alphabetical characters used to label the markers.
    // var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    GMAP_MARKERS = new Array();
    $(".infinite .box:visible").each(function(){
        mapval = {lat: parseFloat($(this).find("addr").data('lat')), lng: parseFloat($(this).find("addr").data('lng'))};
        // console.log(mapval);
        var latLng = new google.maps.LatLng(mapval.lat , mapval.lng);
        var marker = new google.maps.Marker({ position: latLng });
        // markers.push(marker);
        // 經緯度取法：GMAP_MARKERS[].getPosition().lat() or .lng()
        GMAP_MARKERS.push(marker);
    });
    // Add a marker clusterer to manage the markers.
    var markerCluster = new MarkerClusterer(gMapObj, GMAP_MARKERS, {imagePath: '/misc/images/markers/m'});
}
 * */

// 預設地圖參數
var GMAP_DF = {
    CNT_ADDR : '台灣',
    DROOM : 8
};
// 地圖座標點變數
var GMAP_MARKERS = new Array();

var GVAR = {
    // 排序(目前排序設定)
    SORT : {
        sort : '{{ this.session.MAPSEARCH['ACTIVE']['SORT'] }}' ,
        sortmode : '{{ this.session.MAPSEARCH['ACTIVE']['SORTMODE'] }}'
    },
    // 頁籤(all new down)
    TAB : {
        filter : '{{ this.session.MAPSEARCH['ACTIVE']['TAB'] }}'
    },
    // 搜尋表單
    POSTDATA : null,
    // 目前項目個數
    LIST_ITEMS : 0
}

var gMapPanorama;
var streetViewService;

$(function(){
    geocoder = new google.maps.Geocoder();

    if( $("#elmCity").val() != '縣市' ){
        GMAP_DF.CNT_ADDR=$("#elmCity").val();
        GMAP_DF.DROOM=14
    }

    latLng = _map_center_withaddr(
                GMAP_DF.CNT_ADDR ,
                GMAP_DF.DROOM,
                function(GEO, address) {
                    // _map_add_marker(GEO, 1);
                    // _map_update_latlng(GEO);
                }
            );

});
</script>