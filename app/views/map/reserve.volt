{% if this.session.has('USER') %}

<div id="reserveContrl" class="reserve-btn"
    {% if this.session.get('USER')['RESERVES']['items'] == 0 %}
    style="display:none;"
    {% endif %}
>您準備預約的案子
<span id="reserveItems">{{ this.session.get('USER')['RESERVES']['items'] }}</span>
</div>

<div class="reserve-wrap"> {{ this.session.get('USER')['RESERVES']['html'] }} </div>

<a href="{{ url('/reserve/1') }}" class="btn-block btn-blue accept">確定預約</a>

{% endif %}
