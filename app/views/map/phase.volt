<div id="funcBar" class="map-mode">
    {% include "map/search.volt" %}
</div>

<div id="gmap" style="position:relative;width:100%;"></div>

<a href="javascript:" id="hide-btn"><span></span></a>
<div class="side-panel">
    {% include "map/list.volt" %}
</div>

<aside id="mapStreetView" style="display:none;"></aside>

<!--預約區塊-->
<div class="reserve-panel">
    {% include "map/reserve.volt" %}
</div>

<script type="text/javascript">
/**
 * 1. 排序(總價/坪數/屋齡)
 * 2. 物件集合頁籤(全部/新上架/有降價)
 * 3. (表單搜尋操作)
 * */
function phase_refreshLists(postdata, successFunc, AJAX_URL ) {
    //act1. ajax update session
    $.ajax({
        url : AJAX_URL ,
        method : 'POST',
        data : postdata,
        dataType: 'html',
        // 功能不能點！/區塊遮罩與loading...
        beforeSend: function(){
        },
        //  (設定目前顯示頁籤)
        success: function(resp){
            successFunc();
        }
    });
}

/**
 * 處理載入地圖座標點
 * 1. 更新GMAP_MARKERS參數
 * 2. 如何標示點位！
 * */
function phase_refreshMarkers() {
    // Create an array of alphabetical characters used to label the markers.
    GMAP_MARKERS = new Array();
    $(".infinite .box:visible").each(function(){
        mapval = {
            lat: parseFloat($(this).find("addr").data('lat')),
            lng: parseFloat($(this).find("addr").data('lng'))
        };
        // console.log(mapval);
        var latLng = new google.maps.LatLng(mapval.lat , mapval.lng);
        var marker = new google.maps.Marker({ position: latLng });
        // markers.push(marker);
        // 經緯度取法：GMAP_MARKERS[].getPosition().lat() or .lng()
        GMAP_MARKERS.push(marker);
    });
    // Add a marker clusterer to manage the markers.
    var markerCluster = new MarkerClusterer(
                                gMapObj,
                                GMAP_MARKERS,
                                {
                                    imagePath : '/misc/images/markers/m'
                                }
                            );
}


// 設定街景區塊內顯示的物件
function phase_setStreetView( mark_index ) {
    if( !parseInt(mark_index) ){
        return false;
    }

    var streetViewService = new google.maps.StreetViewService();
    var STREETVIEW_MAX_DISTANCE = 100;

    pos=GMAP_MARKERS[mark_index].getPosition();
    // console.log( { lat: pos.lat(), lng: pos.lng() } );

    streetViewService.getPanoramaByLocation( pos , STREETVIEW_MAX_DISTANCE, function (streetViewPanoramaData, status) {
        if (status === google.maps.StreetViewStatus.OK) {
            gMapPanorama.setPosition( { lat: pos.lat(), lng: pos.lng() } );
            return ;
        }else{
            console.log('無法載入該筆街景！');
        }
    });
}

function _initMap() {
    // 配置高度
    var height = window.innerHeight ||
                 document.documentElement.clientHeight ||
                 document.body.clientHeight;
    var footerHt = 0;

    topHeight=$("#pageHeader").height()+$("#funcBar").height();

    mapHt = parseInt(height-topHeight);
    //預先配置地圖顯示高度
    $("#gmap").css({'height':mapHt,'top':topHeight});
    // 調整左側區塊樣式(高度與position位置！)並套用mCustomScrollbar
    $(".side-panel").css('height', mapHt).mCustomScrollbar();
    // 調整街景出現位置！
    $("#mapStreetView").css({right: $(".side-panel").width()+20 });

    gMapObj = new google.maps.Map(
      document.getElementById('gmap'),
      {
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );
    gMapPanorama = new google.maps.Map(
      document.getElementById('mapStreetView'),{
        zoom: 14,
        center: new google.maps.LatLng(0.0, 0.0),
        zoomControl: true,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      }
    );
    streetViewService = new google.maps.StreetViewService();

    // act2. Googlemap項目更新處理！
    // Load GeoJSON.
    // URL = 'https://gist.githubusercontent.com/vgrem/4d6ff14d50f408e864f1ee4614653c1c/raw/schools.geojson';
    URL = '{{ url('/map/loadgeojson' )}}';
    // var geojson = JSON.parse( URL );
    // console.log( geojson );
    // return ;
    gMapObj.data.loadGeoJson( URL , null, function (features) {
        GMAP_MARKERS = features.map(function (feature) {
        var g = feature.getGeometry();
        // console.log( g instanceof google.maps.Data.Point );
        // console.log( g.get().lat() );
        // console.log( g.get().lng() );
        var marker = new google.maps.Marker({ 'position': g.get() });
            return marker;
        });
        var markerCluster = new MarkerClusterer(
                                    gMapObj,
                                    GMAP_MARKERS,
                                    {
                                        imagePath : '/misc/images/markers/m'
                                    }
                                );
    });
    // phase_refreshMarkers();


    // 判斷結束街景模式
    google.maps.event.addListener(gMapObj.getStreetView(),'visible_changed',function(){
        // console.log( $(gMapPanorama.getDiv()).is(':visible') );
        if( !this.getVisible() && $(gMapPanorama.getDiv()).is(':visible') ){
            $(gMapPanorama.getDiv()).hide();
            // alert('streetview is ' +(this.getVisible()?'open':'closed'));
        }
    });

    // pos=GMAP_MARKERS[0].getPosition();
    // streetViewService.getPanoramaByLocation( pos , 100, function (data, status) {
    //     if (status === google.maps.StreetViewStatus.OK) {
    //         gMapPanorama.setPano(data.location.pano);
    //         // gMapPanorama.setPov({
    //         //   heading: 270,
    //         //   pitch: 0
    //         // });
    //         gMapPanorama.setVisible(true);
    //     }
    // });

}

// 預設地圖參數
var GMAP_DF = {
    CNT_ADDR : '台北市',
    DROOM : 12
};
// 地圖座標點變數
var GMAP_MARKERS = new Array();

var GVAR = {
    // 排序(目前排序設定)
    SORT : {
        sort : '{{ this.session.MAPSEARCH['ACTIVE']['SORT'] }}' ,
        sortmode : '{{ this.session.MAPSEARCH['ACTIVE']['SORTMODE'] }}'
    },
    // 頁籤(all new down)
    TAB : {
        filter : '{{ this.session.MAPSEARCH['ACTIVE']['TAB'] }}'
    },
    // 搜尋表單
    POSTDATA : null,
    // 目前項目個數
    LIST_ITEMS : 0
}

var gMapPanorama;
var streetViewService;

$(function(){
    geocoder = new google.maps.Geocoder();

    if( typeof $(":radio[name='city']:checked").val() !== "undefined" ){
        GMAP_DF.CNT_ADDR=$(":radio[name='city']:checked").val();
        GMAP_DF.DROOM=14
    }

    /**
     * 取得目前所在位置
     * 目前因為只有firefox可以不用SSL前提下載入位置所以暫時加入瀏覽器偵測！
     * */
    detect = browserDetection();
    if ( detect.browser == 'firefox' ) {
        // 設定為中心點
        navigator.geolocation.getCurrentPosition(function(geo){
            var lat=geo.coords.latitude;
            var lng=geo.coords.longitude;

            _map_center_withlatlng(lat, lng, 16);
            console.log( lat+' '+lng );
        });
    } else {
        latLng = _map_center_withaddr(
                    GMAP_DF.CNT_ADDR ,
                    GMAP_DF.DROOM,
                    function(GEO, address) {
                        // _map_add_marker(GEO, 1);
                        // _map_update_latlng(GEO);
                    }
                );
    }


    optReservePanelClose_click();


    // hide side-panel
    var isMenuOpen = false;
    $('#hide-btn').click(function () {
        if (isMenuOpen == false) {
            $(".side-panel").clearQueue().animate({
                right: '-34%'
            })
            $(this).animate({'right': '0'});
            // 調整streetview
            $("#mapStreetView").animate({right: 20 });

            $("#hide-btn span").removeClass("arrowR").addClass("arrowL");
            isMenuOpen = true;
        }
        else if (isMenuOpen == true) {
            $(".side-panel").clearQueue().animate({
                right: '0'
            })
            $(this).animate({'right': '34%'});
            // 調整streetview
            $("#mapStreetView").animate({right: $(".side-panel").width()+20 });

            $("#hide-btn span").removeClass("arrowL").addClass("arrowR");
            isMenuOpen = false;
        }
        else{}
    });

    /**
     * Gincy處理自訂捲軸樣式套件
     * */
    (function($){
        $(window).on("load",function(){
            // 列表區塊
            $(".side-panel").mCustomScrollbar();
            // 預約看屋紀錄
            $(".reserve-wrap").mCustomScrollbar();
        });
    })(jQuery);

});
</script>