{% if this.request.getQuery('page') is empty %}
<div class="func">
    <div class="left">目前共有<span id="loadLabel" class="txt-ora" data-loadpage="{{ houseitems.current }}">{{ total_houseitems }}</span>筆物件 </div>
    <div class="sort">
    {% for keyname, item in this.session.MAPSEARCH['SORT'] %}
        <a id="{{item['id']}}" class="{{item['class']}}" data-sort="{{keyname}}" data-stname="{{item['field']}}" href="javascript:;">{{item['label']}} {{item['append']}}</a>
    {% endfor %}
    </div>
</div>

<section class="infinite">
{% endif %}

{% for item in pager %}

<div class="box all {% if item.newinweek == 1 %}new{% endif %} {% if not(item.priceDown is empty) %}down{% endif %}" data-stprice="{{item.priceTotal}}" data-starea="{{item.areaTotal}}" data-sthouseage="{{item.houseage}}">
    <div class="info">
        <h3>{{item.title}}</h3>
        <p>{{item.addressShow}}</p>
        <div class="detail">
            {% if item.areaParking %}
            <span>{{ item.areaTotal }}坪（房{{ item.areaMain }}坪+車{{ item.areaParking }}坪）</span>
            {% else %}
            <span>{{ item.areaTotal }}坪</span>
            {% endif %}
            <span>{{ numformat(item.houseage) }}年</span>

            {% if item.type is not empty %}
            <span>{% if item.floorAt == 0 %}整棟{{item.floorTotal}}樓{% else %}{{item.floorAt}}/{{item.floorTotal}} 樓{% endif %}</span>
            {% endif %}

            <span>{{item.patterns}}</span>
        </div>
    </div>
    <div class="price">
        {{item.priceTotal}}萬
        <!-- <span class="icon-d"></span> -->
    </div>
    <div class="btns">
        <a target="_blank" href="{{item.sourcelink}}" class="btn btn-white txt-ora">查看詳情</a>
        <button type="button"
                class="btn btn-white txt-blue opt-reserve"
                data-itemid="{{item.crawldataId}}">預約看屋</button>

        <button type="button"
                class="btn btn-white txt-white opt-notify"
                data-itemid="{{item.crawldataId}}">降價通知</button>

        <button type="button"
                class="btn btn-white opt-streetview"
                data-lat="{{item.lat}}"
                data-lng="{{item.lng}}"
                data-itemid="{{item.crawldataId}}">地圖街景</button>

        <button type="button"
                class="btn btn-like opt-collect"
                data-itemid="{{item.crawldataId}}"><span>加入收藏</span></button>
    </div>
    <addr style="display:none;" data-name="{{item.title}}" data-lat="{{item.lat}}" data-lng="{{item.lng}}">{{item.addressCity}}{{item.addressDistrict}}</addr>
</div>
{% endfor %}
<aside class="list-pager-nums">
{% if pager.haveToPaginate() %}
    {# Render the navigation #}
    {{ pager.getLayout() }}
{% endif %}
</aside>


{% if this.request.getQuery('page') is empty %}
</section>

<script>
/**
 * 配置地圖列表相關函式庫
 *
 * 1. optSortbtn_click() 排序(頁面現有項目重排)
 * 2. linkPagenation_click() 分頁ajax載入
 * */
function optSortbtn_click()
{
    $(document)
    .on("click", ".sortbtn", function(){
        // 排序根據指定欄位序
        var nowstfield=$(this).data('sort');
        var datast1=$(this).data('stname');

        if( nowstfield == GVAR.SORT.sort ){
            if( GVAR.SORT.sortmode == 'ASC' ){
                txt=$(this).find('i').attr('class');
                $(this).find('i').attr('class', txt.replace('up','down'));
                GVAR.SORT.sortmode = 'DESC';
            }else{
                txt=$(this).find('i').attr('class');
                $(this).find('i').attr('class', txt.replace('down','up'));
                GVAR.SORT.sortmode = 'ASC';
            }
        }else{
            $(".sortbtn").removeClass('current').find('i').attr('class','');
            if( GVAR.SORT.sortmode == 'ASC' ){
                $(this).addClass('current').find('i').attr('class','icon-up-b');
            }else{
                $(this).addClass('current').find('i').attr('class','icon-down-b');
            }
            GVAR.SORT.sort = nowstfield;
        }

        var callrefresh = function() {
            $.ajax({
                url: '?page=1',
                type: 'get',
                beforeSend: function(){
                },
                success: function(resp){
                    $(".infinite").html(resp);
                    // console.log(resp);
                }
            })
        }

        phase_refreshLists({
            do : 'sort',
            sort : GVAR.SORT.sort,
            sortmode : GVAR.SORT.sortmode
        }, callrefresh, '{{ url('/map/updatesession') }}' );
    });
}


function linkPagination_click()
{
    $(document)
    .on("click", ".pagination a", function(){
        $.ajax({
            url: this.href,
            type: 'get',
            beforeSend: function(){
            },
            success: function(resp){
                $(".infinite").html(resp);
                // console.log(resp);
            }
        })
        return false;
    });
}

$(function(){
    optSortbtn_click();

    // 分頁ajax載入
    linkPagination_click();

    // 點選預約看屋
    optReserve_click( '{{ url('/map/setreserve') }}' );

    // 點選預約看屋
    optReserveClose_click( '{{ url('/map/setreserve') }}' );

    _map_display_streetview();
    $(document)
    .on("click", ".opt-notify", function(){
        crawldataId=$(this).data('itemid');
        return false;
    })
    .on("click", ".opt-collect", function(){
        crawldataId=$(this).data('itemid');
        return false;
    });

})
</script>
{% endif %}