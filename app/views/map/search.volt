<form id="qForm" class="map-form" method="post" action="{{ url('/map/search')}}">
    {{ mapform.get('csrf').render() }}
    {{ mapform.renderCity() }}
    {{ mapform.renderDistrict() }}
    {{ mapform.renderTotalprice() }}
    {{ mapform.renderType() }}
    {{ mapform.renderPetternrooms() }}
    {{ mapform.renderAdvance() }}
    {{ mapform.get('title').render() }}
    {{ mapform.get('qBtn').render() }}
    <button type="submit" class="" name="rBtn" value="1">重設</button>
</form>
<div class="map-func">
    <div class="dis">
    {% for keyname, item in this.session.MAPSEARCH['TAB'] %}
    <a id="{{item['id']}}" class="{{item['class']}}" data-filter="{{keyname}}" href="{{ url('/map?filter=')~keyname }}">{{item['label']}}</a>
    {% endfor %}
    </div>
    <div class="mode" style="display:none;">
        <a href="javascript:;" class="active">
            <img src="/misc/images/icon-sort-map.png" width="16" height="16"><span>地圖模式</span>
        </a>
        <a href="javascript:;">
            <img src="/misc/images/icon-sort-list.png" width="16" height="16"><span>列表模式</span>
        </a>
    </div>
</div>

<script>
function mapsearch_load_districts(city, AJAX_URL ) {
    $("#elmDistricts").load( AJAX_URL ,
        {elm:"districts", city:city}, function(){
    });
}

$(function(){
    mapsearch_load_districts( $(":radio[name='city']:checked").val() , '{{ url('/map/formelms') }}');
    $(":radio[name='city']").click(function(){
        mapsearch_load_districts( $(this).val() , '{{ url('/map/formelms') }}');

        $("#elmDistricts").empty();
        $(this).parents('.option-panel').slideUp();
        $("#elmDistricts").slideToggle();
    });

    /**
     * 點選到輸入欄位時將radio指定移到其它上
     * */
     $(":text[id^='patterno'],:text[id^='flooro'],:text[id^='houseageo']").focus(function(){
        $(this).parents('span').find(":radio").prop('checked', true).attr('checked', true);
     });

    // 篩選處理
    // $(".toptab").click(function(){
    //     var value=$(this).data('filter');

    //     // 樣式切換
    //     $(".toptab").removeClass('active');
    //     $(this).addClass('active');

    //     _refreshLists({
    //         do : 'tab',
    //         filter : value
    //     }, function( respJson ){
    //         $(".infinite > .box").each(function(){
    //             if( $(this).hasClass(value) ){
    //                 $(this).show();
    //             }else{
    //                 // 隱藏項目不屬於篩選的項目
    //                 $(this).hide();
    //             }
    //         });
    //     });

    //     // Googlemap項目重新載入！
    // });
});
</script>