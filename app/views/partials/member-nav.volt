{% set membermenus = [
        {
            'label' : "基本資料",
            'icon' : url('misc/images/icon-member-01.png'),
            'submenus' : [
              {
                'label' : "編輯基本資料",
                'url' : url('member/profile')
              },
              {
                'label' : "更改密碼",
                'url' : url('member/profile/editPassword')
              }
            ]
        },
        {
            'label' : "帳戶管理",
            'icon' : url('misc/images/icon-member-02.png'),
            'submenus' : [
              {
                'label' : "保證金帳戶",
                'url' : url('member/account/security')
              },
              {
                'label' : "退款帳戶",
                'url' : url('member/account/refund')
              }
            ]
        },
        {
            'label' : "收藏管理",
            'icon' : url('misc/images/icon-member-03.png'),
            'url' : url('member/favorite')
        },
        {
            'label' : "降價通知",
            'icon' : url('misc/images/icon-member-09.png'),
            'url' : url('member/reduction')
        },
        {
            'label' : "刊登管理",
            'icon' : url('misc/images/icon-member-04.png'),
            'url' : url('member/post')
        },
        {
            'label' : "看屋管理",
            'icon' : url('misc/images/icon-member-05.png'),
            'url' : url('member/viewhouse')
        },
        {
            'label' : "議價管理",
            'icon' : url('misc/images/icon-member-06.png'),
            'url' : url('member/negotiation')
        },
        {
            'label' : "通知中心",
            'icon' : url('misc/images/icon-member-07.png'),
            'url' : url('member/notification'),
            'tmpnotice' : 3
        },
        {
            'label' : "理想好房設定",
            'icon' : url('misc/images/icon-member-08.png'),
            'submenus' : [
              {
                'label' : "理想清單",
                'url' : url('member/findhouse/list')
              },
              {
                'label' : "理想好房設定",
                'url' : url('member/findhouse/setting')
              }
            ]
        }
    ]
%}

<div class="left"><!--Left nav-->
    <ul id="membernav" class="membernav">
        {% for data in membermenus %}
            {% set classval = '' %}
            {% if data['url'] is defined and router.getRewriteUri() is data['url'] %}
                {% set classval = 'default open' %}
            {% elseif data['submenus'] is defined %}
                {% for value in data['submenus'] %}
                    {% if router.getRewriteUri() is value['url'] %}
                        {% set classval = 'default open' %}
                    {% endif %}
                {% endfor %}
            {% endif %}
        <li class="{{ classval }}">
            <div class="link">
            {% if data['url'] is defined %}
            <a href="{{ data['url'] }}">
            {% else %}
            <a href="javascript:">
            {% endif %}
            <img src="{{ data['icon'] }}" width="24" height="24"> {{ data['label'] }}</a></div>
            {% if data['submenus'] is defined %}
            <ul class="submenu">
            {% for value in data['submenus'] %}
                <li><a href=" {{ value['url'] }}"> {{ value['label'] }}</a></li>
            {% endfor %}
            </ul>
            {% endif %}
        </li>
        {% endfor %}
    </ul>
</div>