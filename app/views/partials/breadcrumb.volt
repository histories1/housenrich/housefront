<div class="navigation">
    <div class="row">
        <a href="{{ url("/") }}">首頁</a>
        {% if router.getRewriteUri() == '/member' %}
        <b class="current">會員中心</b>
        {% else %}
        <a href="{{ url("/member") }}">會員中心</a>
        <b class="current">{{ pageHeader }}</b>
        {% endif %}
    </div>
</div>
