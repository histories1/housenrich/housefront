{% set staticmenus = [
        {
            'label' : "使用者條款",
            'url'     : '/terms'
        },
        {
            'label' : "隱私權聲明",
            'url'     : '/privacy'
        },
        {
            'label' : "賣方議價服務聲明",
            'url'     : '/salerstatement'
        },
        {
            'label' : "買方議價服務聲明",
            'url'     : '/buyerstatement'
        }
    ]
%}
<div class="right">
    <ul>
        {% for data in staticmenus %}
            {% if router.getRewriteUri() is data['url'] %}
            {% set apclass = 'current' %}
            {% else %}
            {% set apclass = '' %}
            {% endif %}
        <li class="{{ apclass }}"><a href="{{ data['url'] }}">{{ data['label'] }}</a></li>
        {% endfor %}
    </ul>
</div>