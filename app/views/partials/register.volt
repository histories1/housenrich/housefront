{# 頂部：註冊popup區塊 #}
<div id="regpop" class="login-box pop" style="display:none;">
    <h2>註冊成為會員</h2>
    <a href="{{ url("/auth/register") }}" class="btn btn-block btn-orange">註冊成為會員</a>
    <div class="or-line">或</div>
    <div class="login-other">
        <a href="{{ url("/auth/fb") }}" class="forfb">Facebook註冊</a>
        <a href="{{ url("/auth/line") }}" class="forline">LINE註冊</a>
        <a href="{{ url("/auth/yahoo") }}" class="foryahoo">Yahoo註冊</a>
    </div>
    <div class="login-txt txt-center m-t-m">
    已經有帳號了嗎？<a href="{{ url("/auth/login") }}" class="df txt-b txt-line">點此登入</a>
    </div>
</div>