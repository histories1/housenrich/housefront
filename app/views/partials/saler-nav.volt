{% set slaermenus = [
        {
            'label' : "個人資料",
            'i'     : 1
        },
        {
            'label' : "物件類別",
            'i'     : 2
        },
        {
            'label' : "價格與行情",
            'i'     : 3
        },
        {
            'label' : "完成申請",
            'i'     : 4
        }
    ]
%}
<div class="navigation-bargain">
    <div class="row">
        {% for data in slaermenus %}
            {% if current == data['i'] %}
            {% set apclass = 'current' %}
            {% else %}
            {% set apclass = '' %}
            {% endif %}
        <div class="step {{ apclass }}"><span class="num">{{ data['i'] }}</span>{{ data['label'] }}</div>
        {% endfor %}
    </div>
</div>