{% if router.getRewriteUri() is not "/mapsearch" %}
<footer>
    <div class="row">
        <div id="logo"></div>
        <ul class="nav">
            <li><a href="{{ url("about") }}">關於我們</a></li>
            <li><a href="{{ url("houserich-faq") }}">了解好多房</a></li>
            <li><a href="{{ url("terms") }}">使用者條款</a></li>
            <li><a href="{{ url("privacy") }}">隱私權聲明</a></li>
            <li><a href="{{ url("sitemap") }}">網站地圖</a></li>
        </ul>
        <div class="contact">
            <span><img src="/misc/images/icon-foot-tell.png" width="20" height="20"> 電話：02-3333-2222</span>
            <span><img src="/misc/images/icon-foot-fax.png" width="20" height="20"> 傳真：02-3333-2222</span>
            <span><img src="/misc/images/icon-foot-tell.png" width="20" height="20"> E-mail：<a href="#">service@housenrich.com.tw</a></span>
        </div>
    </div>
    <div class="copy txt-center">Copyright © 好多房 2017 All Rights Reserved</div>
</footer>
{% endif %}