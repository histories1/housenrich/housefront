<header id="pageHeader" class="header">
    <a href="{{ url("/") }}" id="logo"></a>
    <ul id="nav">
        <li><a href="{{ url("map") }}">買屋</a></li>
        <li><a href="{{ url("saler") }}">我要賣房</a></li>
        <li><a href="{{ url("news") }}">房市新聞</a></li>
        <li><a href="{{ url("caseprice") }}">行情</a></li>
        <li><a href="{{ url("assess") }}">估價</a></li>

        {% if session.has('USER') %}
        <li class="log">
            <a href="{{ url("member")}}">
            <i class="fa fa-user"></i> {{ session.get('USER')['PeopleInformation']['fullName'] }}
            </a>
        </li>
        <li class="log">
            <a href="{{ url("auth/logout")}}">登出</a>
        </li>
        {% else %}
        <li class="log"><a href="javascript:viod(0);" class="loginpop_open trigger-login">登入</a></li>
        <li class="reg"><a href="javascript:viod(0);" class="regpop_open trigger-register">註冊</a></li>
        {% endif %}
    </ul>
</header>

{% include "member/auth-login.volt" %}

{% include "partials/register.volt" %}
