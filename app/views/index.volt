<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>好多房</title>
        {{assets.outputCss("headerStyle")}}
        {{assets.outputJs("headerScript")}}
    </head>
    <body>
        <div class="{{ container_parent_class|default(null) }}">
            {{partial("header")}}

            {{ this.flashSession.output() }}

            {{ content() }}

        </div>
        {{assets.outputJs("footerScript")}}
    </body>
</html>
