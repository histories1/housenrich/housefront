<div class="page-header">
    <h1>{{pageHeader}}</h1>
</div>

<p>{{pageDesc}}</p>

<p>
    <strong>Route資訊</strong>
    <hr>
    <dl>
        <dt>Namespace</dt>
        <dd><?=$this->router->getNamespaceName();?></dd>
        <dt>Module</dt>
        <dd><?=$this->router->getModuleName();?></dd>
        <dt>Controller</dt>
        <dd><?=$this->router->getControllerName();?></dd>
        <dt>Action</dt>
        <dd><?=$this->router->getActionName();?></dd>
    </dl>
</p>
