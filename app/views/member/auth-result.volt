<div class="reg-title">
    <h2 class="txt-center">{{ pageMessage['title'] }}</h2>
</div>
<div class="reg-box">
    <form class="form-default form">
        <div class="inner">
            <div class="field">
                {{ pageMessage['msg'] }}
            </div>
        </div>
        {% if !pageMessage['redirect'] %}
        <div class="btn-wrap txt-center">
            <a class="btn btn-blue btn-m" href="{{ url('/') }}">回首頁</a>
        </div>
        {% endif %}
    </form>
</div>


{% if pageMessage['redirect'] %}
<script type="text/javascript">
$(function(){
    setTimeout(function(){
        window.location = '{{ pageMessage['redirect_url'] }}';
    }, 2500);
});
</script>
{% endif %}