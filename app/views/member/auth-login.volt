{# 使用者登入畫面 #}
<div id="loginpop" class="login-box pop" style="margin:auto;display:none;">

<form id="loginForm" method="POST" action="{{ url("/auth/login/post") }}">
    <input type="hidden" name="" value="" />
    <h2>會員登入</h2>
    <input name="account" type="text" class="input-default formail" placeholder="帳號">
    <input name="password" type="password" class="input-default forpw" placeholder="密碼">
    <input type="submit" value="登入" class="btn btn-block btn-orange">
    <div class="login-txt">
        <div class="left">
        還沒有帳號嗎？<a href="{{ url("/auth/register") }}" class="df txt-b txt-line">立即註冊</a>
        </div>
        <div class="right">
        <a href="{{ url("/auth/forgetpasswd") }}" class="df txt-gray txt-line">忘記密碼</a>
        </div>
    </div>
    <div class="or-line">或</div>
    <div class="login-other">
        <a href="{{ url("/auth/facebook?to=oauth") }}" class="forfb">Facebook登入</a>
        <a href="{{ url("/auth/line?to=oauth") }}" class="forline">LINE登入</a>
        <a href="{{ url("/auth/yahoo?openid_mode=oauth") }}" class="foryahoo">Yahoo登入</a>
    </div>
</form>

</div>
