{% include "partials/breadcrumb.volt" %}

<div class="container">
<div class="row member-panel">
    {% include "partials/member-nav.volt" %}

    <!--Right content-->
    <div class="right">
        <h2>退款帳戶</h2>
        <div class="member-wrap">
            <h3 class="title">管理我的退款帳戶</h3>
            <div class="inner">
                <form  class="form-default" id="editpw" method="get" action="">
                    <div class="field">
                        <div class="col-2 field-label">銀行名稱及代碼</div>
                        <div class="col-10">
                            <select class="input-default">
                                <option>004 臺灣銀行</option>
                                <option>005 土地銀行</option>
                                <option>006 合庫商銀</option>
                                <option>007 第一銀行</option>
                                <option>008 華南銀行</option>
                                <option>009 彰化銀行</option>
                                <option>011 上海銀行</option>
                                <option>012 台北富邦</option>
                                <option>013 國泰世華</option>
                            </select>
                        </div>
                    </div>
                    <div class="dash-line"></div>
                    <div class="field">
                        <div class="col-2 field-label">帳戶名稱</div>
                        <div class="col-10"><input type="text" class="input-default" value="王小明專用"></div>
                    </div>
                    <div class="dash-line"></div>
                    <div class="field">
                        <div class="col-2 field-label">帳戶帳號</div>
                        <div class="col-10"><input type="text" class="input-default" value="22111112345"></div>
                    </div>
                </form>
            </div>
        </div>

        <div class="txt-right">
            <button type="button" class="btn btn-blue btn-m">儲存</button>
        </div>
    </div>
</div>
</div>