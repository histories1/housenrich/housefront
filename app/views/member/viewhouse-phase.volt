{% include "partials/breadcrumb.volt" %}


<div class="container">
<div class="row member-panel">
        {% include "partials/member-nav.volt" %}

    <!--Right content-->
    <div class="right">
        <div class="tab-wrapper">
            <div class="tab-title">
                <h2>看屋管理</h2>
                <ul class="tabs">
                    <li class="tabli active"><a href="#tab1">我是買家</a></li>
                    <li class="tabli"><a href="#tab2">我是賣家</a></li>
                </ul>
            </div>
            <div class="tab-container">
                <div id="tab1" class="tab-content">
                    <div class="sort-function">
                      <div class="left">
                        <select class="input-default m">
                            <option>預約中</option>
                            <option>已完成</option>
                        </select>
                      </div>
                      <div class="right"><button type="button" class="btn btn-ss"><img src="{{ url('/misc/images/icon-del.png') }}" width="14" height="14"></button>
                      </div>
                    </div>
                    <table class="table-default">
                      <thead>
                          <tr>
                              <th class="w3">物件標題</th>
                              <th class="w2">時間</th>
                              <th class="w1">帶看人</th>
                              <th class="w2">地點</th>
                              <th class="w05">備註</th>
                              <th class="w1">狀態</th>
                              <th class="w05 txt-right"><input class="default" type="checkbox" name="all" onclick="check_all(this,'c')" value="on"></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>昇陽大地黃金套房*麗山國中學區*</td>
                              <td>2016/11/11 19:00:00</td>
                              <td>王小明</td>
                              <td>台北市大安區復興南路</td>
                              <td class="forps">
                                <a href="#" class="txt-ora btn-pop">備註</a>
                                <div class="ps-pop">這是備註，這是備註備註備註，這是備註，這是備註備註備註。</div>
                              </td>
                              <td>預約中</td>
                              <td class="txt-right"><input class="default" type="checkbox" name="c" value=""></td>
                          </tr>
                          <tr>
                              <td>內湖成功路四段美宅內湖成功路四段美宅</td>
                              <td>2016/11/11 19:00:00</td>
                              <td>王小明</td>
                              <td>台北市大安區復興南路</td>
                              <td class="forps">
                                <a href="#" class="txt-ora btn-pop">備註</a>
                                <div class="ps-pop">這是備註，這是備註備註備註，這是備註，這是備註備註備註。</div>
                              </td>
                              <td>預約中</td>
                              <td class="txt-right"><input class="default" type="checkbox" name="c" value=""></td>
                          </tr>
                          <tr>
                              <td>昇陽大地黃金套房*麗山國中學區*</td>
                              <td>2016/11/11 19:00:00</td>
                              <td>王小明</td>
                              <td>台北市大安區復興南路</td>
                              <td class="forps">

                              </td>
                              <td>已完成</td>
                              <td class="txt-right"><input class="default" type="checkbox" name="c" value=""></td>
                          </tr>
                      </tbody>
                    </table>
                </div><!-- tab-content -->

                <div id="tab2" class="tab-content" style="display: none;">
                    <table class="table-default">
                      <thead>
                          <tr>
                              <th class="w4">物件標題</th>
                              <th class="w2">帶看人</th>
                              <th class="w4">預約時間</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>昇陽大地黃金套房*麗山國中學區*</td>
                              <td>王小明</td>
                              <td>2016/11/11 13:00</td>
                          </tr>
                          <tr>
                              <td>昇陽大地黃金套房*麗山國中學區*</td>
                              <td>王小明</td>
                              <td>2016/11/11 13:00</td>
                          </tr>
                          <tr>
                              <td>昇陽大地黃金套房*麗山國中學區*</td>
                              <td>王小明</td>
                              <td>2016/11/11 13:00</td>
                          </tr>
                          <tr>
                              <td>昇陽大地黃金套房*麗山國中學區*</td>
                              <td>王小明</td>
                              <td>2016/11/11 13:00</td>
                          </tr>
                      </tbody>
                    </table>
                </div><!-- tab-content -->

            </div>
        </div>
    </div>
</div>
</div>