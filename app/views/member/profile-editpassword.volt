{% include "partials/breadcrumb.volt" %}

<div class="container">
<div class="row member-panel">
        {% include "partials/member-nav.volt" %}

    <!--Right content-->
    <div class="right">
        <h2>更改密碼</h2>
        <div class="member-wrap">
            <h3 class="title">更改密碼</h3>
            <div class="inner">
                <form  class="form-default" id="editpw" method="POST" action="{{ url('member/save/PeopleInformation-passwd') }}">
                    <div class="field">
                        <div class="col-2 field-label">原密碼</div>
                        <div class="col-10"><input type="text" class="input-default" value=""></div>
                    </div>
                    <div class="dash-line"></div>
                    <div class="field">
                        <div class="col-2 field-label">新密碼</div>
                        <div class="col-10">
                        <input name="password" type="password" required class="input-default error" id="password" value="12345">
                        <label class="error" for="password">密碼錯誤，請重新輸入</label>
                      </div>
                    </div>
                    <div class="dash-line"></div>
                    <div class="field">
                        <div class="col-2 field-label">再輸入一次新密碼</div>
                        <div class="col-10">
                          <input type="password" id="confirm_password" name="confirm_password" class="input-default" required>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="txt-right">
            <button type="button" class="btn btn-blue btn-m">儲存</button>
        </div>
    </div>

</div>
</div>
