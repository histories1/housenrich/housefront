{% include "partials/breadcrumb.volt" %}

<div class="container">
<div class="row member-panel">
    {% include "partials/member-nav.volt" %}

    <div class="right"><!--Right content-->
        <h2>帳戶管理</h2>
        <div class="member-wrap">
            <h3 class="title">保證金帳戶</h3>
            <div class="inner form-default-t">
                <div class="field">
                    <div class="col-2 field-label">銀行名稱及代碼</div>
                    <div class="col-10">013 國泰世華</div>
                </div>
                <div class="field">
                    <div class="col-2 field-label">帳戶名稱</div>
                    <div class="col-10">好多房保證金信託帳戶</div>
                </div>
                <div class="field">
                    <div class="col-2 field-label">帳戶號碼</div>
                    <div class="col-10">0091191 1278504</div>
                </div>
                <div class="field">
                    <div class="col-2 field-label">保證金總額</div>
                    <div class="col-10">80000</div>
                </div>
                <div class="field">
                    <div class="col-2 field-label">保證金餘額</div>
                    <div class="col-10">20000</div>
                </div>
                <div class="field">
                    <div class="col-2 field-label">保證金使用中</div>
                    <div class="col-10">60000</div>
                </div>
            </div>
        </div>
        <div class="member-wrap">
            <h3 class="title">帳戶紀錄</h3>
            <div class="inner">
              <table class="table-default">
                  <thead>
                      <tr>
                          <th class="w4">時間</th>
                          <th class="w3">摘要</th>
                          <th class="w15">提款</th>
                          <th class="w15">存款</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>2016/11/02 19:00</td>
                          <td>轉帳匯款</td>
                          <td></td>
                          <td>30000</td>
                      </tr>
                      <tr>
                          <td>2016/11/02 19:00</td>
                          <td>現金存入</td>
                          <td></td>
                          <td>100000</td>
                      </tr>
                      <tr>
                          <td>2016/11/02 19:00</td>
                          <td>轉帳匯款</td>
                          <td>50000</td>
                          <td></td>
                      </tr>
                  </tbody>
              </table>
            </div>
        </div>

        <div class="member-wrap">
            <h3 class="title">保證金使用紀錄</h3>
            <div class="inner">
              <table class="table-default">
                  <thead>
                      <tr>
                          <th class="w2">摘要</th>
                          <th class="w2">金額</th>
                          <th class="w3">使用時間</th>
                          <th class="w3">退回時間</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>議價編號A</td>
                          <td>90000</td>
                          <td>2016/11/02 19:00</td>
                          <td>2016/11/02 19:00</td>
                      </tr>
                      <tr>
                          <td>議價編號B</td>
                          <td>90000</td>
                          <td>2016/11/02 19:00</td>
                          <td></td>
                      </tr>
                  </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
</div>