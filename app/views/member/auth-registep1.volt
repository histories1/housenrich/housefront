<div class="reg-title">
    <h2 class="txt-center">手機簡訊驗證</h2>
</div>
<div class="reg-box">

    <form id="smsForm" class="form-default form form-validation" method="post" action="{{ url("auth/register/sms") }}">
    {{ form.render("csrf") }}
    {{ form.render("type") }}

    <div class="inner">
        <div class="field">
            {{ form.render("cellphone") }}
            <button id="mappingBtn" type="button" class="btn btn-blue btn-ajaxsubmit hide" data-do="send">接收簡訊碼</button>

            <p id="phoneMsg" class="help-block txt-s txt-gray">請輸入台灣手機門號。</p>
        </div>

        <div id="codeBlock" class="field">
        {{ form.render("smscode") }}
            <p id="resMsg" class="help-block txt-s txt-gray">簡訊碼收到後，請於10分鐘內進行驗證。</p>
        </div>
    </div>

    <div class="btn-wrap txt-center">
        <button id="sendBtn" type="button" class="btn btn-blue btn-m btn-ajaxsubmit" data-do="mapping">送出驗證</button>
    </div>

    </form>
</div>

<script type="text/javascript">
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout(timer);
    timer = setTimeout(callback, ms);
  };
})();

$(function(){

    // 手機門號輸入
    $("#cellphone").keyup(function(){
        phonenum=jQuery(this).val();

        delay(function(){
            jQuery.ajax({
                url : '{{ url("auth/register/sms") }}',
                type: "POST",
                data: {cellphone:phonenum, do:'check' },
                dataType: "json",
                success: function(resp){
                    if( resp.res == 'pass_check' ){
                        $("#phoneMsg").removeClass('valid-block').text('請點選左側接收簡訊碼。');
                        $("#mappingBtn").removeClass('hide');
                    }else{
                        $("#phoneMsg").addClass('valid-block').text(resp.msg);
                        $("#mappingBtn").addClass('hide');
                    }
                }
            });
        }, 700 );
    });


    $(".btn-ajaxsubmit").click(function(){
        var validator=$("#smsForm").validate();
        if( !validator.form() ){
            return false;
        }
        dataset = $('#smsForm').serialize()+"&do="+$(this).data('do');

        $.ajax({
            url: $('#smsForm').attr('action'),
            type: 'POST',
            dataType: 'json',
            data: dataset,
            beforeSend: function(){
                $(".btn-ajaxsubmit").hide();
            },
            success: function( resp ){
                if( !resp.res ){
                    $("#resMsg").addClass('valid-block').text(resp.msg).show();
                }else if( resp.res == 'pass_send' ){
                    $("#resMsg").removeClass('valid-block').text('簡訊碼收到後，請於10分鐘內進行驗證。');
                }else if( resp.res == 'pass_mapping' ){
                    $(".btn-ajaxsubmit").attr('disabled', true);
                    window.location.reload();
                }
            },
            complete: function(){
                $(".btn-ajaxsubmit").show();
            }
        });
    });
});
</script>