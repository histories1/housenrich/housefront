<div class="container">
<div class="row member-panel">
        {% include "partials/member-nav.volt" %}

    <!--Right content-->
    <div class="right">

        <h2 class="m-b-m">刊登管理</h2>
        <div class="sort-member">
            <span>排列方式依：</span>
            <span class="current"><a href="#">上架日期<i class="icon-up-w"></i></a></span>
            <span class="down"><a href="#">總價<i class="icon-down-b"></i></a></span>
            <span class="down"><a href="#">單價<i class="icon-down-b"></i></a></span>
            <span class="down"><a href="#">坪數<i class="icon-down-b"></i></a></span>
            <span class="down"><a href="#">屋齡<i class="icon-down-b"></i></a></span>
            <span class="down"><a href="#">點閱數<i class="icon-down-b"></i></a></span>
        </div>
        <div class="sort-function">
            <div class="left sortnav"><a href="#" class="current">全部</a><a href="#">刊登中</a><a href="#">已下架</a></div>
        </div>

        <div class="object-list">
            <div> <!-- object 1 -->
                <div class="box">
                    <div class="imgs swiper-container swiper-container-horizontal">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide swiper-slide-active" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                            <div class="swiper-slide swiper-slide-next" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                            <div class="swiper-slide" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev swiper-button-disabled"></div>
                    </div>
                    <div class="info">
                        <h2 class="object-name">昇陽大地黃金套房麗*山國中學區*</h2>
                        <h3 class="object-add">台北市北投區行義路</h3>
                        <div class="detail">
                            <span>72.34坪(含車位4.75坪)</span>
                            <span>32年</span>
                            <span>電梯大樓</span>
                            <div>3房/3廳/2衛/1室(加蓋：3房/3廳/2衛/1室)</div>
                            <div><span>6F-7F/B2-6F</span><span>點閱數：500</span></div>
                        </div>
                        <div class="btns"><a href="published-edit.html" class="btn btn-white">編輯</a><a href="#" class="btn btn-orange">申請上架</a></div>
                    </div>
                    <div class="price">
                        <div class="oprice">4,580萬</div>
                        <div class="cprice"><span>4,188</span>萬</div>
                        <div><span class="down"><span class="icon-down-w"></span>8.56%</span></div>
                        <div class="txt-ora">(含車位價)</div>
                        <div>40萬/坪</div>
                    </div>
                </div><!-- box -->
                <div class="object-ps">
                    <textarea rows="3">寫下對此案件的想法</textarea>
                    <div class="txt-right"><button type="button" class="btn btn-default">儲存</button></div>
                </div>
            </div><!-- object 1 -->

            <div> <!-- object 2 -->
                <div class="box">
                    <div class="imgs swiper-container swiper-container-horizontal">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide swiper-slide-active" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                            <div class="swiper-slide swiper-slide-next" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                            <div class="swiper-slide" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev swiper-button-disabled"></div>
                    </div>
                    <div class="info">
                        <h2 class="object-name">昇陽大地黃金套房*麗山國中學區*</h2>
                        <h3 class="object-add">台北市北投區行義路</h3>
                        <div class="detail">
                            <span>72.34坪(含車位4.75坪)</span>
                            <span>32年</span>
                            <span>電梯大樓</span>
                            <div>3房/3廳/2衛/1室(加蓋：3房/3廳/2衛/1室)</div>
                            <div><span>6F-7F/B2-6F</span><span>點閱數：500</span></div>
                        </div>
                        <div class="btns"><a href="published-edit.html" class="btn btn-white">編輯</a><a href="#" class="btn btn-blue">申請下架</a></div>
                    </div>
                    <div class="price">
                        <div class="oprice">4,580萬</div>
                        <div class="cprice"><span>4,188</span>萬</div>
                        <div><span class="down"><span class="icon-down-w"></span>8.56%</span></div>
                        <div class="txt-ora">(含車位價)</div>
                        <div>40萬/坪</div>
                    </div>
                </div><!-- box -->
                <div class="object-ps">
                    <textarea rows="3">寫下對此案件的想法</textarea>
                    <div class="txt-right"><button type="button" class="btn btn-default">儲存</button></div>
                </div>
            </div><!-- object 2 -->

        </div><!-- object-list -->

    </div>
</div>
</div>