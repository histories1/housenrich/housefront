{% include "partials/breadcrumb.volt" %}

<div class="container">
<div class="row member-panel">
        {% include "partials/member-nav.volt" %}

    <!--Right content-->
    <div class="right">

        <h2 class="m-b-m">理想清單</h2>
        <div class="sort-member">
            <span>排列方式依：</span>
            <span class="current"><a href="#">上架日期<i class="icon-up-w"></i></a></span>
            <span class="down"><a href="#">總價<i class="icon-down-b"></i></a></span>
            <span class="down"><a href="#">單價<i class="icon-down-b"></i></a></span>
            <span class="down"><a href="#">坪數<i class="icon-down-b"></i></a></span>
            <span class="down"><a href="#">屋齡<i class="icon-down-b"></i></a></span>
            <span class="down"><a href="#">點閱數<i class="icon-down-b"></i></a></span>
        </div>
        <div class="condition-tag">
            <select class="input-default m">
                <option>條件A</option>
                <option>條件B</option>
            </select>
                當前設定：<span class="con-tag">2房</span>
                <span class="con-tag">800-1200萬</span>
                <span class="con-tag">10-12年</span>
                <span class="con-tag">30-50萬</span>
                <span class="con-tag">30-40坪</span>
                <span class="con-tag">坐東朝西/坐東南朝西北/坐西北朝東南</span>
                <span class="con-tag">公寓/別墅</span>
                <span class="con-tag">代理人刊登/中介刊登</span>
                <span class="con-tag">有車位</span>
                <span class="con-tag">傳統市場/近公園</span>
        </div>
        <div class="sort-function">
            <div class="left sortnav"><a href="#" class="current">全部</a><a href="#">刊登中</a><a href="#">已下架</a></div>
            <div class="right">
                <button type="button" class="btn" title="刪除"><img src=" {{ url('/misc/images/icon-del') }}.png" width="14" height="14"></button>
                <span class="btn" title="全選"><input type="checkbox" class="default" name="all" onclick="check_all(this,'c')" value="on"></span>
            </div>
        </div>
        <div class="object-list">

            <div class="box">
                <a href="#" class="btn-del"><input type="checkbox" class="default" name="c" value=""></a>
                <div class="imgs swiper-container swiper-container-horizontal">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide swiper-slide-active" style="width: 300px; margin-right: 30px;"><img src=" {{ url('/misc/images/sample1.jpg') }}"></div>
                        <div class="swiper-slide swiper-slide-next" style="width: 300px; margin-right: 30px;"><img src=" {{ url('/misc/images/sample1.jpg') }}"></div>
                        <div class="swiper-slide" style="width: 300px; margin-right: 30px;"><img src=" {{ url('/misc/images/sample1.jpg') }}"></div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev swiper-button-disabled"></div>
                </div>
                <div class="info">
                    <h2 class="object-name">昇陽大地黃金套房*麗山國中學區*</h2>
                    <h3 class="object-add">台北市北投區行義路</h3>
                    <div class="detail">
                        <span>72.34坪(含車位4.75坪)</span>
                        <span>32年</span>
                        <span>電梯大樓</span>
                        <div>3房/3廳/2衛/1室(加蓋：3房/3廳/2衛/1室)</div>
                        <div><span>6F-7F/B2-6F</span><span>點閱數：500</span></div>
                    </div>
                    <div class="btns"><a href="#" class="btn btn-gray btn-disable"><img src=" {{ url('/misc/images/icon-member') }}-09.png" width="22" height="22"> 降價通知</a><a href="#" class="btn btn-gray"><img src=" {{ url('/misc/images/icon-member') }}-03.png" width="20" height="20"> 加入收藏</a></div>
                </div>
                <div class="price">
                    <div class="oprice">4,580萬</div>
                    <div class="cprice"><span>4,188</span>萬</div>
                    <div><span class="down"><span class="icon-down-w"></span>8.56%</span></div>
                    <div class="txt-ora">(含車位價)</div>
                    <div>40萬/坪</div>
                </div>
            </div><!-- object 1 -->

            <div class="box">
                <a href="#" class="btn-del"><input type="checkbox" class="default" name="c" value=""></a>
                <div class="imgs swiper-container swiper-container-horizontal">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide swiper-slide-active" style="width: 300px; margin-right: 30px;"><img src=" {{ url('/misc/images/sample1.jpg') }}"></div>
                        <div class="swiper-slide swiper-slide-next" style="width: 300px; margin-right: 30px;"><img src=" {{ url('/misc/images/sample1.jpg') }}"></div>
                        <div class="swiper-slide" style="width: 300px; margin-right: 30px;"><img src=" {{ url('/misc/images/sample1.jpg') }}"></div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev swiper-button-disabled"></div>
                </div>
                <div class="info">
                    <h2 class="object-name">昇陽大地黃金套房*麗山國中學區*</h2>
                    <h3 class="object-add">台北市北投區行義路</h3>
                    <div class="detail">
                        <span>72.34坪(含車位4.75坪)</span>
                        <span>32年</span>
                        <span>電梯大樓</span>
                        <div>3房/3廳/2衛/1室(加蓋：3房/3廳/2衛/1室)</div>
                        <div><span>6F-7F/B2-6F</span><span>點閱數：500</span></div>
                    </div>
                    <div class="btns"><a href="#" class="btn btn-gray"><img src=" {{ url('/misc/images/icon-member') }}-09.png" width="22" height="22"> 降價通知</a><a href="#" class="btn btn-gray btn-disable"><img src=" {{ url('/misc/images/icon-member') }}-03.png" width="20" height="20"> 加入收藏</a></div>
                </div>
                <div class="price">
                    <div class="oprice">4,580萬</div>
                    <div class="cprice"><span>4,188</span>萬</div>
                    <div><span class="down"><span class="icon-down-w"></span>8.56%</span></div>
                    <div class="txt-ora">(含車位價)</div>
                    <div>40萬/坪</div>
                </div>
            </div><!-- object 1 -->

            <div class="box">
                <a href="#" class="btn-del"><input type="checkbox" class="default" name="c" value=""></a>
                <div class="imgs swiper-container swiper-container-horizontal">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide swiper-slide-active" style="width: 300px; margin-right: 30px;"><img src=" {{ url('/misc/images/sample1.jpg') }}"></div>
                        <div class="swiper-slide swiper-slide-next" style="width: 300px; margin-right: 30px;"><img src=" {{ url('/misc/images/sample1.jpg') }}"></div>
                        <div class="swiper-slide" style="width: 300px; margin-right: 30px;"><img src=" {{ url('/misc/images/sample1.jpg') }}"></div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev swiper-button-disabled"></div>
                </div>
                <div class="info">
                    <h2 class="object-name">昇陽大地黃金套房*麗山國中學區*</h2>
                    <h3 class="object-add">台北市北投區行義路</h3>
                    <div class="detail">
                        <span>72.34坪(含車位4.75坪)</span>
                        <span>32年</span>
                        <span>電梯大樓</span>
                        <div>3房/3廳/2衛/1室(加蓋：3房/3廳/2衛/1室)</div>
                        <div><span>6F-7F/B2-6F</span><span>點閱數：500</span></div>
                    </div>
                    <div class="btns"><a href="#" class="btn btn-gray"><img src=" {{ url('/misc/images/icon-member') }}-09.png" width="22" height="22"> 降價通知</a><a href="#" class="btn btn-gray"><img src=" {{ url('/misc/images/icon-member') }}-03.png" width="20" height="20"> 加入收藏</a></div>
                </div>
                <div class="price">
                    <div class="oprice">4,580萬</div>
                    <div class="cprice"><span>4,188</span>萬</div>
                    <div><span class="down"><span class="icon-down-w"></span>8.56%</span></div>
                    <div class="txt-ora">(含車位價)</div>
                    <div>40萬/坪</div>
                </div>
            </div><!-- object 1 -->

        </div><!-- object-list -->

    </div>
</div>
</div>