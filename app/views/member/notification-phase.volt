{% include "partials/breadcrumb.volt" %}


<div class="container">
<div class="row member-panel">
    {% include "partials/member-nav.volt" %}

    <!--Right content-->
    <div class="right">

        <h2>通知中心</h2>
        <div class="member-wrap2">
            <div class="sort-function">
                <div class="left sortnav"><a href="#" class="current">全部</a><a href="#">未讀</a><a href="#">已讀</a></div>
                <div class="right">
                 1-10則，共99則
                <button type="button" class="btn" title="刪除"><img src="{{ url('/misc/images/icon-del.png') }}" width="14" height="14"></button>
                <span class="btn" title="全選"><input type="checkbox" class="default" name="all" onclick="check_all(this,'c')" /></span>
                 </div>
            </div>
            <div class="msg-box">
                <div class="box unread">
                    <a href="message-detail.html" class="txt">
                        <h3>這是未讀的訊息標題</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag1">找到房</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
                <div class="box unread">
                    <a href="message-detail.html" class="txt">
                        <h3>未讀訊息</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag2">學區新案上架</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
                <div class="box read">
                    <a href="message-detail.html" class="txt">
                        <h3>這是已讀的訊息標題這是已讀訊息標題</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag1">找到房</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
                <div class="box read">
                    <a href="message-detail.html" class="txt">
                        <h3>這是訊息標題這是訊息標題</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag2">學區新案上架</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
                <div class="box read">
                    <a href="message-detail.html" class="txt">
                        <h3>這是訊息標題這是訊息標題</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag3">收藏物件下架</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
                <div class="box read">
                    <a href="message-detail.html" class="txt">
                        <h3>這是訊息標題這是訊息標題</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag4">降價</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
                <div class="box read">
                    <a href="message-detail.html" class="txt">
                        <h3>這是訊息標題這是訊息標題</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag1">議價成功</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
                <div class="box read">
                    <a href="message-detail.html" class="txt">
                        <h3>這是訊息標題這是訊息標題</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag2">學區新案上架</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
                <div class="box read">
                    <a href="message-detail.html" class="txt">
                        <h3>這是訊息標題這是訊息標題</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag1">議價成功</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
                <div class="box read">
                    <a href="message-detail.html" class="txt">
                        <h3>這是訊息標題這是訊息標題</h3>
                        <p>訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容，訊息內容，這是訊息內容，訊息內容訊息內容</p>
                    </a>
                    <div class="tag"><span class="msg-tag2">學區新案上架</span></div>
                    <div class="txt-gray">2016/11/11</div>
                    <a href="#" class="del"><input type="checkbox" class="default" name="c" value=""></a>
                </div>
            </div> <!-- msg box list -->

            <div class="page-nav">
                <a href="#" class="current">1</a><a href="#">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a><a href="#">6</a><a href="#">...</a><a href="#">20</a>
            </div>
        </div>

    </div>
</div>
</div>