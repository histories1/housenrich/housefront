{% include "partials/breadcrumb.volt" %}

<div class="container">
<div class="row member-panel">
        {% include "partials/member-nav.volt" %}

        <!--Right content-->
        <div class="right">
            <h2>編輯基本資料</h2>
            <div class="member-wrap">
                <h3 class="title">基本資料<span class="must txt-right">為必填</span></h3>
                <div class="inner">
                    <form class="form-default form-validation" method="POST" action="{{ url('member/profile/save/PeopleInformation') }}">
                        {{ form.render('PeopleId') }}
                        <div class="field">
                            <div class="col-2 field-label must">帳號</div>
                            <div class="col-10">{{ form.get('account').getValue() }}</div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 field-label must">密碼</div>
                            <div class="col-10"> <a href="{{ url('member/profile/editPassword') }}" class="btn btn-default">更改</a></div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 field-label must">姓名</div>
                            <div class="col-10">
                            {{ form.render('fullName') }}
                            </div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 field-label must">性別</div>
                            <div class="col-10">
                                {{ form.render('gender') }}
                            </div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 field-label must">年齡</div>
                            <div class="col-10">
                                {{ form.render('age') }}
                            </div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 field-label">婚姻/成員</div>
                            <div class="col-10">
                                {{ form.renderMarriageMembers() }}
                            </div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 field-label">職業</div>
                            <div class="col-10">
                                {{ form.render('occupation') }}
                            </div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            {{ form.renderAnnualincome() }}
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 field-label">完整地址</div>
                            {{ form.renderGroups(["addressCity",
                                                "addressDistrict",
                                                "addressRoad",
                                                "addressLane",
                                                "addressAlley",
                                                "addressNo",
                                                "addressNoEx",
                                                "addressFloor",
                                                "addressFloorEx"],
                                                "\\Housefront\\Forms\\Decorators\\Addresses") }}
                        </div>
                        <div class="dash-line"></div>
                        <div id="smsForm" class="field">
                            <div class="col-2 field-label">手機</div>
                            <div class="col-10">
                                {% if session.get('USER')['People'] is defined %}
                                <span>{{ session.get('USER')['People']['cellphone'] }}</span>
                                {% else %}
                                <input type="text" class="input-default L" id="cellphone" name="cellphone" value="{{ session.get('USER')['People']['cellphone'] }}" />
                                <button type="button" class="btn btn-gray btn-getcode btn-ajaxsms" data-do="memsend">發送認證碼</button>

                                <input type="text" class="input-default m" id="smscode" name="smscode" placeholder="請輸入認證碼" />

                                <button type="button" class="btn btn-blue btn-mappingcode btn-ajaxsms" data-do="mapping">確認</button>

                                <div id="resMsg"></div>
                                {% endif %}
                            </div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 field-label">E-mail</div>
                            <div class="col-10">
                                {{ form.render('email') }}
                            </div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 field-label">LineID</div>
                            <div class="col-10">
                                {{ form.render('lineID') }}
                            </div>
                        </div>
                    <div class="txt-right">
                        <button type="button" class="btn btn-blue btn-m btn-ajaxsubmit">儲存</button>
                    </div>
                    </form>
                </div>
            </div> <!-- box end -->

            <div class="member-wrap">
                <h3 class="title">發票資料</h3>
                <div class="inner">
                    <form class="form-default form-validation" method="POST" action="{{ url('member/profile/save/PeopleInvoice') }}">
                        {{ formI.render('PeopleId') }}
                        <div class="field">
                            <div class="col-2 field-label">買受人/抬頭</div>
                            <div class="col-10">{{ formI.render('title') }}</div>
                        </div>
                        <div class="field">
                            <div class="col-2 field-label">統一編號</div>
                            <div class="col-10">{{ formI.render('NO') }}</div>
                        </div>
                        <div class="field">
                            <div class="col-2 field-label">聯絡電話</div>
                            <div class="col-10">{{ formI.render('phone') }}</div>
                        </div>
                        <div class="field">
                            <div class="col-2 field-label">收件地址</div>
                            {{ formI.renderGroups(["addressCity",
                                                "addressDistrict",
                                                "addressRoad",
                                                "addressLane",
                                                "addressAlley",
                                                "addressNo",
                                                "addressNoEx",
                                                "addressFloor",
                                                "addressFloorEx"],
                                                "\\Housefront\\Forms\\Decorators\\Addresses") }}
                        </div>
                    <div class="txt-right">
                        <button type="button" class="btn btn-blue btn-m btn-ajaxsubmit">儲存</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

</div>
</div>
<script>
$(function(){
    {% if session.USER['addressCity'] is defined %}
    _depend_city_withdistrict( $("#addressCity"), 1 );
    {% endif %}

    {% if session.USER['addrCity'] is defined %}
    _depend_city_withdistrict( $("#addrCity"), 1 );
    {% endif %}

    $(".btn-ajaxsms").click(function(){
        todo = $(this).data('do');
        $.ajax({
            url: '{{ url('member/smscheck') }}',
            type: 'POST',
            dataType: 'json',
            data: {cellphone: $("#cellphone").val(),smscode: $("#smscode").val(), do: todo},
            beforeSend: function(){
                $(".btn-ajaxsms").hide();
            },
            success: function( resp ){
                if( !resp.res ){
                    $("#resMsg").addClass('valid-block').text(resp.msg).show();
                }else if( resp.res == 'pass_send' ){
                    $("#resMsg").removeClass('valid-block').text('簡訊碼收到後，請於10分鐘內進行驗證。');
                }else if( resp.res == 'pass_mapping' ){
                    $(".btn-ajaxsms").attr('disabled', true);
                    window.location.reload();
                }
            },
            complete: function(){
                $(".btn-ajaxsms").show();
            }
        });
    });
});
</script>
