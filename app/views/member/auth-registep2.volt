<div class="reg-title">
    <h2 class="txt-center">會員註冊</h2>
    <div class="txt-right"><span class="must">為必填</span></div>
</div>
<div class="reg-box">
    <form id="regiForm" class="form-default form form-validation" method="post" action="{{ url("auth/register/post") }}">

    {{ form.render("csrf") }}

        <div class="inner">
            <div class="field">
                <div class="col-2 field-label must">帳號</div>
                <div class="col-10">
                {{ form.render("account") }}
                </div>
            </div>
            <div class="field">
                <div class="col-2 field-label must">密碼</div>
                <div class="col-10">
                {{ form.render("password") }}
                </div>
            </div>
            <div class="field">
                <div class="col-2 field-label must">確認密碼</div>
                <div class="col-10"><input type="password" id="confirm_password" name="confirm_password" class="input-default valid-confirmpw" required></div>
            </div>
            <div class="field">
                <div class="col-2 field-label must">姓名</div>
                <div class="col-10">{{ form.render("fullName") }}</div>
            </div>
            <div class="field">
                <div class="col-2 field-label must">性別</div>
                <div class="col-10">
                    <div class="radio-style">
                        <input type="radio" id="male" name="gender" value="男" checked />
                        <label for="male">男</label>
                        <input type="radio" id="female" name="gender" value="女" />
                        <label for="female">女</label>
                    </div>
                </div>
            </div>
            <div class="field">
                <div class="col-2 field-label must">年齡</div>
                <div class="col-10">{{ form.render("age") }}歲</div>
            </div>

            <div class="field">
                <div class="col-2 field-label">Email</div>
                <div class="col-10">{{ form.render("email") }}</div>
            </div>

            <div class="field">
                <div class="col-2 field-label">LineID</div>
                <div class="col-10">{{ form.render("lineID") }}</div>
            </div>

            <div class="field check-style txt-center txt-s">
                <input type="checkbox" id="agree" name="agree" value="1" aria-required="true" >
                <label>
                我已閱讀並同意<a href="#" class="txt-blue txt-line df">使用者條款與隱私權政策</a>
                </label>
            </div>
        </div> <!-- inner end -->
        <div class="btn-wrap txt-center">
            <input class="btn btn-blue btn-m" type="submit" value="註冊" />
        </div>
    </form>
</div>

<script type="text/javascript">
$(function(){
    // $("#regiForm").submit(function(){
    //     return false;
    // });
});
</script>
