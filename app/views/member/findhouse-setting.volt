{% include "partials/breadcrumb.volt" %}

<div class="container">
<div class="row member-panel">
        {% include "partials/member-nav.volt" %}

    <!--Right content-->
    <div class="right">
        <h2>找房條件設定</h2>
        <div class="member-wrap">
            <h3 class="title">條件設定</h3>
            <div class="inner">
            <div class="sort-function">
                  <div class="left">
                    <a href="condition-add.html" class="btn btn-blue">+ 新增條件</a>
                  </div>
                  <div class="right"><button type="button" class="btn btn-ss"><img src="../images/icon-del.png" width="14" height="14"></button>
                  </div>
                </div>
                <table class="table-default">
                  <thead>
                      <tr>
                          <th class="w5">條件名稱</th>
                          <th class="w15">條件內容</th>
                          <th class="w3">建立時間</th>
                          <th class="w05"><input type="checkbox" class="default" name="all" onclick="check_all(this,'c')" value="on"></th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>條件名稱Ａ條件名稱Ａ條件名稱Ａ條件名稱Ａ</td>
                          <td><a href="condition-detail.html" class="txt-ora">內容詳情</a></td>
                          <td>2016/11/11 19:00:00</td>
                          <td><input type="checkbox" class="default" name="c" value=""></td>
                      </tr>
                      <tr>
                          <td>條件名稱Ｂ</td>
                          <td><a href="condition-detail.html" class="txt-ora">內容詳情</a></td>
                          <td>2016/11/11 19:00:00</td>
                          <td><input type="checkbox" class="default" name="c" value=""></td>
                      </tr>
                      <tr>
                          <td>條件名稱Ａ條件名稱Ａ條件名稱Ａ條件名稱Ａ</td>
                          <td><a href="condition-detail.html" class="txt-ora">內容詳情</a></td>
                          <td>2016/11/11 19:00:00</td>
                          <td><input type="checkbox" class="default" name="c" value=""></td>
                      </tr>
                      <tr>
                          <td>條件名稱Ｃ</td>
                          <td><a href="condition-detail.html" class="txt-ora">內容詳情</a></td>
                          <td>2016/11/11 19:00:00</td>
                          <td><input type="checkbox" class="default" name="c" value=""></td>
                      </tr>
                  </tbody>
              </table>
            </div>
        </div>

    </div>
</div>
</div>