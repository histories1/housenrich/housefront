{% include "partials/breadcrumb.volt" %}

<div class="container">
<div class="row member-panel">
        {% include "partials/member-nav.volt" %}

    <!--Right content-->
    <div class="right">
        <div class="tab-wrapper">
            <div class="tab-title">
                <h2>議價管理</h2>
                <ul class="tabs">
                    <li class="tabli active"><a href="#tab1">議價中</a></li>
                    <li class="tabli"><a href="#tab2">議價失敗</a></li>
                    <li class="tabli"><a href="#tab3">議價成功</a></li>
                </ul>
            </div>
            <div class="tab-container">

                <div id="tab1" class="tab-content"> <!-- 議價中 -->
                    <div class="object-list">
                        <!-- object 1 -->
                        <div class="box">
                            <div class="imgs swiper-container swiper-container-horizontal">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide swiper-slide-active" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                    <div class="swiper-slide swiper-slide-next" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                    <div class="swiper-slide" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev swiper-button-disabled"></div>
                            </div>
                            <div class="info">
                                <h2 class="object-name">昇陽大地黃金套房*麗山國中學區*</h2>
                                <h3 class="object-add">台北市北投區行義路</h3>
                                <div class="detail">
                                    <span>72.34坪(含車位4.75坪)</span>
                                    <span>32年</span>
                                    <span>電梯大樓</span>
                                    <div>3房/3廳/2衛/1室(加蓋：3房/3廳/2衛/1室)</div>
                                    <div><span>6F-7F/B2-6F</span><span>點閱數：500</span></div>
                                </div>
                            </div>
                            <div class="price">
                                <div class="oprice">4,580萬</div>
                                <div class="cprice"><span>4,188</span>萬</div>
                                <div><span class="down"><span class="icon-down-w"></span>8.56%</span></div>
                                <div class="txt-ora">(含車位價)</div>
                                <div>40萬/坪</div>
                            </div>
                        </div><!-- box -->

                        <table class="table-default">
                          <thead>
                              <tr>
                                  <th>狀態</th>
                                  <th>議價編號</th>
                                  <th>剩餘時間</th>
                                  <th>買方最新出價</th>
                                  <th>賣方最新出價</th>
                                  <th>保證金金額</th>

                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>
                                  <span class="bg-tag1">等待對方回應</span>
                                <!--<span class="bg-tag2">新出價</span>-->
                                <!--<span class="bg-tag3">出價被拒絕</span>-->
                                <!--<span class="bg-tag4">等待確認使用保證金</span>-->
                                  </td>
                                  <td>A00001</td>
                                  <td>10天10小時10分</td>
                                  <td>2300萬</td>
                                  <td>2300萬</td>
                                  <td>10,000</td>
                              </tr>
                              <tr>
                                  <td><span class="bg-tag3">出價被拒絕</span></td>
                                  <td>A00002</td>
                                  <td>10天10小時10分</td>
                                  <td>2300萬</td>
                                  <td>2300萬</td>
                                  <td>10,000</td>
                              </tr>
                              <tr>
                                  <td><span class="bg-tag4">等待確認使用保證金</span></td>
                                  <td>A00003</td>
                                  <td>10天10小時10分</td>
                                  <td>2300萬</td>
                                  <td>2300萬</td>
                                  <td>10,000</td>
                              </tr>
                          </tbody>
                        </table>

                        <div class="c-line"></div>

                        <!-- object 2 -->
                        <div class="box">
                            <div class="imgs swiper-container swiper-container-horizontal">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide swiper-slide-active" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                    <div class="swiper-slide swiper-slide-next" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                    <div class="swiper-slide" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev swiper-button-disabled"></div>
                            </div>
                            <div class="info">
                                <h2 class="object-name">昇陽大地黃金套房*麗山國中學區*</h2>
                                <h3 class="object-add">台北市北投區行義路</h3>
                                <div class="detail">
                                    <span>72.34坪(含車位4.75坪)</span>
                                    <span>32年</span>
                                    <span>電梯大樓</span>
                                    <div>3房/3廳/2衛/1室(加蓋：3房/3廳/2衛/1室)</div>
                                    <div><span>6F-7F/B2-6F</span><span>點閱數：500</span></div>
                                </div>
                            </div>
                            <div class="price">
                                <div class="oprice">4,580萬</div>
                                <div class="cprice"><span>4,188</span>萬</div>
                                <div><span class="down"><span class="icon-down-w"></span>8.56%</span></div>
                                <div class="txt-ora">(含車位價)</div>
                                <div>40萬/坪</div>
                            </div>
                        </div><!-- box -->

                        <table class="table-default">
                          <thead>
                              <tr>
                                  <th>狀態</th>
                                  <th>議價編號</th>
                                  <th>剩餘時間</th>
                                  <th>買方最新出價</th>
                                  <th>賣方最新出價</th>
                                  <th>保證金金額</th>

                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><span class="bg-tag2">新出價</span></td>
                                  <td>A00001</td>
                                  <td>10天10小時10分</td>
                                  <td>2300萬</td>
                                  <td>2300萬</td>
                                  <td>10,000</td>
                              </tr>
                              <tr>
                                  <td><span class="bg-tag1">等待對方回應</span></td>
                                  <td>A00002</td>
                                  <td>10天10小時10分</td>
                                  <td>2300萬</td>
                                  <td>2300萬</td>
                                  <td>10,000</td>
                              </tr>
                              <tr>
                                  <td><span class="bg-tag1">等待對方回應</span></td>
                                  <td>A00003</td>
                                  <td>10天10小時10分</td>
                                  <td>2300萬</td>
                                  <td>2300萬</td>
                                  <td>10,000</td>
                              </tr>
                          </tbody>
                        </table>

                    </div> <!-- object list -->
                </div><!-- tab-content -->

                <div id="tab2" class="tab-content" style="display: none;"> <!-- 議價失敗 -->

                    <div class="object-list">
                        <div class="box">
                            <div class="imgs swiper-container swiper-container-horizontal">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide swiper-slide-active" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                    <div class="swiper-slide swiper-slide-next" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                    <div class="swiper-slide" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev swiper-button-disabled"></div>
                            </div>
                            <div class="info">
                                <h2 class="object-name">昇陽大地黃金套房*麗山國中學區*</h2>
                                <h3 class="object-add">台北市北投區行義路</h3>
                                <div class="detail">
                                    <span>72.34坪(含車位4.75坪)</span>
                                    <span>32年</span>
                                    <span>電梯大樓</span>
                                    <div>3房/3廳/2衛/1室(加蓋：3房/3廳/2衛/1室)</div>
                                    <div><span>6F-7F/B2-6F</span><span>點閱數：500</span></div>
                                </div>
                            </div>
                            <div class="price">
                                <div class="oprice">4,580萬</div>
                                <div class="cprice"><span>4,188</span>萬</div>
                                <div><span class="down"><span class="icon-down-w"></span>8.56%</span></div>
                                <div class="txt-ora">(含車位價)</div>
                                <div>40萬/坪</div>
                            </div>
                        </div><!-- box -->

                        <table class="table-default">
                          <thead>
                              <tr>
                                  <th>狀態</th>
                                  <th>議價編號</th>
                                  <th>結束時間</th>
                                  <th>買方最新出價</th>
                                  <th>賣方最新出價</th>
                                  <th>保證金金額</th>
                                  <th></th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td><span class="bg-tag5">議價時間結束</span></td>
                                  <td>A00001</td>
                                  <td>2016/11/11 19:00:00</td>
                                  <td>2300萬</td>
                                  <td>2300萬</td>
                                  <td>10,000</td>
                                  <td><a href="#" class="btn txt-ora">再次開啟議價</a></td>
                              </tr>
                              <tr>
                                  <td><span class="bg-tag6">對方取消議價</span></td>
                                  <td>A00002</td>
                                  <td>2016/11/11 19:00:00</td>
                                  <td>2300萬</td>
                                  <td>2300萬</td>
                                  <td>10,000</td>
                                  <td></td>
                              </tr>
                          </tbody>
                        </table>
                    </div> <!-- object-list -->
                </div><!-- tab-content -->


                <div id="tab3" class="tab-content" style="display: none;"> <!-- 議價成功 -->
                    <div class="object-list forbtm">
                        <div class="box">
                            <div class="imgs swiper-container swiper-container-horizontal">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide swiper-slide-active" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                    <div class="swiper-slide swiper-slide-next" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                    <div class="swiper-slide" style="width: 300px; margin-right: 30px;"><img src="{{ url('/misc/images/sample1.jpg') }}"></div>
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev swiper-button-disabled"></div>
                            </div>
                            <div class="info">
                                <h2 class="object-name">昇陽大地黃金套房*麗山國中學區*</h2>
                                <h3 class="object-add">台北市北投區行義路</h3>
                                <div class="detail">
                                    <span>72.34坪(含車位4.75坪)</span>
                                    <span>32年</span>
                                    <span>電梯大樓</span>
                                    <div>3房/3廳/2衛/1室(加蓋：3房/3廳/2衛/1室)</div>
                                    <div><span>6F-7F/B2-6F</span><span>點閱數：500</span></div>
                                </div>
                                <div class="btns">
                                <!--<span class="bg-sucsess-tag1">預約中</span>
                                <span class="bg-sucsess-tag2">2016年10月10日已完成</span>
                                <span class="bg-sucsess-tag3">賣方未履行簽約義務</span>-->
                                <span class="bg-sucsess-tag3">買方未履行簽約義務</span>
                                </div>
                            </div>
                            <div class="price">
                                <div class="oprice">4,580萬</div>
                                <div class="cprice"><span>4,188</span>萬</div>
                                <div><span class="down"><span class="icon-down-w"></span>8.56%</span></div>
                                <div class="txt-ora">(含車位價)</div>
                                <div>40萬/坪</div>
                            </div>
                        </div><!-- box -->

                        <table class="table-default">
                          <thead>
                              <tr>
                                  <th>議價編號</th>
                                  <th>成功時間</th>
                                  <th>雙方議定價格</th>
                                  <th>保證金金額</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <td>A00001</td>
                                  <td>2016/11/11 19:00:00</td>
                                  <td class="txt-b txt-ora">2300萬</td>
                                  <td>10,000</td>
                              </tr>
                          </tbody>
                        </table>
                    </div> <!-- object-list -->
                </div><!-- tab-content -->

            </div><!-- tab-container -->
        </div>

    </div>
</div>
</div>