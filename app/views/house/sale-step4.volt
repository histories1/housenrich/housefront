{% include "partials/saler-nav.volt" %}

<div class="sell-container">

    <div class="row sell-bg4">
        <div class="col-7">
            <div class="success-box">
                <div class="check-circle">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="60.594px" height="29.714px" viewBox="0 0 90.594 59.714" enable-background="new 0 0 90.594 59.714" xml:space="preserve">
                      <polyline class="check" fill="none" stroke="#fff" stroke-width="10" stroke-miterlimit="10" points="1.768,23.532 34.415,56.179 88.826,1.768"/>
                    </svg>
                </div>
                <div class="txt">
                    <span class="txt-gray txt-s">Congratulations!</span>
                    <h1>您已完成賣房申請！</h1>
                    <p>您的賣方申請已送出，好多房會盡快與您聯絡。</p>
                </div>
            </div>
        </div>
    </div><!-- row -->

</div>

<script>
$(function(){
    $('.check').css('stroke-dashoffset', 0);
});
</script>