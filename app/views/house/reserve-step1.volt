<div class="container bg-gray">
<div class="reserve">
    <h1 class="title txt-center">確認預約資訊</h1>
    <h3 class="txt-gray txt-center">感謝您預約看屋，請再次確認您的聯絡資訊！</h3>
    <form action="{{ url('/reserve/2') }}" method="post">
    <input type="hidden" name="peopleId" value="{{ this.session.get('USER')['People']['peopleId'] }}" />

        <div class="m-t-m m-b-m">
            <input name="name" type="text" class="input-default" placeholder="姓名" value="{{ this.session.get('USER')['PeopleInformation']['fullName'] }}">
            <input name="" type="text" class="input-default" placeholder="電話" value="{{ this.session.get('USER')['PeopleInformation']['account'] }}">
        </div>
        <div>
            <h3>預約清單</h3>

            {{ this.session.get('USER')['RESERVES']['html'] }}

        </div>
        <div class="txt-center">
           <a href="/map" class="btn btn-gray btn-m">返回</a>
           <button type="submit" class="btn btn-orange btn-m">確認</button>
        </div>
    </form>
</div>
</div>

<script>
$(function(){
    optReserveClose_click('{{ url('/map/setreserve') }}');
});
</script>