{{ partial("house/buy-searchnav") }}

<div class="container bg-gray">

    <div class="search-row">

        <div class="search-default">
        <form class="search-form" method="post">
            <input name="keyword" type="text" placeholder="請輸入關鍵字或物件編號" class="input-txt" />
            {% if session.SEARCH['display']['fm'] !== 'mrt' %}
                {{ sform.renderArea() }}
            {% endif %}

            {% if session.SEARCH['display']['fm'] === 'mrt' %}
            {{ sform.renderMrt() }}
            {# 若是社區搜尋顯示社區搜尋欄位#}
            {% elseif session.SEARCH['display']['fm'] === 'community' %}
            <div class="option-btn">
            <input name="" type="text" placeholder="社區名稱" class="input-txt-c">
            </div>
            {# 若是學區搜尋顯示學級跟學校 #}
            {% elseif session.SEARCH['display']['fm'] === 'school' %}
                {{ sform.renderSchoolType() }}
                {{ sform.renderSchool() }}
            {% endif %}
            <!--總價-->
            {{ sform.renderPrice() }}
            <!--進階-->
            {{ sform.renderAdvance() }}

            <button type="submit" class="btn-sent" name="lq" value="1">搜尋</button>

        </form>
        </div>

        {% if session.SEARCH['display']['fm'] === 'community' %}
        {{ partial("house/result-communityblock") }}
        {% endif %}

        {{ partial("house/result-modeswitch") }}

        {{ partial("house/result-"~session.SEARCH['display']['mod']) }}
    </div>

</div>