{% set searchmenus = [
        {
            'label' : "條件搜尋",
            'identify': 'default'
        },
        {
            'label' : "捷運搜尋",
            'identify': 'mrt'
        },
        {
            'label' : "社區搜尋",
            'identify': 'community'
        },
        {
            'label' : "學區搜尋",
            'identify': 'school'
        },
        {
            'label' : "地圖搜尋",
            'identify': 'map'
        }
    ]
%}
<div class="search-mode">
    <div class="row">
        {% for data in searchmenus %}
            {% if session.SEARCH['display']['fm'] is data['identify'] %}
            {% set apclass = 'current' %}
            {% else %}
            {% set apclass = '' %}
            {% endif %}
        <a class="{{ apclass }}" href="?fm={{ data['identify'] }}">{{ data['label'] }}</a>
        {% endfor %}
    </div>
</div>