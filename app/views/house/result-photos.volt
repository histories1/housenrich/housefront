<div class="m-t-s m-b-s">共 <span class="txt-red">{{houses.total_items}}</span> 個結果</div>

<div class="object-img-list">

{% for i, house in houses.items %}
<a href="#" class="box">
    <div class="imgs swiper-container">
        {% for i, media in house.RichitemMedia if media is defined %}
            <div class="swiper-slide"><img src="/file/access/RichitemMedia/{{media.UUID}}" /></div>
        {% elsefor %}
            <div class="swiper-slide"><img src="/misc/images/sample1.jpg"></div>
            <div class="swiper-slide"><img src="/misc/images/sample2.jpg"></div>
            <div class="swiper-slide"><img src="/misc/images/sample3.jpg"></div>
        {% endfor %}
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
    <div class="info">
        <h2 class="object-name">{{house.title}}</h2>
        <h3 class="object-add">{{house.addressCity}}{{house.addressDistrict}}{{house.addressRoad}}</h3>
        <div class="detail">
            <span>3房</span>
            <span>{{house.totalArea}}坪{%if house.parkingArea is defined%}(含車位{{house.parkingArea}}坪){% endif %}</span>
            <span>{{house.houseAgeYear}}年{{house.houseAgeMonth}}月</span>
        </div>
    </div>
    <div class="price">
        {%if house.INFOS.priceRichitem is defined%}<div class="cprice"><span>{{house.INFOS.priceRichitem}}</span>萬</div>{% endif %}
        <div><span class="down"><span class="icon-down-w"></span>0.00%</span></div>
    </div>
</a>
{% endfor %}

</div>
