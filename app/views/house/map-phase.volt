<div class="map-mode">
    {% include "map/search.volt" %}

    <div class="map-func">
        <div class="dis">
        {% for index,item in funcdic %}
        <a id="{{item['id']}}" class="{{item['class']}}" href="javascript:;">{{item['label']}}</a>
        {% endfor %}
        </div>
        <div class="mode" style="display:none;">
            <a href="javascript:;" class="active">
                <img src="/misc/images/icon-sort-map.png" width="16" height="16"><span>地圖模式</span>
            </a>
            <a href="javascript:;">
                <img src="/misc/images/icon-sort-list.png" width="16" height="16"><span>列表模式</span>
            </a>
        </div>
    </div>
</div>

<div class="map-container">

    <div id="gmap" style="width:100%; height:100vh;">
        <div class="icon-map1" style="position:absolute; top:200px; left:500px;">2100萬/2房</div>
        <div class="icon-map1" style="position:absolute; top:600px; left:200px;">2100萬/2房</div>
        <div class="icon-map1" style="position:absolute; top:500px; left:480px;">2100萬/2房</div>
        <div class="icon-map2" style="position:absolute; top:550px; left:400px;">4筆物件</div>
        <div class="icon-map2" style="position:absolute; top:350px; left:600px;">4筆物件</div>
        <div class="icon-map2" style="position:absolute; top:650px; left:650px;">4筆物件</div>
    </div>

    <div class="side-panel">
        {% include "map/list.volt" %}
    </div>

</div>

<script type="text/javascript">
function _initMap() {
    gMapObj = new google.maps.Map(
      document.getElementById('gmap'),
      {
        center: {lat: -34.397, lng: 150.644},
        zoom: 10,
        streetViewControl: false,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );

    /**
     * 從地圖點擊時抓取經緯度
     * */
    gMapObj.addListener("click", function(event){
        _map_add_marker(event.latLng, 1);
        _map_update_latlng(event.latLng);
    });
}

$(function(){
    // 儲存座標點
    // _map_save_geomarker();
    //object map scroll
    $(".side-panel").mCustomScrollbar();

    $(".xxbtn").click(function(){
      $(".c-box").hide();
    });
});
</script>