{% set searchmodes = [
        {
            'label' : "列表模式",
            'img'   : "icon-sort-list.png",
            'identify'     : 'lists'
        },
        {
            'label' : "圖片模式",
            'img'   : "icon-sort-img.png",
            'identify'     : 'photos'
        },
        {
            'label' : "地圖模式",
            'img'   : "icon-sort-map.png",
            'identify'     : 'map'
        }
    ]
%}
{%  set sortmodes = [
        {
            'label' : "上架日期",
            'identify'     : 'initTime'
        },
        {
            'label' : "總價",
            'identify'     : 'priceRichitem'
        },
        {
            'label' : "單價",
            'identify'     : 'priceSingle'
        },
        {
            'label' : "坪數",
            'identify'     : 'areaRight'
        },
        {
            'label' : "屋齡",
            'identify'     : 'houseAge'
        },
        {
            'label' : "點閱數",
            'identify'     : 'pageviews'
        }
    ]
%}
<div class="search-sort">
    <div class="left">
        {% for data in searchmodes %}
            {% if session.SEARCH['display']['mod'] is defined and
                  session.SEARCH['display']['mod'] is data['identify'] %}
            {% set apclass = 'active' %}
            {% else %}
            {% set apclass = '' %}
            {% endif %}
        <a class="mode-ico {{ apclass }}" href="?mod={{ data['identify'] }}">
        <img src="/misc/images/{{ data['img'] }}" alt="{{ data['label'] }}" width="16" height="16" /> {{ data['label'] }}</a>
        {% endfor %}
    </div>
    <div class="right">
        <span>排列方式依：</span>
        {% set icon = '' %}
        {% set srtmod = 'ASC' %}
        {% for data in sortmodes %}
            {% if session.SEARCH['display']['srt'] is defined and
                  session.SEARCH['display']['srt'] is data['identify'] %}
                {% set apclass = 'current' %}
                {% if session.SEARCH['display']['srtmod'] is defined and
                      session.SEARCH['display']['srtmod'] is 'ASC' %}
                {% set icon = '<i class="icon-up-w"></i>' %}
                {% set srtmod = 'DESC' %}
                {% else %}
                {% set icon = '<i class="icon-down-w"></i>' %}
                {% set srtmod = 'ASC' %}
                {% endif %}
            {% else %}
            {% set apclass = '' %}
            {% endif %}
        <a href="?srt={{ data['identify'] }}&srtmod={{ srtmod }}" class="sort-ico {{ apclass }}">{{ data['label'] }}{{ icon }}</a>
        {% endfor %}
    </div>
</div>