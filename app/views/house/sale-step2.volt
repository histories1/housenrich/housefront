{% include "partials/saler-nav.volt" %}

<div class="sell-container">

    <div class="row sell-bg2">
        <div class="col-7">
            <div class="sell-title">
                <span class="txt-gray txt-s">step2</span>
                <h1>請選擇您的物件類別</h1>
            </div>
            <form class="form-default form-validation" method="post" action="{{ url('saler/step3') }}">
                {{ form.render('csrf') }}

                <div class="radio-style2 radio-big">
                    <label for="type1">
                    <input type="radio" id="type1" name="type" value="房屋" checked /><span>房屋</span>
                    </label>
                </div>
                <div>
                    {{ form.renderGroups(["addressCity",
                                            "addressDistrict",
                                            "addressRoad",
                                            "addressLane",
                                            "addressAlley",
                                            "addressNo",
                                            "addressNoEx",
                                            "addressFloor",
                                            "addressFloorEx"],
                                            "\\Housefront\\Forms\\Decorators\\Addresses") }}
                </div>
                <div class="dash-line m-b-s m-t-s"></div>
                <div>
                    房屋：{{ form.render('areaHouse') }}坪，
                    車位：{{ form.render('areaParking') }}坪
                </div>

                <div class="radio-style2 radio-big">
                    <label for="type2">
                    <input type="radio" id="type2" name="type" value="其他" /><span>其他（土地或車位）</span>
                    </label>
                </div>
                <div class="c-line"></div>
                <div class="txt-right">
                    <a href="{{ url('saler/step1') }}" class="btn btn-default btn-m">返回</a>
                    <button type="submit" class="btn btn-blue btn-m">繼續</button>
                </div>
            </form>
        </div>
    </div><!-- row -->

</div>

<script>
$(function(){
    {% if session.SALER['addressCity'] is defined %}
    _depend_city_withdistrict( $("#addressCity"), 1 );
    {% endif %}

    {% if session.SALER['addrCity'] is defined %}
    _depend_city_withdistrict( $("#addrCity"), 1 );
    {% endif %}
});
</script>
