<div class="object-comm-list">
    <div class="box">
        <div class="imgs swiper-container">
                <div id="slider" class="flexslider">
                    <ul class="slides">
                        <li><img src="/misc/images/sample1.jpg" /></li>
                        <li><img src="/misc/images/sample2.jpg" /></li>
                        <li><img src="/misc/images/sample3.jpg" /></li>
                        <li><img src="/misc/images/sample1.jpg" /></li>
                        <li><img src="/misc/images/sample2.jpg" /></li>
                    </ul>
                  </div>
                  <div id="carousel" class="flexslider">
                    <ul class="slides">
                        <li><img src="/misc/images/sample1.jpg" /></li>
                        <li><img src="/misc/images/sample2.jpg" /></li>
                        <li><img src="/misc/images/sample3.jpg" /></li>
                        <li><img src="/misc/images/sample1.jpg" /></li>
                        <li><img src="/misc/images/sample2.jpg" /></li>
                    </ul>
                </div>
        </div>
        <div class="info">
            <h2 class="object-name">皇祥玉鼎</h2>
            <h3 class="object-add">台北市中山區南京東路二段142巷2號</h3>
            <p class="txt-b">平均出售單價：<span class="cprice txt-red">50萬/坪</span></p>
            <pclass="txt-b">出售中案件數：23件</p>
            <p class="txt-b">完成日期：104年8月30日</p>
            <div class="detail">
                <span>主要建材：鋼筋混凝土</span>
                <span>建設公司：吉軒建設</span>
                <span>總層數：B3 ~ 15</span>
                <span>基地面積：2400平方公尺</span>
                <span>棟數：3</span>
                <span>總戶數：240</span>
                <span>路寬：8m</span>
                <span>1F現況：店面</span>
            </div>
            <div>最近捷運站：松江南京站 一號出口 600M</div>
            <div>公共設施：會議室、游泳池、健身房、中庭花園</div>
            <div>簡介：這是簡介，這是簡介這是簡介，這是簡介這是簡介，這是簡介<br />這是簡介，這是簡介這是簡介，這是簡介這是簡介，這是簡介。</div>
        </div>

    </div><!-- box -->
</div><!-- object-list -->