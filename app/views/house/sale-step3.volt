{% include "partials/saler-nav.volt" %}

<div class="sell-container">

    <div class="row sell-bg3">
        <div class="col-7">
            <div class="sell-title">
                <span class="txt-gray txt-s">step3</span>
                <h1>請填寫您的物件價格</h1>
            </div>
            <form class="form-default form-validation" method="post" action="{{ url('saler/step4') }}">
                {{ form.render('csrf') }}

                <div class="field">
                    <div class="col-2 field-label">價格</div>
                    <div class="col-10">
                        <div class="input-money"><i>$</i>
                        {{ form.render('price') }}<span>萬元</span></div>
                    </div>
                </div>
                <div class="field">
                    <div class="col-2 field-label">社區名稱</div>
                    <div class="col-10">
                    {{ form.render('community') }}
                    </div>
                </div>

                <h1 class="m-t-m">附近行情參考</h1>
                <div class="quotes">

                    <div class="imgs"><img src="/misc/images/icon-quotes1.png" ></div>
                    <div class="price">
                        <span class="txt-b">大樓</span><span class="txt-gray">成交平均</span><br />
                        <span class="txt-b"><span class="txt-red font22">71.8</span>萬/坪</span>
                    </div>
                    <div class="imgs"><img src="/misc/images/icon-quotes2.png" ></div>
                    <div class="price">
                        <span class="txt-b">公寓</span><span class="txt-gray">成交平均</span><br />
                        <span class="txt-b"><span class="txt-red font22">55.3</span>萬/坪</span>
                    </div>
                    <div class="imgs"><img src="/misc/images/icon-quotes3.png" ></div>
                    <div class="price">
                        <span class="txt-b">近半年趨勢</span><br />
                        <span class="txt-b">
                        <img src="/misc/images/icon-rise.png" width="16" height="16" >
                        漲幅：<span class="txt-red font22">100%</span></span>
                    </div>
                </div>
                <div class="c-line"></div>
                <div class="txt-right">
                    <a href="{{ url('saler/step2') }}" class="btn btn-default btn-m">返回</a>
                    <button type="submit" class="btn btn-blue btn-m">繼續</button>
                </div>
            </form>
        </div>
    </div><!-- row -->

</div>