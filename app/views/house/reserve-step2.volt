<div class="container bg-gray">
    <div class="reserve">
        <div class="check-circle">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="60.594px" height="29.714px" viewBox="0 0 90.594 59.714" enable-background="new 0 0 90.594 59.714" xml:space="preserve">
              <polyline class="check" fill="none" stroke="#fff" stroke-width="10" stroke-miterlimit="10" points="1.768,23.532 34.415,56.179 88.826,1.768"/>
            </svg>
        </div>
        <script>
        $(document).ready(function () {
              $('.check').css('stroke-dashoffset', 0);
        });
        </script>

        <div class="txt-center">
            <h1 class="title">完成預約！</h1>
            <h3 class="m-b-s">您已成功預約，24H內會與您電話聯繫</h3>
            <p class="txt-gray">您預約的物件已加入收藏。</p>
            <div class="m-t-m">
                <a href="{{ url('/map') }}" class="btn btn-gray">繼續搜尋物件</a>
                <a href="javascript:;" class="btn btn-gray">前往收藏列表</a>
            </div>
        </div>

    </div>
</div>