{% include "partials/saler-nav.volt" %}

<div class="sell-container">

    <div class="row sell-bg1">
        <div class="col-7">
            <div class="sell-title">
                <span class="txt-gray txt-s">step1</span>
                <h1>請輸入您的個人資料</h1>
            </div>
            <form class="form-default form-validation" method="post" action="{{ url('saler/step2') }}">
                {{ form.render('csrf') }}

                <div class="field">
                    <div class="col-2 field-label">姓名</div>
                    <div class="col-10">
                        {{ form.render('fullName') }}
                    </div>
                </div>
                <div class="field">
                    <div class="col-2 field-label">性別</div>
                    <div class="col-10">
                        {{ form.render('gender') }}
                    </div>
                </div>
                <div class="field">
                    <div class="col-2 field-label">手機號碼</div>
                    <div class="col-10">
                        {{ form.render('cellphone') }}
                    </div>
                </div>
                <div class="c-line"></div>
                <div class="txt-right">
                    <button type="submit" class="btn btn-blue btn-m">繼續</button>
                </div>
            </form>
        </div>
    </div><!-- row -->

</div>