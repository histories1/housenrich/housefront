<?php

namespace Housefront\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class PeopleInvoiceForm extends \Personalwork\Forms\Form
{

	/**
	 * @Comment("買受人/抬頭")
	 */
	private function _Title() {
		$element = new \Personalwork\Forms\Elements\Text("title");
		$element->setAttributes(array(
					'class' => 'input-default',
				));
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		return $element;
	}

	/**
	 * @Comment("統一編號")
	 */
	private function _No() {
		$element = new \Personalwork\Forms\Elements\Text("NO");
		$element->setAttributes(array(
					'class' => 'input-default',
				));
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("聯絡電話")
	 */
	private function _Phone() {
		$element = new \Personalwork\Forms\Elements\Text("phone");
		$element->setAttributes(array(
					'class' => 'input-default',
				));
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}

	/**
	 * @Comment("收件地址")
	 */
	private function _Address() {
		$element = new \Personalwork\Forms\Elements\Text("address");
		$element->addValidator(new StringLength([
			"max" => 100
		]));
		return $element;
	}

	/**
	 * @Comment("地址(縣市)")
	 */
	private function _Addresscity() {
		$element = new \Personalwork\Forms\Elements\Select("addressCity");
		// $element->setLabel("地址(縣市)");
		$element->setAttributes(array(
					"class" => "input-default m select-append depend-city",
					"data-target" => ".addrDistrict",
				));
		$city = \Houserich\Models\Fieldoptions::findByFieldname("縣市");
		$items = array(''=>'請選擇');
		foreach($city as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(行政區)")
	 */
	private function _Addressdistrict() {
		$element = new \Personalwork\Forms\Elements\Select("addressDistrict");
		// $element->setLabel("地址(行政區)");
		$element->setAttributes(array(
					"class" => "input-default m addrDistrict hide",
				));
		$district = \Houserich\Models\Fieldoptions::findByFieldname("行政區");
		$opt = array( ["value" => '',"label"=> "請選擇"] );
		foreach ($district as $item) {
			$opt[] = ["label"=>$item->label,
					  "value"=>$item->value,
					  "data-city"=>$item->parentLabel];
		}
		$element->setOptions($opt);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(路)")
	 */
	private function _Addressroad() {
		$element = new \Personalwork\Forms\Elements\Text("addressRoad");
		$element->setAttributes(array(
					"class" => "input-default m",
					"placeholder"=> "路/段/道(含段)名"
				));
		$element->addValidator(new StringLength([
			"max" => 20,
			"message"=>"地址路名欄位長度超過15字元限制。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(巷)")
	 */
	private function _Addresslane() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressLane");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("巷");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址巷欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(弄)")
	 */
	private function _Addressalley() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressAlley");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("弄");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址弄欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號)")
	 */
	private function _Addressno() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNo");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("號");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號之)")
	 */
	private function _Addressnoex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNoEx");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號之欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓)")
	 */
	private function _Addressfloor() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloor");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("樓");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓之)")
	 */
	private function _Addressfloorex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloorEx");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓之欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
     * This method returns the default value for field 'csrf'
     */
    public function getCsrf()
    {
        return $this->security->getToken();
    }

	public function initialize() {
		$id = new \Phalcon\Forms\Element\Hidden("PeopleId");
		$this->add($id);

		$this->add($this->_Title());
		$this->add($this->_No());
		$this->add($this->_Phone());
		$this->add($this->_Address());
		$this->add($this->_Addresscity());
		$this->add($this->_Addressdistrict());
		$this->add($this->_Addressroad());
		$this->add($this->_Addresslane());
		$this->add($this->_Addressalley());
		$this->add($this->_Addressno());
		$this->add($this->_Addressnoex());
		$this->add($this->_Addressfloor());
		$this->add($this->_Addressfloorex());

		// Add a text element to put a hidden CSRF
        $this->add( new \Phalcon\Forms\Element\Hidden( "csrf" ) );
	}
}
