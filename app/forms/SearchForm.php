<?php
/**
 * 處理所有搜尋欄位
 * 1. 一般條件
 * */
namespace Housefront\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class SearchForm extends \Personalwork\Forms\Form
{
    var $html;


    /**
     * @Comment("地址(縣市)")
     */
    private function _Addresscity() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("addressCity[]");
        $element->setLabel("縣市");
        $element->setAttributes(array(
                    "data-disabled" => ".addressDistrict",
                ));
        $city = \Houserich\Models\Fieldoptions::findByFieldname("縣市");
        $items = array();
        foreach($city as $i=> $item){
            $items[] = ["id" => "city{$i}",
                        "label"=>$item->label,
                        "value"=>$item->value,
                        "class"=>null];
        }
        $element->setUserOptions(array(
                    "label-class" => "col-2 txt-ora",
                    "parent-class" => "col-10 check-style2",
                    "items" => $items
                ));
        return $element;
    }

    /**
     * @Comment("地址(行政區)")
     */
    private function _Addressdistrict() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("addressDistrict[]");
        $element->setLabel("行政區");
        $element->setAttributes(array(
                ));
        $district = \Houserich\Models\Fieldoptions::findByFieldname("行政區");
        $opt = array();
        foreach ($district as $i => $item) {
            $opt[] = ["id"=>"district{$i}",
                      "label"=>$item->label,
                      "value"=>$item->value,
                      "class"=>'hide',
                      "data-city"=>$item->parentLabel];
        }
        $element->setUserOptions(array(
                    "label-class" => "col-2 txt-ora",
                    "parent-class" => "col-10 check-style2",
                    "items" => $opt
                ));
        return $element;
    }


    /**
     * 產出「一般條件：地區」
     * */
    public function renderArea()
    {
        $this->html = '<div class="search-option">';
        $this->html .= '<a href="javascript:" class="option-btn">地區 <span class="icon-down-b"></span></a>';

        $this->html .= '<div class="option-panel">';
            $this->html.= '<div class="field">';
            $this->html.= $this->_Addresscity()->renderHousenrich();
            $this->html.= '</div>';
            $this->html.= '<div class="dash-line"></div>';
            $this->html.= '<div class="field">';
            $this->html.= $this->_Addressdistrict()->renderHousenrich();
            $this->html.= '</div>';
        $this->html.= '</div>';

        $this->html.= '</div>';

        return $this->html;
    }


    /**
     * @Comment("售價1")
     */
    private function _Price1() {
        $element = new \Personalwork\Forms\Elements\Select("price1");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "400" => "400", "800" => "800", "1200" => "1200", "1600" => "1600", "2000" => "2000", "2600" => "2600", "3200" => "3200", "4000" => "4000", "5000" => "5000" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("售價2")
     */
    private function _Price2() {
        $element = new \Personalwork\Forms\Elements\Select("price2");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "400" => "400", "800" => "800", "1200" => "1200", "1600" => "1600", "2000" => "2000", "2600" => "2600", "3200" => "3200", "4000" => "4000", "5000" => "5000" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * 產出「一般條件：總價」
     * */
    public function renderPrice()
    {
        $this->html = '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">總價 <span class="icon-down-b"></span></a>';

        $this->html.= '<div class="option-panel">';
        $this->html.= '<div class="field">
                        <div class="col-2 txt-ora">總價</div>
                        <div class="col-10 forcombo">';
        $this->html.= $this->_Price1()->render();
        $this->html.= '~';
        $this->html.= $this->_Price2()->render();
        $this->html.= '萬
                        </div>
                       </div>
                       </div>';

        $this->html.= '</div>';

        return $this->html;
    }


    /**
     * @Comment("用途")
     */
    private function _Usefor() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("usefor[]");
        $element->setLabel("用途");
        $usefor = \Houserich\Models\Fieldoptions::findByFieldname("用途");
        $opt = array();
        foreach ($usefor as $i => $item) {
            if( $item->label != '其他' ){
                $opt[] = ["id"=>"usefor{$i}",
                          "label"=>$item->label,
                          "value"=>$item->value,
                          "class"=>null];
            }
        }
        $element->setUserOptions(array(
                    "label-class" => "col-2 txt-ora",
                    "parent-class" => "col-10 check-style2",
                    "items" => $opt
                ));
        return $element;
    }

    /**
     * @Comment("型態")
     */
    private function _Type() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("type[]");
        $element->setLabel("型態");
        $item_type = \Houserich\Models\Fieldoptions::findByFieldname("型態");
        $opt = array();
        foreach($item_type as $i => $item){
            if( $item->label != '其他' ){
                $opt[] = ["id"=>"type{$i}",
                          "label"=>$item->label,
                          "value"=>$item->value,
                          "class"=>null];
            }
        }
        $element->setUserOptions(array(
                    "label-class" => "col-2 txt-ora",
                    "parent-class" => "col-10 check-style2",
                    "items" => $opt
                ));
        return $element;
    }


    /**
     * @Comment("單價1")
     */
    private function _SinglePrice1() {
        $element = new \Personalwork\Forms\Elements\Select("singleprice1");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "10"=>"10", "20"=>"20", "30"=>"30", "40"=>"40", "50"=>"50", "60"=>"60", "70"=>"70" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("單價2")
     */
    private function _SinglePrice2() {
        $element = new \Personalwork\Forms\Elements\Select("singleprice2");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "10"=>"10", "20"=>"20", "30"=>"30", "40"=>"40", "50"=>"50", "60"=>"60", "70"=>"70" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("坪數1")
     */
    private function _Area1() {
        $element = new \Personalwork\Forms\Elements\Select("area1");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "15"=>"15", "25"=>"25", "35"=>"35", "45"=>"45", "55"=>"55" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("坪數2")
     */
    private function _Area2() {
        $element = new \Personalwork\Forms\Elements\Select("area2");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "15"=>"15", "25"=>"25", "35"=>"35", "45"=>"45", "55"=>"55" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("使用坪數1")
     */
    private function _Areause1() {
        $element = new \Personalwork\Forms\Elements\Select("areause1");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "15"=>"15", "25"=>"25", "35"=>"35", "45"=>"45", "55"=>"55" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("使用坪數2")
     */
    private function _Areause2() {
        $element = new \Personalwork\Forms\Elements\Select("areause2");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "15"=>"15", "25"=>"25", "35"=>"35", "45"=>"45", "55"=>"55" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("樓層1")
     */
    private function _Floor1() {
        $element = new \Personalwork\Forms\Elements\Select("floor1");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "1"=>"1", "2"=>"2", "3"=>"3", "5"=>"5", "8"=>"8", "12"=>"12" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("樓層2")
     */
    private function _Floor2() {
        $element = new \Personalwork\Forms\Elements\Select("floor2");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "1"=>"1", "2"=>"2", "3"=>"3", "5"=>"5", "8"=>"8", "12"=>"12" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("屋齡1")
     */
    private function _Houseage1() {
        $element = new \Personalwork\Forms\Elements\Select("houseage1");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "5"=>"5", "10"=>"10", "15"=>"15", "20"=>"20", "25"=>"25", "30"=>"30", "35"=>"35", "40"=>"40" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("屋齡2")
     */
    private function _Houseage2() {
        $element = new \Personalwork\Forms\Elements\Select("houseage2");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $opt = array( ""=>"", "5"=>"5", "10"=>"10", "15"=>"15", "20"=>"20", "25"=>"25", "30"=>"30", "35"=>"35", "40"=>"40" );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("格局")
     */
    private function _Pettern() {
        $element = new \Personalwork\Forms\Elements\RadioGroup("pettern");
        $opt = array(
                  [
                    "id"=>"pettern1",
                    "label"=>"開放式",
                    "value"=>"開放式"
                  ],
                  [
                    "id"=>"pettern2",
                    "label"=>"套房",
                    "value"=>"套房"
                  ]
               );
        $element->setUserOptions(array("items" => $opt));
        return $element;
    }

    /**
     * @Comment("格局")
     */
    private function _Petternrooms() {
        $element = new \Personalwork\Forms\Elements\RadioGroup("petternrooms");

        $opt = array();
        for($i=1; $i<6;$i++){
            $postfix=null;
            if( $i == 5){ $postfix='以上';}
            $opt[] = ["id"=>"petternrooms{$i}", "label"=>"{$i}房{$postfix}", "value"=>"{$i}房"];
        }
        $element->setUserOptions(array("items" => $opt));
        return $element;
    }

    /**
     * @Comment("車位")
     */
    private function _Parking() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("parking");
        $element->setLabel("車位");
        $item_type = \Houserich\Models\Fieldoptions::findByFieldname("車位種類");
        $opt = array();
        foreach($item_type as $i => $item){
                $opt[] = ["id"=>"parking{$i}",
                          "label"=>$item->label,
                          "value"=>$item->value,
                          "class"=>null];
        }
        $element->setUserOptions(array(
                    "label-class" => "col-2 txt-ora",
                    "parent-class" => "col-10 check-style2",
                    "items" => $opt
                ));
        return $element;
    }

    /**
     * @Comment("公設比")
     */
    private function _Ratiopublicarea() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("ratiopublicarea");
        $element->setLabel("公設比");
        $opt = array(
                  [
                    "id"=>"ratiopublicarea1",
                    "label"=>"0 ~ 10%",
                    "value"=>"10%",
                    "class"=>null
                  ],
                  [
                    "id"=>"ratiopublicarea2",
                    "label"=>"10% ~ 20%",
                    "value"=>"20%",
                    "class"=>null
                  ],
                  [
                    "id"=>"ratiopublicarea3",
                    "label"=>"20% ~ 30%",
                    "value"=>"30%",
                    "class"=>null
                  ]
               );
        $element->setUserOptions(array(
                    "label-class" => "col-2 txt-ora",
                    "parent-class" => "col-10 check-style2",
                    "items" => $opt
                ));
        return $element;
    }

    /**
     * @Comment("朝向")
     */
    private function _Faceto() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("faceto[]");
        $opt = array(
                  [
                    "id"=>"faceto_building",
                    "label"=>"大樓",
                    "value"=>"大樓",
                    "class"=>null
                  ],
                  [
                    "id"=>"faceto_door",
                    "label"=>"大門",
                    "value"=>"大門",
                    "class"=>null
                  ],
                  [
                    "id"=>"faceto_window",
                    "label"=>"客廳窗",
                    "value"=>"客廳窗",
                    "class"=>null
                  ]
               );
        $element->setUserOptions(array(
                    "parent-class" => "check-style2 siwtch-faceto",
                    "items" => $opt
                ));
        return $element;
    }

    /**
     * @Comment("大樓朝向")
     */
    private function _FtBuilding() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("facetobuilding[]");
        $item = \Houserich\Models\Fieldoptions::findByFieldname("大樓朝向");
        $opt = array();
        foreach($item as $i => $item){
            if( empty($item->label) ){continue;}
            $opt[] = ["id"=>"facetobuilding{$i}",
                      "label"=>$item->label,
                      "value"=>$item->value,
                      "class"=>null];
        }
        $element->setUserOptions(array(
                    "parent-id" => "buildingBlock",
                    "parent-class" => "check-style2 hide",
                    "items" => $opt
                ));
        return $element;
    }

    /**
     * @Comment("大門朝向")
     */
    private function _FtDoor() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("facetodoor[]");
        $item = \Houserich\Models\Fieldoptions::findByFieldname("大門朝向");
        $opt = array();
        foreach($item as $i => $item){
            if( empty($item->label) ){continue;}
            $opt[] = ["id"=>"facetodoor{$i}",
                      "label"=>$item->label,
                      "value"=>$item->value,
                      "class"=>null];
        }
        $element->setUserOptions(array(
                    "parent-id" => "doorBlock",
                    "parent-class" => "check-style2 hide",
                    "items" => $opt
                ));
        return $element;
    }

    /**
     * @Comment("客廳窗朝向")
     */
    private function _FtWindow() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("facetowindow[]");
        $item = \Houserich\Models\Fieldoptions::findByFieldname("客廳窗朝向");
        $opt = array();
        foreach($item as $i => $item){
            if( empty($item->label) ){continue;}
            $opt[] = ["id"=>"facetowindow{$i}",
                      "label"=>$item->label,
                      "value"=>$item->value,
                      "class"=>null];
        }
        $element->setUserOptions(array(
                    "parent-id" => "windowBlock",
                    "parent-class" => "check-style2 hide",
                    "items" => $opt
                ));
        return $element;
    }

    /**
     * @Comment("其他條件")
     */
    private function _Others() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("other[]");
        $element->setLabel("其他條件");
        $item = \Houserich\Models\Fieldoptions::findByFieldname("延伸其他");
        $opt = array();
        foreach($item as $i => $item){
                $opt[] = ["id"=>"facetowindow{$i}",
                          "label"=>$item->label,
                          "value"=>$item->value,
                          "class"=>null];
        }
        $element->setUserOptions(array(
                    "label-class" => "col-2 txt-ora",
                    "parent-class" => "col-10 check-style2",
                    "items" => $opt
                ));
        return $element;
    }

    /**
     * 產出「一般條件：進階搜尋」
     * */
    public function renderAdvance()
    {
        $this->html = '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">進階搜尋 <span class="icon-down-b"></span></a>';

            $this->html.= '<div class="option-panel option-3">';
            $this->html.= '<div class="field">';
                $this->html.= $this->_Usefor()->renderHousenrich();
            $this->html.= '</div>';
            $this->html.= '<div class="dash-line"></div>';
            $this->html.= '<div class="field">';
                $this->html.= $this->_Type()->renderHousenrich();
            $this->html.= '</div>';
            $this->html.= '<div class="dash-line"></div>';
            $this->html.= '<div class="field">
                            <div class="col-2 txt-ora">單價</div>
                            <div class="col-10 forcombo">';
                $this->html.= $this->_SinglePrice1()->render().'~';
                $this->html.= $this->_SinglePrice2()->render().'萬/坪
                            </div>
                           </div>';
            $this->html.= '<div class="dash-line"></div>';
            $this->html.= '<div class="field">
                            <div class="col-2 txt-ora">坪數</div>
                            <div class="col-10">
                                <span class="forcombo ss">';
                    $this->html.= $this->_Area1()->render().'~';
                    $this->html.= $this->_Area2()->render().'坪
                                </span>
                                <span class="txt-ora m-l-m">使用坪數</span>
                                <span class="forcombo ss">';
                    $this->html.= $this->_Areause1()->render().'~';
                    $this->html.= $this->_Areause2()->render().'坪
                                </span>
                            </div>
                           </div>';
            $this->html.= '<div class="dash-line"></div>';
            $this->html.= '<div class="field">
                            <div class="col-2 txt-ora">樓層</div>
                            <div class="col-10">
                                <span class="forcombo ss">';
                    $this->html.= $this->_Floor1()->render().'~';
                    $this->html.= $this->_Floor2()->render().'樓
                                </span>
                                <span class="txt-ora m-l-m">屋齡</span>
                                <span class="forcombo ss">';
                    $this->html.= $this->_Houseage1()->render().'~';
                    $this->html.= $this->_Houseage2()->render().'年
                                </span>
                            </div>
                           </div>
                           <div class="dash-line"></div>
                           <div class="field">
                            <div class="col-2 txt-ora">格局</div>
                            <div class="col-10">';
                    $this->html.= $this->_Pettern()->renderHousenrich3();
                    $this->html.= $this->_Petternrooms()->renderHousenrich3();
            $this->html.= '</div>
                           </div>
                           <div class="dash-line"></div>
                           <div class="field">';
                $this->html.= $this->_Parking()->renderHousenrich();
            $this->html.= '</div>
                           <div class="dash-line"></div>
                           <div class="field">';
                $this->html.= $this->_Ratiopublicarea()->renderHousenrich();
            $this->html.= '</div>
                        <div class="dash-line"></div>
                        <div class="field">
                            <div class="col-2 txt-ora">朝向</div>
                            <div class="col-10">';
                $this->html.= $this->_Faceto()->renderHousenrich();
                $this->html.= $this->_FtBuilding()->renderHousenrich();
                $this->html.= $this->_FtDoor()->renderHousenrich();
                $this->html.= $this->_FtWindow()->renderHousenrich();
            $this->html.=  '</div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="field">';
                $this->html.= $this->_Distance()->renderHousenrich();
            $this->html.=  '</div>
                        <div class="dash-line"></div>
                        <div class="field">';
                $this->html.= $this->_Others()->renderHousenrich();
            $this->html.=  '</div>
                        <div class="txt-right"><button type="submit" class="btn btn-white txt-red" name="cq" value="1">清除所有條件</button></div>';
        $this->html.='</div>';

        $this->html.='</div>';

        return $this->html;
    }

    /**
     * @Comment("捷運路線")
     */
    private function _Mrt() {
        $element = new \Housefront\Forms\Elements\MrtLine("mrt[]");
        $element->setLabel("請選擇路線");
        $this->mrts = \Houserich\Models\Fieldoptions::findByFieldname("捷運路線");
        $opt = array();
        foreach($this->mrts as $i => $item){
            $opt[] = ["id"=>"line{$i}",
                      "label"=>$item->label,
                      "identify"=>$item->value];
        }
        $element->setUserOptions(array(
                    "label-class" => "col-2 txt-ora",
                    "parent-class" => "col-10 check-style2 font15",
                    "lines" => $opt
                ));
        return $element;
    }


    /**
     * 根據傳入路線名稱從mrt內取出站名排序
     * */
    private function _Mrtstation() {
        $element = new \Housefront\Forms\Elements\MrtLine("mrtstations[]");

        // 根據路線取得完整站名
        $linemap = array();
        foreach ($this->mrts as $mrtline) {
            $lines = \Houserich\Models\Mrt::find(["lineLabel like '%{$mrtline->label}%'", "group"=>["name"], "order"=>"lineId"]);
            $stations=array();
            foreach ($lines as $line) {
                preg_match("/{$mrtline->value}0?(\d+)/",$line->lineId, $num);
                if( count($num) > 0){
                    $station = $line->toArray();
                    $station['id'] = "{$mrtline->value}{$num[1]}";
                    $stations[intval($num[1])] = $station;
                }
            }
            // var_dump($stations);die();
            ksort($stations);
            $linemap[$mrtline->value] = $stations;
        }

        // 將所有站加入元素
        $element->setUserOptions(array(
                "label-class" => "col-2 txt-ora",
                "parent-class" => "col-10 check-style2 font15",
                "stations" => $linemap
            ));
        return $element;
    }


    /**
     * @Comment("捷運距離")
     * */
    private function _Distance() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("distance[]");
        $element->setLabel("捷運距離");
        $opt = array(
                  [
                    "id"=>"distance1",
                    "label"=>"0~0.5km",
                    "value"=>"500",
                    "class"=>null
                  ],
                  [
                    "id"=>"distance2",
                    "label"=>"0.5~1km",
                    "value"=>"1000",
                    "class"=>null
                  ],
                  [
                    "id"=>"distance3",
                    "label"=>"1~1.5km",
                    "value"=>"1500",
                    "class"=>null
                  ]
               );
        $element->setUserOptions(array(
                    "label-class" => "col-2 txt-ora",
                    "parent-class" => "col-10 check-style2",
                    "items" => $opt
                ));
        return $element;
    }


    /**
     * 產出「捷運搜尋：捷運站」
     * */
    public function renderMrt()
    {
        $this->html = '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">捷運站 <span class="icon-down-b"></span></a>';

            $this->html.= '<div class="option-panel forscroll">';
            $this->html.= '<div class="field">';
                $this->html.= $this->_Mrt()->renderLine();
            $this->html.= '</div>';
            $this->html.= '<div class="dash-line"></div>';

            foreach ($this->mrts as $mrtline) {
                $this->html.= $this->_Mrtstation()->renderStations();
            }
            $this->html.= '</div>';

        $this->html.= '</div>';

        $this->html.= '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">捷運距離 <span class="icon-down-b"></span></a>';
        $this->html.= '<div class="option-panel">
                        <div class="field">';
        $this->html.= $this->_Distance()->renderHousenrich();
        $this->html.= ' </div>
                       </div>';

        $this->html.= '</div>';

        return $this->html;
    }


    /**
     * 產出「學區搜尋：學級」
     * */
    public function renderSchoolType()
    {
        $this->html = '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">學級 <span class="icon-down-b"></span></a>';

        $this->html.= '<div class="option-panel">
                    <div class="field">
                        <div class="col-2 txt-ora">學級</div>
                        <div class="col-10 check-style2">
                            <label for="grade1"><input type="checkbox" id="grade1" name="grade"><span>國小</span></label>
                            <label for="grade2"><input type="checkbox" id="grade2" name="grade"><span>國中</span></label>
                        </div>
                    </div>
                </div>';

        $this->html.= '</div>';

        return $this->html;
    }

    /**
     * 產出「學區搜尋：學校」
     * */
    public function renderSchool()
    {
        $this->html = '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">學校 <span class="icon-down-b"></span></a>';

        $this->html.= '<div class="option-panel">
                    <div class="field">
                        <div class="col-2 txt-ora">學校</div>
                        <div class="col-10 check-style2">
                            <label for="school1"><input type="checkbox" id="school1" name="school"><span>大同國小</span></label>
                            <label for="school2"><input type="checkbox" id="school2" name="school"><span>大同國中</span></label>
                            <label for="school3"><input type="checkbox" id="school3" name="school"><span>大同國小</span></label>
                            <label for="school4"><input type="checkbox" id="school4" name="school"><span>大同國中</span></label>
                            <label for="school5"><input type="checkbox" id="school5" name="school"><span>大同國小</span></label>
                            <label for="school6"><input type="checkbox" id="school6" name="school"><span>大同國中</span></label>
                        </div>
                    </div>
                </div>';

        $this->html.= '</div>';

        return $this->html;
    }

    public function initialize() {

    }
}