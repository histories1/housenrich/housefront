<?php

namespace Housefront\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Email;

class PeopleInformationForm extends \Personalwork\Forms\Form
{
	/**
	 * @Comment("帳號")
	 */
	private function _Account() {
		$element = new \Personalwork\Forms\Elements\Text("account");

		$people = \Houserich\Models\People::findFirst( $this->getDI()->get('session')->get('REGI_PEOPLEID') );

		$element->setLabel("帳號")
				->setDefault($people->cellphone)
				->setAttributes([
					"class"		=> "input-default",
					"readonly"	=> true,
					"required"	=> "required"
				]);
		$element->addValidator(new StringLength([
			"max" => 15
		]));

		return $element;
	}

	/**
	 * @Comment("密碼")
	 */
	private function _Password() {
		$element = new \Phalcon\Forms\Element\Password("password");
		$element->setLabel("密碼")
				->setAttributes([
					"class"		=> "input-default",
					"required"	=> "required"
				]);
		return $element;
	}

	/**
	 * @Comment("姓名")
	 */
	private function _Fullname() {
		$element = new \Personalwork\Forms\Elements\Text("fullName");
		$element->setLabel("姓名")
				->setAttributes([
					"class"		=> "input-default",
					"required"	=> "required"
				]);
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}

	/**
	 * @Comment("身分證字號")
	 */
	private function _Id() {
		$element = new \Personalwork\Forms\Elements\Text("ID");
		$element->setLabel("身分證字號");
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		return $element;
	}


	/**
	 * @Comment("性別")
	 */
	private function _Gender() {
		$element = new \Personalwork\Forms\Elements\RadioGroup("gender");

		$items = [["id"=>'male',"label"=>'男',"value"=>'男'], ["id"=>'female',"label"=>'女',"value"=>'女']];
		$element->setLabel("性別")
				->setAttributes([
					"required"	=> "required"
				])
				->setUserOptions([
					"format"	=> "Housenrich",
					"items"		=> $items,
					"parent-class" => "radio-style",
					"label-class"	=> ''
				]);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("年齡")
	 */
	private function _Age() {
		$element = new \Personalwork\Forms\Elements\Numeric("age");
		$element->setAttributes([
					"class"		=> "input-default m m-r-s valid-price",
					"required"	=> "required"
				])
				->setUserOptions(array(
					"label-class" => "control-label",
					"postfix-label"=> "歲"
				));
		$element->addValidator(new StringLength([
			"max" => 10,
		]));
		return $element;
	}

	/**
	 * @Comment("婚姻")
	 */
	private function _Marriage() {
		$element = new \Personalwork\Forms\Elements\RadioGroup("marriage");

		$items = [["id"=>'marriage1',"label"=>'未婚',"value"=>'未婚'], ["id"=>'marriage2',"label"=>'已婚',"value"=>'已婚']];
		if( isset($this->getDI()->get('session')->get('USER')['PeopleInformation']) ){
		$element->setDefault( $this->getDI()->get('session')->get('USER')['PeopleInformation']['marriage'] );
		}
		$element->setUserOption("items", $items);
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}

	/**
	 * @Comment("成員人數")
	 */
	private function _Members() {
		$element = new \Personalwork\Forms\Elements\Numeric("members");
		$element->setAttributes([
					"class"	=> 'input-default xs',
				]);
		if( isset($this->getDI()->get('session')->get('USER')['PeopleInformation']) ){
		$element->setDefault( $this->getDI()->get('session')->get('USER')['PeopleInformation']['members'] );
		}
		$element->addValidator(new StringLength([
			"max" => 5
		]));
		return $element;
	}


	public function renderMarriageMembers() {
		$html = '<div class="radio-style2">';

		$radios = $this->_Marriage()->getUserOption('items');

		foreach ($radios as $i => $radio) {
			$checked = ($radio['value']==$this->_Marriage()->getValue())? 'checked="checked"' : '';
			$html.= '<label for="'.$radio['id'].'">';
			$html.= '<input type="radio" id="'.$radio['id'].'" name="'.$this->_Marriage()->getName().'" value="'.$radio['value'].'" '.$checked.' />';

			if( $radio['label'] == '未婚') {
				$html.= '<span>'.$radio['label'].'</span>';
			}else{
				$html.= '<span>'.$radio['label']. $this->_Members()->render().' 人<span>';
			}
			$html.= '</label>';
		}
		$html .= '</div>';

		return $html;
	}

	/**
	 * @Comment("職業")
	 */
	private function _Occupation() {
		$element = new \Personalwork\Forms\Elements\Select("occupation");
		$element->setLabel("職業");
		$element->setAttributes(array(
				'class' => 'input-default m select-append',
			));
		$occupation = \Houserich\Models\Fieldoptions::findByFieldname("職業");
		$items = array();
		foreach ($occupation->toArray() as $item) {
			$items[$item['label']]=$item['value'];
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 20
		]));
		return $element;
	}

	/**
	 * @Comment("年收入(單位:萬)")
	 */
	private function _Annualincome() {
		$element = new \Personalwork\Forms\Elements\Numeric("annualIncome");
		if( isset($this->getDI()->get('session')->get('USER')['PeopleInformation']) ){
		$element->setDefault( $this->getDI()->get('session')->get('USER')['PeopleInformation']['annualIncome'] );
		}
		return $element;
	}

	public function renderAnnualincome() {
		$html = '<div class="col-2 field-label">年收入</div>'.PHP_EOL;
		$html.= '<div class="col-10">'.PHP_EOL;
		$html.= '<div class="input-money">'.PHP_EOL;
		$html.= '<i>$</i>'.PHP_EOL;
		$html.= $this->_Annualincome()->render();
		$html.= '<span>萬元</span>'.PHP_EOL;
		$html.= '</div>'.PHP_EOL;
		$html.= '</div>'.PHP_EOL;

		return $html;
	}

	/**
	 * @Comment("通訊地址")
	 */
	private function _Address() {
		$element = new \Personalwork\Forms\Elements\Text("address");
		$element->addValidator(new StringLength([
			"max" => 100
		]));
		return $element;
	}

	/**
	 * @Comment("地址(縣市)")
	 */
	private function _Addresscity() {
		$element = new \Personalwork\Forms\Elements\Select("addressCity");
		// $element->setLabel("地址(縣市)");
		$element->setAttributes(array(
					"class" => "input-default m select-append depend-city",
					"data-target" => ".addressDistrict",
				));
		$city = \Houserich\Models\Fieldoptions::findByFieldname("縣市");
		$items = array(''=>'請選擇');
		foreach($city as $item){
			$items[$item->label] = $item->value;
		}
		$element->setOptions($items);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(行政區)")
	 */
	private function _Addressdistrict() {
		$element = new \Personalwork\Forms\Elements\Select("addressDistrict");
		// $element->setLabel("地址(行政區)");
		$element->setAttributes(array(
					"class" => "input-default m addressDistrict hide",
				));
		$district = \Houserich\Models\Fieldoptions::findByFieldname("行政區");
		$opt = array( ["value" => '',"label"=> "請選擇"] );
		foreach ($district as $item) {
			$opt[] = ["label"=>$item->label,
					  "value"=>$item->value,
					  "data-city"=>$item->parentLabel];
		}
		$element->setOptions($opt);
		$element->addValidator(new StringLength([
			"max" => 10
		]));
		return $element;
	}

	/**
	 * @Comment("地址(路)")
	 */
	private function _Addressroad() {
		$element = new \Personalwork\Forms\Elements\Text("addressRoad");
		$element->setAttributes(array(
					"class" => "input-default m",
					"placeholder"=> "路/段/道(含段)名"
				));
		$element->addValidator(new StringLength([
			"max" => 20,
			"message"=>"地址路名欄位長度超過15字元限制。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(巷)")
	 */
	private function _Addresslane() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressLane");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("巷");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址巷欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(弄)")
	 */
	private function _Addressalley() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressAlley");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("弄");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址弄欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號)")
	 */
	private function _Addressno() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNo");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("號");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(號之)")
	 */
	private function _Addressnoex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressNoEx");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址號之欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓)")
	 */
	private function _Addressfloor() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloor");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("樓");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓欄位必須為數值格式。"
		]));
		return $element;
	}

	/**
	 * @Comment("地址(樓之)")
	 */
	private function _Addressfloorex() {
		$element = new \Personalwork\Forms\Elements\Numeric("addressFloorEx");
		$element->setAttributes(array(
					"class" => "input-default s",
				))
				->setLabel("之");
		$element->addFilter('emptytozero');
		$element->addValidator(new Numericality([
		    "message"=>"地址樓之欄位必須為數值格式。"
		]));
		return $element;
	}


	/**
	 * @Comment("電子信箱")
	 */
	private function _Email() {
		$element = new \Personalwork\Forms\Elements\Text("email");
		$element->setLabel("電子信箱")
				->setAttributes([
					"class"		=> "input-default",
				]);
		$element->addValidator(new StringLength([
			"max" => 100
		]));
		return $element;
	}

	/**
	 * @Comment("LineID")
	 */
	private function _LineID() {
		$element = new \Personalwork\Forms\Elements\Text("lineID");
		$element->setLabel("Line")
				->setAttributes([
					"class"		=> "input-default",
				]);
		$element->addValidator(new StringLength([
			"max" => 30
		]));
		return $element;
	}

	/**
     * This method returns the default value for field 'csrf'
     */
    public function getCsrf()
    {
        return $this->security->getToken();
    }


	public function initialize($entity=null, $options=array()) {
		$id = new \Phalcon\Forms\Element\Hidden("PeopleId");

		$this->add($this->_Account());
		$this->add($this->_Fullname());
		$this->add($this->_Email());
		$this->add($this->_Password());
		$this->add($this->_Gender());
		$this->add($this->_Age());
		$this->add($this->_LineID());

		// 在會員中心內才綁定以下這些欄位。
		if( !empty($options['membercenter']) ){
			// $this->add($this->_Id());
			$this->add($this->_Marriage());
			$this->add($this->_Members());
			$this->add($this->_Occupation());
			$this->add($this->_Annualincome());
			$this->add($this->_Address());
			$this->add($this->_Addresscity());
			$this->add($this->_Addressdistrict());
			$this->add($this->_Addressroad());
			$this->add($this->_Addresslane());
			$this->add($this->_Addressalley());
			$this->add($this->_Addressno());
			$this->add($this->_Addressnoex());
			$this->add($this->_Addressfloor());
			$this->add($this->_Addressfloorex());
			$this->add($this->_Email());
		}else{
			$id->setDefault( $this->getDI()->get('session')->get('REGI_PEOPLEID') );
		}
		$this->add($id);

		// Add a text element to put a hidden CSRF
        $this->add( new \Phalcon\Forms\Element\Hidden( "csrf" ) );
	}
}
