<?php
/**
 * 處理地圖搜尋欄位
 * 1. 縣市
 * 2. 行政區
 * 3. 價格(總價)
 * 4.
 * */
namespace Housefront\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;


class MapForm extends \Personalwork\Forms\Form
{
    var $html;

    /**
     * @Comment("地址(縣市)")
     */
    private function _City() {
        $element = new \Personalwork\Forms\Elements\RadioGroup("city");
        // $element->setLabel("縣市");
        $element->setAttributes(array(
                    "name"  => "city",
                ));
        $d = new \Houserich\Models\Districts();
        if( isset($this->POST) && !empty($this->POST->city) ){
            $element->setDefault($this->POST->city);
        }

        $element->setUserOptions(array(
                    "parent-id" => "elmCity",
                    "parent-class" => "option-panel city",
                    "items" => $d->getZones()
                ));
        return $element;
    }


    /**
     * 產出「縣市」
     * */
    public function renderCity()
    {
        $elm = $this->_City();
        $pattr = ['id'=>$elm->getUserOption('parent-id'),
                  'class'=>$elm->getUserOption('parent-class')];

        $this->html .= '<div class="search-option">'.PHP_EOL;

        $this->html .= "\t".'<a href="javascript:" class="option-btn">縣市 <span class="icon-down-b"></span></a>'.PHP_EOL;
        $this->html .= "\t".Tag::tagHtml('div', $pattr, FALSE, TRUE, TRUE).PHP_EOL;
            $i=0;
            foreach ($elm->getUserOption('items') as $zone => $cities) {
        $this->html .= "\t\t".'<div>'.PHP_EOL;
        $this->html .= "\t\t\t".'<span class="txt-ora">'.$zone.'</span>'.PHP_EOL;
        $this->html .= "\t\t\t\t".'<span class="radio-style2">'.PHP_EOL;
                foreach ($cities as $city) {
                    if( $elm->getValue() == $city ){
                        $checked='checked="checked"';
                    }else{
                        $checked=null;
                    }
                    $this->html .= "\t\t\t\t".'<label for="city'.$i.'"><input type="radio" id="city'.$i.'" name="'.$elm->getAttribute('name').'" value="'.$city.'" '.$checked.' /><span>'.$city.'</span></label>'.PHP_EOL;
                $i++;
                }
        $this->html.= "\t\t\t".'</span>'.PHP_EOL;
        $this->html.= "\t\t".'</div>'.PHP_EOL;
        }

        $this->html.= "\t".'</div>'.PHP_EOL;
        $this->html.= '</div>'.PHP_EOL;
        print($this->html);
    }


    /**
     * @Comment("地址(行政區)")
     */
    private function _District() {
        $element = new \Personalwork\Forms\Elements\RadioGroup("district");
        $element->setAttributes(array(
                    "name"=> "district[]"
                ));

        if( isset($this->POST) && !empty($this->POST->district) ){
            $element->setDefault($this->POST->district);
        }

        $opt = array();
        if( !empty($element->getValue()) ){
            $district = \Houserich\Models\Districts::findByCity( $this->get('city')->getValue() );
            foreach ($district as $i => $item) {
                if( in_array($item->district, $element->getValue()) ){
                    $t = ["id"=>"district{$i}",
                          "label"=>$item->district,
                          "value"=>$item->district,
                          "checked"=>"checked='checked'"];
                }else{
                    $t = ["id"=>"district{$i}",
                          "label"=>$item->district,
                          "value"=>$item->district,
                          "checked"=>null];
                }
                $opt[] = $t;
            }
        }
        $element->setUserOptions(array(
                    "parent-id" => "elmDistricts",
                    "parent-class" => "option-panel check-style2",
                    "items" => $opt
                ));
        return $element;
    }


    /**
     * 產出「行政區」
     * */
    public function renderDistrict()
    {
        $elm = $this->_District();
        $pattr = ['id'=>$elm->getUserOption('parent-id'),
                  'class'=>$elm->getUserOption('parent-class')];

        $this->html = '<div class="search-option">';
        $this->html .= '<a href="javascript:" class="option-btn">地區 <span class="icon-down-b"></span></a>';

            $this->html .= "\t".Tag::tagHtml('div', $pattr, FALSE, TRUE, TRUE).PHP_EOL;
                if( count($elm->getUserOption('items')) > 0 ){
                foreach ($elm->getUserOption('items') as $i => $item) {
                    $this->html .= '<label for="district'.$i.'"><input type="checkbox" id="district'.$i.'" name="'.$elm->getAttribute('name').'" value="'.$item['value'].'" '.$item['checked'].' /><span>'.$item['label'].'</span></label>';
                }
                }else{
                    $this->html .= '請先選擇縣市!';
                }
            $this->html.= '</div>';

        $this->html.= '</div>';

        print($this->html);
    }


    /**
     * @Comment("售價1")
     */
    private function _Totalprice1() {
        $element = new \Personalwork\Forms\Elements\Select("totalprice1");
        $element->setAttributes(array(
                    "name"  => "totalprice[0]",
                    "class" => "js-select",
                ));

        if( isset($this->POST) && isset($this->POST->totalprice[0]) ){
            $element->setDefault($this->POST->totalprice[0]);
        }

        $opt = array(
                "" => '',
                "0" => '0',
                "400" => '400',
                "800" => '800',
                "1200" => '1200',
                "1600" => '1600',
                "2000" => '2000',
                "2600" => '2600',
                "3200" => '3200',
                "4000" => '4000',
                "5000" => '5000',
              );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("售價2")
     */
    private function _Totalprice2() {
        $element = new \Personalwork\Forms\Elements\Select("totalprice2");
        $element->setAttributes(array(
                    "name"  => "totalprice[1]",
                    "class" => "js-select",
                ));

        if( isset($this->POST) && !empty($this->POST->totalprice) ){
            $element->setDefault($this->POST->totalprice[1]);
        }

        $opt = array(
                "" => '',
                "400" => '400',
                "800" => '800',
                "1200" => '1200',
                "1600" => '1600',
                "2000" => '2000',
                "2600" => '2600',
                "3200" => '3200',
                "4000" => '4000',
                "5000" => '5000',
              );
        $element->setOptions($opt);
        return $element;
    }


    /**
     * 產出「售價」
     * */
    public function renderTotalprice()
    {
        $this->html = '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">價格 <span class="icon-down-b"></span></a>';

        $this->html.= '<div class="option-panel"><span class="forcombo ss">';
        $this->html.= $this->_Totalprice1()->render();
        $this->html.= '~';
        $this->html.= $this->_Totalprice2()->render();
        $this->html.= '萬
                       </span></div>';

        $this->html.= '</div>';

        print($this->html);
    }


    /**
     * @Comment("型態")
     */
    private function _Type() {
        $element = new \Personalwork\Forms\Elements\CheckGroup("type");
        $element->setLabel("型態");
        $element->setAttributes(array(
                    "name" => "type[]"
                ));

        $item_type = array(
                    ['label'=>'土地', 'value'=>'土地', 'checked'=>null],
                    ['label'=>'公寓', 'value'=>'公寓', 'checked'=>null],
                    ['label'=>'電梯大樓', 'value'=>'電梯大樓', 'checked'=>null],
                    ['label'=>'透天厝', 'value'=>'透天厝', 'checked'=>null],
                    ['label'=>'店面', 'value'=>'店面', 'checked'=>null],
                    ['label'=>'廠辦', 'value'=>'廠辦', 'checked'=>null],
                    // ['label'=>'車位', 'value'=>7, 'checked'=>null],
                  );
        if( isset($this->POST) && !empty($this->POST->type) ){
            $element->setDefault($this->POST->type);
        foreach ($item_type as $i => $val) {
            if( in_array($val['value'], $element->getValue()) ){
                $item_type[$i]['checked'] = "checked='checked'";
            }
        }
        }

        $element->setUserOptions(array(
                    "parent-class" => "option-panel check-style2",
                    "items" => $item_type
                ));
        return $element;
    }


    /**
     * 產出「型態」
     * */
    public function renderType()
    {
        $elm = $this->_Type();
        $pattr = ['class'=>$elm->getUserOption('parent-class')];

        $this->html = '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">型態 <span class="icon-down-b"></span></a>';

        $this->html .= "\t".Tag::tagHtml('div', $pattr, FALSE, TRUE, TRUE).PHP_EOL;
                if( count($elm->getUserOption('items')) > 0 ){
                foreach ($elm->getUserOption('items') as $i => $item) {
                    $this->html .= '<label for="type'.$i.'"><input type="checkbox" id="type'.$i.'" name="type[]" value="'.$item['value'].'" '.$item['checked'].' /><span>'.$item['label'].'</span></label><BR/>';
                }
                }
        $this->html.= '</div>';

        $this->html.= '</div>';

        print($this->html);
    }


    /**
     * @Comment("格局")
     */
    private function _Petternrooms() {
        $element = new \Personalwork\Forms\Elements\RadioGroup("petternroom");

        $patterns = array(
                    ['value'=>null ,'label'=>"不限", 'checked'=>null],
                    ['value'=>1 ,'label'=>"1房", 'checked'=>null],
                    ['value'=>2 ,'label'=>"2房", 'checked'=>null],
                    ['value'=>3 ,'label'=>"3房", 'checked'=>null],
                    ['value'=>4 ,'label'=>"4房以上", 'checked'=>null],
                    ['value'=>'others' ,'label'=>"其他", 'checked'=>null ],
                  );
        if( isset($this->POST) && !empty($this->POST->petternroom) ){
            $element->setDefault($this->POST->petternroom);
            foreach ($patterns as $i => $val) {
                if( $val['value']==$element->getValue() ){
                    $patterns[$i]['checked'] = "checked='checked'";
                }
            }
        }
        $element->setUserOptions(array(
                "parent-class" => "option-panel radio-style2",
                "items" => $patterns
            ));
        return $element;
    }


    /**
     * 產出「格局」
     * */
    public function renderPetternrooms()
    {
        $elm = $this->_Petternrooms();
        $pattr = ['class'=>$elm->getUserOption('parent-class')];

        $this->html = '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">格局 <span class="icon-down-b"></span></a>';

        $this->html .= "\t".Tag::tagHtml('div', $pattr, FALSE, TRUE, TRUE).PHP_EOL;
                if( count($elm->getUserOption('items')) > 0 ){
                foreach ($elm->getUserOption('items') as $i => $item) {
                    if( $item['value'] == 'others' ){
                        $this->html .= '<span>';
                    }
                    $this->html .= '<label for="'.$elm->getAttribute('id').$i.'"><input type="radio" id="'.$elm->getAttribute('id').$i.'" name="'.$elm->getName().'" value="'.$item['value'].'" '.$item['checked'].' /><span>'.$item['label'].'</span></label>';
                    if( $item['value'] == 'others' ){
                        $val0 = ( isset($this->POST) && !empty($this->POST->patternrooms[0]) && $this->POST->petternroom=='others' ) ? $this->POST->patternrooms[0] : null;
                        $val1 = ( isset($this->POST) && !empty($this->POST->patternrooms[1]) && $this->POST->petternroom=='others' ) ? $this->POST->patternrooms[1] : null;
                        $this->html .= '<input type="text" class="input-default xs" id="patterno1" name="patternrooms[0]" value="'.$val0.'" /> ~
                <input type="text" class="input-default xs" id="patterno2" name="patternrooms[1]" value="'.$val1.'" /></span> 房';

                        $this->html .= '</span>';
                    }
                    $this->html .= '<BR/>';
                }
                }
        $this->html.= '</div>';

        $this->html.= '</div>';

        print($this->html);
    }


    /**
     * @Comment("樓層")
     */
    private function _Floor() {
        $element = new \Personalwork\Forms\Elements\RadioGroup("floor");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        // var_dump($element->getValue());die();
        $floor = array(
                    ['value'=>null, 'label'=>'不限', 'checked'=>null],
                    // ['value'=>2 , 'label'=>'B1+1樓, 'checked'=>null'],
                    ['value'=>1 , 'label'=>'1樓', 'checked'=>null],
                    ['value'=>2 , 'label'=>'2樓', 'checked'=>null],
                    // ['value'=>'2,3', 'label'=>'2~3樓, 'checked'=>null'],
                    ['value'=>'top', 'label'=>'頂樓', 'checked'=>null],
                    ['value'=>'others', 'label'=>'其他', 'checked'=>null]
                  );
        if( isset($this->POST) && !empty($this->POST->floor) ){
            $element->setDefault($this->POST->floor);
            foreach ($floor as $i => $val) {
                if( $val['value']==$element->getValue() ){
                    $floor[$i]['checked'] = "checked='checked'";
                }
            }
        }
        $element->setUserOptions(array(
                "parent-class" => "option-panel radio-style2",
                "items" => $floor
            ));
        return $element;
    }


    /**
     * @Comment("屋齡")
     */
    private function _Houseage() {
        $element = new \Personalwork\Forms\Elements\RadioGroup("houseage");
        $element->setAttributes(array(
                    "class" => "js-select",
                ));
        $houseage = array(
                    ['value' => null, 'label' => '不限', 'checked'=>null],
                    ['value' => 5   , 'label' => '5年內', 'checked'=>null],
                    ['value' => 10  , 'label' => '10年內', 'checked'=>null],
                    ['value' => 20  , 'label' => '20年內', 'checked'=>null],
                    ['value' => 30  , 'label' => '30年內', 'checked'=>null],
                    ['value' => 'others', 'label' => '其他', 'checked'=>null],
                  );
        if( isset($this->POST) && !empty($this->POST->houseage) ){
            $element->setDefault($this->POST->houseage);
            foreach ($houseage as $i => $val) {
                if( $val['value']==$element->getValue() ){
                    $houseage[$i]['checked'] = "checked='checked'";
                }
            }
        }

        $element->setUserOptions(array(
                "parent-class" => "option-panel radio-style2",
                "items" => $houseage
            ));
        return $element;
    }


    /**
     * @Comment("坪數1")
     */
    private function _Area1() {
        $element = new \Personalwork\Forms\Elements\Select("area");
        $element->setAttributes(array(
                    "name" => "area[0]",
                    "class" => "js-select",
                ));

        if( isset($this->POST) && isset($this->POST->area[0]) ){
            $element->setDefault($this->POST->area[0]);
        }

        $opt = array(
                "" => '',
                "0" => '0',
                "15" => '15',
                "25" => '25',
                "35" => '35',
                "45" => '45',
                "55" => '55',
              );
        $element->setOptions($opt);
        return $element;
    }

    /**
     * @Comment("坪數2")
     */
    private function _Area2() {
        $element = new \Personalwork\Forms\Elements\Select("area");
        $element->setAttributes(array(
                    "name" => "area[1]",
                    "class" => "js-select",
                ));

        if( isset($this->POST) && !empty($this->POST->area[1]) ){
            $element->setDefault($this->POST->area[1]);
        }

        $opt = array(
                "" => '',
                "15" => '15',
                "25" => '25',
                "35" => '35',
                "45" => '45',
                "55" => '55',
              );
        $element->setOptions($opt);

        return $element;
    }


    /**
     * @Comment("車位")
     */
    private function _Parking() {
        $element = new \Personalwork\Forms\Elements\RadioGroup("parking");
        $parking = array(
                ['id'=>'parking1', 'label'=>'無', 'value'=>0, 'checked'=>null],
                ['id'=>'parking2', 'label'=>'有', 'value'=>1, 'checked'=>null],
               );
        if( isset($this->POST) && isset($this->POST->parking) ){
            $element->setDefault($this->POST->parking);
            foreach ($parking as $i => $val) {
                if( $val['value']==$element->getValue() ){
                    $parking[$i]['checked'] = "checked='checked'";
                }
            }
        }
        $element->setUserOptions(array(
                    "items" => $parking
                ));
        return $element;
    }



    /**
     * @Comment("關鍵字")
     */
    private function _Title() {
        $element = new \Phalcon\Forms\Element\Text("title");
        $element->setAttributes(array(
                    "class" => "input-default L",
                    "placeholder" => "請輸入關鍵字"
                ));
        return $element;
    }

    /**
     * 產出「一般條件：進階搜尋」
     * */
    public function renderAdvance()
    {
        $this->html = '<div class="search-option">';
        $this->html.= '<a href="javascript:" class="option-btn">進階條件 <span class="icon-down-b"></span></a>';
        $this->html.= '<div class="option-panel L">';

            $this->html.= '<div class="field">';
                $this->html.= '<div class="col-2 txt-ora">樓層</div>
                               <div class="col-10 radio-style2">';
            $floor = $this->_Floor();
                if( count($floor->getUserOption('items')) > 0 ){
                foreach ($floor->getUserOption('items') as $i => $item) {
                    if( $item['value'] == 'others' ){
                        $this->html .= '<span>';
                    }
                    $this->html .= '<label for="'.$floor->getName().$i.'"><input type="radio" id="'.$floor->getName().$i.'" name="'.$floor->getName().'" value="'.$item['value'].'" '.$item['checked'].' /><span>'.$item['label'].'</span></label>';
                    if( $item['value'] == 'others' ){
                        $val0 = ( isset($this->POST) && !empty($this->POST->flooro[0]) && $this->POST->floor=='others' ) ? $this->POST->flooro[0] : null;
                        $val1 = ( isset($this->POST) && !empty($this->POST->flooro[1]) && $this->POST->floor=='others' ) ? $this->POST->flooro[1] : null;
                        $this->html .= '<input type="text" class="input-default xs" id="flooro1" name="flooro[0]" value="'.$val0.'" /> ~
                                        <input type="text" class="input-default xs" id="flooro2" name="flooro[1]" value="'.$val1.'" /></span> 樓';
                        $this->html .= '</span>';
                    }
                }
                }
                $this->html.= '</div>';
            $this->html.= '</div>';

            $this->html.= '<div class="dash-line"></div>';
            $this->html.= '<div class="field">
                            <div class="col-2 txt-ora">屋齡</div>
                            <div class="col-10 radio-style2">';
                $house = $this->_Houseage();
                if( count($house->getUserOption('items')) > 0 ){
                foreach ($house->getUserOption('items') as $i => $item) {
                    if( $item['value'] == 'others' ){
                        $this->html .= '<span>';
                    }
                    $this->html .= '<label for="'.$house->getName().$i.'"><input type="radio" id="'.$house->getName().$i.'" name="'.$house->getName().'" value="'.$item['value'].'" '.$item['checked'].' /><span>'.$item['label'].'</span></label>';
                    if( $item['value'] == 'others' ){
                        $val0 = ( isset($this->POST) && !empty($this->POST->houseageo[0]) && $this->POST->houseage=='others' ) ? $this->POST->houseageo[0] : null;
                        $val1 = ( isset($this->POST) && !empty($this->POST->houseageo[1]) && $this->POST->houseage=='others' ) ? $this->POST->houseageo[1] : null;
                        $this->html .= '<input type="text" class="input-default xs" id="houseageo1" name="houseageo[0]" value="'.$val0.'" /> ~
                <input type="text" class="input-default xs" id="houseageo2" name="houseageo[1]" value="'.$val1.'" /></span> 年';
                        $this->html .= '</span>';
                    }
                }
                }
                $this->html.= '</div>';
            $this->html.= '</div>';

            $this->html.= '<div class="dash-line"></div>';
            $this->html.= '<div class="field">
                            <div class="col-2 txt-ora">坪數</div>
                            <div class="col-10"><span class="forcombo ss">';
                    $this->html.= $this->_Area1()->render().'~';
                    $this->html.= $this->_Area2()->render().'坪
                            </span></div>
                           </div>';

            $this->html.= '<div class="dash-line"></div>
                           <div class="field">
                                <div class="col-2 txt-ora">車位</div>
                                <div class="col-10 radio-style2">';
            $p = $this->_Parking();
                if( count($p->getUserOption('items')) > 0 ){
                foreach ($p->getUserOption('items') as $i => $item) {
                    $this->html .= '<label for="'.$p->getName().$i.'"><input type="radio" id="'.$p->getName().$i.'" name="'.$p->getName().'" value="'.$item['value'].'" '.$item['checked'].' /><span>'.$item['label'].'</span></label>';
                }
                }
                $this->html.= ' </div>
                           </div>';

        $this->html.='</div>';
        $this->html.='</div>';

        return $this->html;
    }


    public function initialize() {
        // primary key

        if( !empty($this->getEntity()) ){
            $this->POST = $this->getEntity();
        }

        $this->add($this->_City());
        $this->add($this->_District());
        $this->add($this->_Totalprice1());
        $this->add($this->_Totalprice2());
        $this->add($this->_Type());
        $this->add($this->_Petternrooms());
        $this->add($this->_Floor());
        $this->add($this->_Houseage());
        $this->add($this->_Area1());
        $this->add($this->_Area2());
        $this->add($this->_Parking());
        $this->add($this->_Title());


        $btn = new \Phalcon\Forms\Element\Submit("qBtn");
        $btn->setName('qBtn');
        $btn->setAttributes(array(
                    "class" => "btn-sent",
                    "value" => "搜尋"
                ));
        $this->add($btn);

        $this->add( new \Phalcon\Forms\Element\Hidden( "csrf" ) );
    }
}