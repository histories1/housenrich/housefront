<?php

namespace Housefront\Forms\Decorators;

use Phalcon\Tag as Tag;
use \Personalwork\Forms\Decorators\AbstractGroupDecorators as Decorator;

/**
 * address群組欄位，配置地址區塊包含指定呼叫縣市與行政區
 * */
class Addresses extends Decorator
{
    var $elementNames = array();

    var $form;

    public function __construct($elements){
        foreach ($elements as $i => $elm) {
            if( !is_object($this->form) ){
                $this->form = $elm->getForm();
            }
            $this->elementNames[] = $elm->getName();
        }

        $this->setElements($elements);
    }

    public function toHtml() {

        $this->html = '<div class="col-10">';

        if( in_array("addressCity", $this->elementNames) && in_array("addressDistrict", $this->elementNames) ){
            $this->html.= $this->form->renderGroups(["addressCity", "addressDistrict"],
                                                   "\\Housefront\\Forms\\Decorators\\DependSelect");
        }

        // 路
        if( ($key=array_search("addressRoad", $this->elementNames)) ){
            $this->html.= $this->elements[$key]->render();
        }
        // 巷
        if( ($key=array_search("addressLane", $this->elementNames)) ){
            $land=$this->elements[$key];
            $this->html.= $land->render();
            $this->html.= "\t\t\t".Tag::tagHtml( 'label',
                                            array(
                                                'for' => $land->getName(),
                                            ), FALSE, TRUE, TRUE);
            $this->html .= $land->getLabel() . Tag::tagHtmlClose('label').PHP_EOL;
        }

        // 弄
        if( ($key=array_search("addressAlley", $this->elementNames)) ){
            $land=$this->elements[$key];
            $this->html.= $land->render();
            $this->html.= "\t\t\t".Tag::tagHtml( 'label',
                                            array(
                                                'for' => $land->getName(),
                                            ), FALSE, TRUE, TRUE);
            $this->html .= $land->getLabel() . Tag::tagHtmlClose('label').PHP_EOL;
        }

        // 號
        if( ($key=array_search("addressNo", $this->elementNames)) ){
            $land=$this->elements[$key];
            $this->html.= $land->render();
            $this->html.= "\t\t\t".Tag::tagHtml( 'label',
                                            array(
                                                'for' => $land->getName(),
                                            ), FALSE, TRUE, TRUE);
            $this->html .= $land->getLabel() . Tag::tagHtmlClose('label').PHP_EOL;
        }

        // 號之
        if( ($key=array_search("addressNoEx", $this->elementNames)) ){
            $land=$this->elements[$key];
            $this->html.= "\t\t\t".Tag::tagHtml( 'label',
                                            array(
                                                'for' => $land->getName(),
                                            ), FALSE, TRUE, TRUE);
            $this->html .= $land->getLabel() . Tag::tagHtmlClose('label').PHP_EOL;
            $this->html.= $land->render();
        }

        // 樓
        if( ($key=array_search("addressFloor", $this->elementNames)) ){
            $land=$this->elements[$key];
            $this->html.= $land->render();
            $this->html.= "\t\t\t".Tag::tagHtml( 'label',
                                            array(
                                                'for' => $land->getName(),
                                            ), FALSE, TRUE, TRUE);
            $this->html .= $land->getLabel() . Tag::tagHtmlClose('label').PHP_EOL;
        }

        // 樓之
        if( ($key=array_search("addressFloorEx", $this->elementNames)) ){
            $land=$this->elements[$key];
            $this->html.= "\t\t\t".Tag::tagHtml( 'label',
                                            array(
                                                'for' => $land->getName(),
                                            ), FALSE, TRUE, TRUE);
            $this->html .= $land->getLabel() . Tag::tagHtmlClose('label').PHP_EOL;
            $this->html.= $land->render();
        }

        $this->html.= '</div>';

        return $this->html;
    }
}