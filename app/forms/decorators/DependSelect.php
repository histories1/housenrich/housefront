<?php

namespace Housefront\Forms\Decorators;

use Phalcon\Tag as Tag;
use \Personalwork\Forms\Decorators\AbstractGroupDecorators as Decorator;

/**
 * 設定兩個表單元素有相依處理作法
 * 需搭配js對應data-處理
 * */
class DependSelect extends Decorator
{
    public function toHtml() {

        foreach ($this->elements as $element) {
            // grid-div
            if( !empty($element->getAttribute('parent-class')) ){

                $this->html .= "\t\t".Tag::tagHtml('div',array('class' => $element->getAttribute('parent-class')), FALSE, TRUE, TRUE).PHP_EOL;
            }

            if( !empty($element->getLabel()) ){
                // label
                $this->html .= "\t\t\t".Tag::tagHtml( 'label',
                                                array(
                                                    'for' => $element->getName(),
                                                    'class' => 'control-label'
                                                ), FALSE, TRUE, TRUE);
                $this->html .= $element->getLabel();
                $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
            }

                // element
                $this->html .= "\t\t\t".$element->render().PHP_EOL;

            // grid-div
            if( !empty($element->getAttribute('parent-class')) ){
                $this->html.= "\t\t".Tag::tagHtmlClose('div').PHP_EOL;
            }
        }

        return $this->html;
    }
}