<?php

namespace Housefront\Forms\Decorators;

use Phalcon\Tag as Tag;
use \Personalwork\Forms\Decorators\AbstractGroupDecorators as Decorator;

/**
 * grid群組欄位，自動根據傳入元素個數配置grid值
 * */
class GridElement extends Decorator
{
    public function toHtml() {

        $grid = floor(12 / count($this->elements));

        foreach ($this->elements as $element) {

            // grid-div
            if( $element->getUserOption('grid-class') ){
                $attr['class'] = "col-sm-".$grid." ".$element->getUserOption('grid-class');
            }else{
                $attr = array('class' => "col-sm-".$grid);
            }
            if( $element->getUserOption('grid-id') ){
                $attr['id'] = $element->getUserOption('grid-id');
            }

            $this->html .= "\t\t".Tag::tagHtml('div', $attr, FALSE, TRUE, TRUE).PHP_EOL;

                // label
                if( !empty($element->getLabel()) ){
                $this->html .= "\t\t\t".Tag::tagHtml( 'label',
                                                array(
                                                    'for' => $element->getName(),
                                                    'class' => 'control-label'
                                                ), FALSE, TRUE, TRUE);
                $this->html .= $element->getLabel();
                $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
                }
                // element
                $this->html .= "\t\t\t".$element->render().PHP_EOL;

            $this->html.= "\t\t".Tag::tagHtmlClose('div').PHP_EOL;

        }

        return $this->html;
    }
}