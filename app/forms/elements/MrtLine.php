<?php

namespace Housefront\Forms\Elements;

use Phalcon\Tag as Tag;

/**
 * 捷運站路線欄位，
 * */
class MrtLine extends \Personalwork\Forms\Elements\CheckGroup
{
    var $html;


    function __construct($name, $default=null, $opt=array()) {
        parent::__construct($name, $default, $opt);
    }

    public function renderLine() {
        $attributes = $this->getAttributes();

        $userOpts = $this->getUserOptions();

        if( isset($userOpts['label-class']) ){
            $this->html .= "\t".Tag::tagHtml( 'div', ['class'=>$userOpts['label-class']], FALSE, TRUE, TRUE);
            $this->html .= "\t\t".$this->getLabel();
            $this->html .= "\t".Tag::tagHtmlClose('div').PHP_EOL;
        }
        if( isset($userOpts['parent-class']) ){
            $parent_attr['class'] = $userOpts['parent-class'];
            $parent_attr['id'] = ( isset($userOpts['parent-id']) ) ? $userOpts['parent-id']: null;
            $this->html .= "\t".Tag::tagHtml( 'div', $parent_attr, FALSE, TRUE, TRUE);
        }

        foreach ($userOpts['lines'] as $i => $line) {
            // 調整checkbox屬性
            $attr['type'] = 'checkbox';
            $attr['id'] = 'line-'.$line['identify'];
            $attr['name'] = $this->getName();
            $attr['value'] = $line['label'];
            // if( $line['label'] == $this->getValue() ){
            if( preg_match("/{$line['label']}/u", $this->getValue())  ){
                $attr['checked'] = 'checked';
            }elseif( isset($userOpts['default']) && $line['value'] == $userOpts['default'] ){
                $attr['checked'] = 'checked';
            }else{
                $attr['checked'] = null;
            }
            // input:checkbox
            $this->html .= "\t\t".Tag::tagHtml( 'label',
                                                array(
                                                    'for' => $attr['id'],
                                                ), FALSE, TRUE, TRUE);
            $this->html .= Tag::tagHtml( 'input', $attr, TRUE, TRUE, TRUE);
            $this->html .= "<span><img src='/misc/images/mrt/line-{$line['identify']}.png' />{$line['label']}</span>";
            $this->html .= Tag::tagHtmlClose('label').PHP_EOL;
        }

        if( isset($userOpts['parent-class']) ){
            $this->html .= "\t".Tag::tagHtmlClose('div').PHP_EOL;
        }

        return $this->html;
    }


    /**
     * 直接判斷是否轉乘站設定附加樣式
     * */
    private function _check_transfer_stations( &$station, $nowlineid ) {
        $station['exclass'] = null;

        $stations = ['南港展覽館站', '大安', '民權西路', '忠孝新生', '松江南京', ''];

        if( in_array($station['name'], $stations) ){
            $station['exclass'] = 'trans';
        }

        if( $station['name'] == '南港展覽館站' ){
            if( $nowlineid == 'BL' ){
                $station['apphtml'] = '<div><img src="/misc/images/mrt/line-BR.png" /></div>';
            }elseif( $nowlineid == 'BR' ){
                $station['apphtml'] = '<div><img src="/misc/images/mrt/line-BL.png" /></div>';
            }
        }elseif( $station['name'] == '新北投站' ){
            $station['exclass'] = 'trun';
            $station['apphtml'] = '<div class="turn-line">
                                        <div class="st noline">
                                        <label for="R22-1">
                                            <input type="checkbox" id="R22-1" name="mrtstations[]" value="on">
                                            <span></span>
                                        </label>
                                        <div>新北投</div>
                                        </div>
                                    </div>';
        }else{
            $station['apphtml'] = '<div></div>';
        }
    }

    public function renderStations() {
        $userOpts = $this->getUserOptions();

        $this->html = '';

        foreach($userOpts['stations'] as $identify => $stations){
            $linename = \Houserich\Models\Fieldoptions::findFirstByValue($identify);
        $this->html.= '<div class="field mrtline line-'.$identify.' hide">';
                $this->html.= '<div class="col-2"><span class="line-name">'.$linename->label.'</span></div>';
                $this->html.= '<div class="col-10">';

            $attr = array();
            foreach($stations as $station){

                $this->_check_transfer_stations( $station , $identify);

                $attr['type']   = 'checkbox';
                $attr['id']     = $station['id'];
                $attr['name']   = $this->getName();
                $attr['value']  = $station['name'];
                // if( preg_match("/{$attr['value']}/iu", $this->getValue()) ){
                //     $attr['checked'] = 'checked';
                // }else{
                //     $attr['checked'] = null;
                // }

            $this->html.= '<div class="st '.$station['exclass'].'">
                              <label for="'.$attr['id'].'">';
                $this->html.= Tag::tagHtml( 'input', $attr, TRUE, TRUE, TRUE);
                $this->html.='<span></span></label>
                              <div>'.str_replace('站','',$station['name']).'</div>
                              '.$station['apphtml'].'
                          </div>';
            }

                $this->html.= '</div>';
            $this->html.= '</div>';
            $this->html.= '<div class="dash-line hide"></div>';
        }

        return $this->html;
    }
}