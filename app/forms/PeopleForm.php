<?php

namespace Housefront\Forms;

use Phalcon\Tag;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Email;

class PeopleForm extends \Personalwork\Forms\Form
{
	/**
     * This method returns the default value for field 'csrf'
     */
    public function getCsrf()
    {
        return $this->security->getToken();
    }


	public function initialize() {
		$element = new \Phalcon\Forms\Element\Hidden("type");
		$element->setDefault('local');
		$this->add($element);

		$element = new \Personalwork\Forms\Elements\Text("cellphone");
		$element->setLabel("手機門號")
				->setAttributes([
					"class" => 'input-default m',
					"required" => "required"
				])
				->setUserOption('label-class', 'col-2 field-label must');
		$element->addValidator(new StringLength([
			"max" => 15
		]));
		$this->add($element);

		$element = new \Personalwork\Forms\Elements\Text("smscode");
		$element->setLabel("驗證碼")
				->setAttributes(array(
					"class" => 'input-default m',
					"placeholder" => ""
				))
				->setUserOption('label-class', 'col-2 field-label must');
		$element->addValidator(new StringLength([
			"max" => 6
		]));
		$this->add($element);

		// Add a text element to put a hidden CSRF
		$this->add( new \Phalcon\Forms\Element\Hidden( "csrf" ) );
	}
}
