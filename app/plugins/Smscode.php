<?php

namespace Housefront\Plugins;

use \Phalcon\Mvc\User\Plugin as UserPlugin;

/**
 * 處理簡訊平台費用
 */
class Smscode extends UserPlugin
{
    const ERR_PHONE_EXISTED = 0;

    const ERR_PHONE_FORMATFAIL = 0;

    const RES_CHECK_PASS = 'pass_check';

    const ERR_SEND_FAIL = 0;

    const ERR_SEND_RESEND = 0;

    const ERR_SEND_NORESULT = 0;

    const RES_SEND_PASS = 'pass_send';

    const ERR_MAPPING_TIMEOUT = 0;

    const ERR_MAPPING_NOTMATCH = 0;

    const RES_MAPPING_PASS = 'pass_mapping';

    const ERR_SAVE_FAIL = 0;
    /**
     * 會員中心手機驗證後儲存功能
     * */
    const RES_SET_PASS = 'pass_setphone';


    var $code;

    var $messages = array();

    var $result;

    /**
     * TwSMS_API_4.0版_規格說明
     * 1. 傳送使用 http GET 或 http POST 方式。
     * 2. 回傳採用 xml 格式
     * 3. message TAG 請先使用 urlencode 編碼,避免部分符號造成發送錯誤。
     * 4. 所有含 * 的 Tag 為必需填寫之參數,其他 Tag 不填寫時系統將會自動帶入內定值。 5. 長簡訊功能有部份電信業者(如 PHS)及部份手機無法支援,將會發送失敗。
     * 6. 長簡訊不支援國際門號
     * 7. 簡訊內容換行請使用 \\n
     * */
    var $_sms = array(
                'api' => 'https://api.twsms.com/smsSend.php',
                'Tag' => array(
                            'username' => 'housenrich',
                            'password' => '43943971',
                            'expirytime' => 600,
                            /**
                             * 主動式回傳簡訊接收狀態,將接收結果回傳到這個參數設定的網址內。
                             * 此參數空白將不主動回報接收狀態(可使用被動式查詢)。
                             * 例如:http://www.abc.com/twsmsResp.php
                             * */
                            // 'drurl' => 'http://wetalk.personalwork.tw/',
                            // 簡訊發送門號使用 0961295325 顯示
                            'mo' => 'Y',
                            // 09xxxxxxxx(台灣門號) or 國碼+門號 (國際門號) 台灣門號每則扣 1 通,國際門號每則扣 3 通
                            'mobile' => null,
                            'message' => null
                )
            );

    function __construct()
    {

    }

    public function setCode($code=null) {
        if( empty($code) ){
            $this->code = rand(123456, 654321);
        }else{
            $this->code = $code;
        }

        return $this;
    }

    public function getCode() {
        return $this->code;
    }


    public function getMessages() {
        return $this->messages;
    }


    public function setResult($res) {
        $this->result = $res;
        return $this;
    }


    public function getResult() {
        return $this->result;
    }


    /**
     * 透過ajax檢查手機號碼是否已存在資料庫
     * */
    function check_sms( $num ){
        // string filter
        $mobile_num=filter_var($num, FILTER_SANITIZE_NUMBER_INT, array());

        if( !preg_match("/^(\+886|0)9\d{8}$/", $mobile_num) ){
            $this->messages[] = "您輸入的門號格式不符無法接收簡訊，請檢查。";
            $this->setResult( self::ERR_PHONE_FORMATFAIL );
            return false;
        }
        // checkif exist
        if( $this->session->has("REGI_PHONE") &&
            $this->session->get("REGI_PHONE") != $mobile_num ){
        }

        $poeple = \Houserich\Models\People::findFirstByCellphone($mobile_num);
        if( $poeple ){
            $this->messages[] = "此手機門號已經被使用，請更換。";
            $this->setResult( self::ERR_PHONE_EXISTED );
            return false;
        }else{
            // 紀錄手機門號
            $this->session->set('REGI_PHONE', $num);

            return $this->setResult( self::RES_CHECK_PASS );
        }
    }


    /**
     * 根據手機門號優先檢查是否是設定於目前會員欄位
     * */
    function memsend_sms($num) {
        // string filter
        $mobile_num=filter_var($num, FILTER_SANITIZE_NUMBER_INT, array());
        if( $this->session->has('REGI_TIME') &&
            ($this->session->get('REGI_TIME') > (time()-600)) ){
            $this->messages[] = "您已於10分鐘內提出接收簡訊碼，請再次檢查手機或等待10分鍾過後再次處理。";
            $this->setResult( self::ERR_SEND_RESEND );
            return false;
        }

        // 若使用者已經設定手機門號
        if( !empty($this->session->get('USER')['People']['cellphone']) && $this->session->get('USER')['People']['cellphone']==$num){
            $this->setCode();
            $code = $this->getCode();
            return $this->_sendproc($mobile_num, urlencode("好多房手機簡訊認證碼:{$code}，請於10分鐘內於會員基本欄位頁面上完成認證。") );
        }elseif( !$this->check_sms($num) ){
            return false;
        }

    }


    /**
     * 傳送簡訊
     * 1. session暫存手機碼，判斷是否之前已經送出過(十分鐘內禁止重送！)
     * */
    function send_sms($num){
        // 比對手機門號
        if( !$this->check_sms($num) ){
            return false;
        }

        // 10分內已申請過！
        if( $this->session->has('REGI_TIME') &&
            ($this->session->get('REGI_TIME') > (time()-600)) ){
            $this->messages[] = "您已於10分鐘內提出接收簡訊碼，請再次檢查手機或等待10分鍾過後再次處理。";
            $this->setResult( self::ERR_SEND_RESEND );
            return false;
        }

        $this->setCode();
        $code = $this->getCode();
        return $this->_sendproc($num, urlencode("好多房手機簡訊認證碼:{$code}，請於10分鐘內於註冊頁面上完成認證。") );
    }


    private function _sendproc($num, $msg) {
        $this->_sms['Tag']['mobile'] = $num;
        $this->_sms['Tag']['message'] = $msg;

        // combine request url
        foreach($this->_sms['Tag'] as $k => $v){
            $t[] = "{$k}={$v}";
        }
        $param=implode("&",$t);
        $url = "{$this->_sms["api"]}?{$param}";

        ob_start();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $result=curl_exec($ch);
        curl_close($ch);
        ob_clean();
        if( $result ){
            // session
            $this->session->set("REGI_CODE", $this->getCode() );
            $this->session->set("REGI_TIME", time());
            return $this->setResult( self::RES_SEND_PASS );
        }else{
            $this->messages[] = "很抱歉！簡訊伺服器發生異常，請等候一段時間後再次處理。";
            $this->setResult( self::ERR_SEND_NORESULT );
            return false;
        }
    }


    /**
     * 比對手機認證碼
     * 1. 認證碼過期
     * 2. 認證碼不符
     * 3. 認證成功，顯示註冊鈕
     * 4. 寫入紀錄
     * */
    function mapping_sms($num){

        $code = intval($this->request->getPost("smscode", 'int'));

        if( $this->session->has('REGI_TIME') &&
            ($this->session->get('REGI_TIME') < (time()-600)) ){
            $this->messages[] = "認證碼已失效，請點選接收簡訊再次取得新的認證碼。";
            $this->setResult( self::ERR_MAPPING_TIMEOUT );
            return false;
        }elseif( $num != $this->session->get("REGI_PHONE") ||
                 $code != $this->session->get("REGI_CODE")
            ){
            $this->messages[] = "認證碼比對錯誤，請確認是否填寫正確。";
            $this->setResult( self::ERR_MAPPING_NOTMATCH );
            return false;
        }else{
            return $this->setResult( self::RES_MAPPING_PASS );
        }
    }
}