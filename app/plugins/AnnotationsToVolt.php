<?php
/**
 * 處理在Action執行期間將@volt註釋解析轉存Volt變數
 * 透過$di['dispatcher']直接註冊
 *
 */
namespace Housefront\Plugins;

use \Phalcon\Mvc\User\Plugin as UserPlugin;
use \Phalcon\Events\Event;
use \Phalcon\Mvc\Dispatcher;

class AnnotationsToVolt extends UserPlugin
{
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        // get Class Annotations
        // $reflator=$annotations->get( $dispatcher->getControllerClass() );

        $annotations = $this->annotations->getMethod(
            $dispatcher->getControllerClass(),
            $dispatcher->getActiveMethod()
        );

        if( $annotations->has('volt') ){
            $annotation = $annotations->get("volt");

            // 直接將@Volt指定為變數
            // foreach ($annotation->getArguments() as $key => $value) {
            //     var_dump($key);
            //     var_dump($value);
            // }
            $this->view->setVars($annotation->getArguments());
        }
    }


    /**
     * 自動根據指定Volt變數與樣板檔案存在與否來render規則命名樣板檔
     *
     * */
    public function afterDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        $path = array();
        $path[] = PPS_APP_APPSPATH . "/views/";
        $namespace = explode("\\", $dispatcher->getNamespaceName());

        // like member/profile-phase.volt
        if( $this->view->render == 'ruleMCA' ){
            $path[] = strtolower($namespace[count($namespace)-1]."/");
            $path[] = $this->dispatcher->getControllerName().'-'.$this->dispatcher->getActionName();
            $path[] = '.volt';
            $file = implode("",$path);
            if( file_exists( $file ) ){
                $this->view->pick( strtolower($path[1].$path[2]) );
            }
        // like index/about.volt
        }elseif( $this->view->render == 'ruleCA' ){
            $path[] = $this->dispatcher->getControllerName()."/";
            $path[] = $this->dispatcher->getActionName();
            $path[] = '.volt';
            $file = implode("",$path);
            if( file_exists( $file ) ){
                $this->view->pick( strtolower($path[1].$path[2]) );
            }
        }
    }
}
