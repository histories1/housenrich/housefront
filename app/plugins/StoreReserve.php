<?php

namespace Housefront\Plugins;

use \Phalcon\Mvc\User\Plugin as UserPlugin;

/**
 * 處理地圖模式，預約看屋後處理內容
 * 1. 設定cookie
 * 2. cookie內容轉存session
 * 3. 取出顯示結構
 */
class StoreReserve extends UserPlugin
{

    public function __construct(){

    }

    public function _get_reserve_from_cookie()
    {
        if( $this->cookies->has('USER-RESERVE') ){
            $tmp = $this->cookies->get('USER-RESERVE');
            $cookie=unserialize($tmp);
        }else{
            $cookie = array("items"=>0, "itemids"=>[]);
        }
        return $cookie;
    }


    /**
     * 處理根據cookie預約看屋編號組成結構
     *
     * @return array['items','itemids','html']
     * */
    public function _get_reserve_structure($reserves=null)
    {
        // 取得cookie清單內的物件編號載入
        if( empty($reserves) ){
            $reserves=$this->_get_reserve_from_cookie();
        }

        $content=array();
        foreach($reserves['itemids'] as $crawldataId){
            $item=\Houserich\Models\Crawldata::findFirst($crawldataId);
            $tmp="<div class='box'>
                    <div class='info'>
                        <h3>{$item->title}</h3>
                        <p>{$item->addressShow}</p>";
                $tmp.= "<div class='detail'>
                            <span>".round($item->houseage)."年</span>";
            if( !empty($item->type) ){
                if( $item->floorAt == 0 ){
                $tmp.=     "<span>整棟{$item->floorTotal}樓</span>";
                }else{
                $tmp.=     "<span>{$item->floorAt}/{$item->floorTotal} 樓</span>";
                }
            }
                if( $item->areaParking ){
                $tmp.=      "{$item->areaTotal}坪（房{$item->areaMain}坪＋車{$item->areaParking}坪）";
                }else{
                $tmp.=      "{$item->areaTotal}坪";
                }
                $tmp.= "</div>
                    </div>
                    <div class='price'>
                        {$item->priceTotal}萬
                        <span class='icon-d'></span>
                    </div>
                    <a href='javascript:;'
                       class='close opt-reserve-close'
                       data-itemid='{$item->crawldataId}'>×</a>
                  </div>";
            $content[] = $tmp;
        }
        $reserves['html'] = implode("\n",$content);

        return $reserves;
    }

}