<?php
/**
 * 行情 - 實價地圖
 * */

namespace Housefront\Controllers\Price;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Price")
 *
 * @RoutePrefix("/caseprice")
 * */
class MapController extends ControllerBase
{
    /**
     * @Route('')
     *
     * @volt(pageHeader:"行情", pageDesc:"描述", render:"ruleMCA")
     * */
    public function phaseAction()
    {
    }
}