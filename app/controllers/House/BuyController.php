<?php
/**
 * 買房選單功能
 *
 * */

namespace Housefront\Controllers\House;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

/**
 * @category("House")
 *
 * @RoutePrefix("/search")
 */
class BuyController extends ControllerBase
{
    public function initialize() {
        if( $this->session->has('SEARCH') ){
            $item = (object)$this->session->get('SEARCH');
        }else{
            // 預設搜尋模式與排序模式
            $q = [
                    "display"=> [
                        // form mode
                        "fm"=> 'default',
                        // display mode
                        "mod"=> 'lists',
                        // order sort
                        "srt"=> 'initTime',
                        // order sort mode
                        "srtmod"=> 'ASC'
                    ],
                 ];
            $this->session->set('SEARCH', $q);

            $item = null;
        }

        $this->view->sform = new \Housefront\Forms\SearchForm($item);

        $this->_set_display();

        parent::initialize();
    }

    /**
     * 處理接收GET參數決定模式與排序
     * */
    protected function _set_display() {
        if( !$this->request->isGet() ){ return; }

        $Q = $this->session->get('SEARCH');
        foreach ($_GET as $key => $value) {
            $Q['display'][$key] = $value;
        }
        $this->session->set('SEARCH', $Q);
    }


    protected function _set_session($fields) {
        if( $this->session->has('SEARCH') ){
            $saler = $this->session->get('SEARCH');
            foreach ($fields as $field) {
                $saler[$field] = $_POST[$field];
            }
        }else{
            $saler = array();
            foreach ($fields as $field) {
                $saler[$field] = $_POST[$field];
            }
        }
        $this->session->set('SEARCH', $saler);
    }


    /**
     * 根據get參數直接配置SQL語法order部份！
     * */
    protected function _order(&$sql)
    {
        // 排序配置
        if( isset($_GET['srt']) ){
            //@todo : 過濾字串配置throw error
            if( $this->request->getQuery('srt','string') == 'initTime' ){
                $oderfield = 'R.richitemId';
            }elseif( $this->request->getQuery('srt','string') == 'houseAge' ){
                $oderfield = '(R.houseAgeYear*12+R.houseAgeMonth)';
            }else{
                $oderfield = $this->request->getQuery('srt','string');
            }

            if( in_array($this->request->getQuery('srtmod','string'), ["ASC","DESC"])){
                $mode = $this->request->getQuery('srtmod','string');
            }else{
                $mode = "ASC";
            }

            $this->session->set('ORDER', $oderfield );
            $this->session->set('ORDERMODE', $mode );
        }

        if( $this->session->has('ORDER') && $this->session->has('ORDERMODE') ){
            $sql .= " ORDER BY {$this->session->get('ORDER')} {$this->session->get('ORDERMODE')}";
        }
    }


    /**
     * 根據
     * */
    protected  function _filter(&$sql)
    {
        $qf = new \Phalcon\Session\Bag("QUERYFILTERS");

        if( !empty($_POST['lq']) ){
            unset($_POST['lq']);
            foreach ($_POST as $key => $value) {
                $sf = 'Q'.strtoupper($key);
                if( !empty($this->request->getPost($key,'string')) ){
                    $qf->set($sf, $value);
                }
            }
        }elseif( !empty($_POST['cq']) ){
            $qf->destroy();
        }

        $where = array();
        // keyword
        if( $qf->has('QKEYWORD') ){
            $f = $qf->get('QKEYWORD');
            $where[] = "(R.title LIKE '%{$f}%' OR R.richitemNo LIKE '%{$f}%')";
        }

        // usefor
        // if( $qf->has('QUSEFOR') ){
        //     $f = $qf->get('QUSEFOR');
        //     $where[] = "(R.usefor ";
        // }

        // // type
        // if( $qf->has('QTYPE') ){
        //     $f = $qf->get('QTYPE');
        //     $where[] = "(R.type IN ()";
        // }

        // price1
        if( $qf->has('QPRICE1') ){
            $f = $qf->get('QPRICE1');
            $where[] = "(RI.priceRichitem >= {$f})";
        }

        // price2
        if( $qf->has('QPRICE2') ){
            $f = $qf->get('QPRICE2');
            $where[] = "(RI.priceRichitem <= {$f})";
        }

        // singleprice1
        if( $qf->has('QPRICESINGLE1') ){
            $f = $qf->get('QPRICESINGLE1');
            $where[] = "(RI.priceSingle >= {$f})";
        }

        // singleprice2
        if( $qf->has('QPRICESINGLE2') ){
            $f = $qf->get('QPRICESINGLE2');
            $where[] = "(RI.priceSingle <= {$f})";
        }

        // area1
        if( $qf->has('QAREA1') ){
            $f = $qf->get('QAREA1');
            $where[] = "(RI.areaRight >= {$f})";
        }

        // area2
        if( $qf->has('QAREA2') ){
            $f = $qf->get('QAREA2');
            $where[] = "(RI.areaRight <= {$f})";
        }

        // areause1
        if( $qf->has('QAREAUSE1') ){
            $f = $qf->get('QAREAUSE1');
            $where[] = "(RI.areaUsing >= {$f})";
        }

        // areause2
        if( $qf->has('QAREAUSE2') ){
            $f = $qf->get('QAREAUSE2');
            $where[] = "(RI.areaUsing <= {$f})";
        }

        // floor1
        if( $qf->has('QFLOOR1') ){
            $f = $qf->get('QFLOOR1');
            $where[] = "(R.floorAbove >= {$f})";
        }

        // floor2
        if( $qf->has('QFLOOR2') ){
            $f = $qf->get('QFLOOR2');
            $where[] = "(R.floorAbove <= {$f})";
        }

        // houseage1
        if( $qf->has('QHOUSEAGE1') ){
            $f = $qf->get('QHOUSEAGE1');
            $where[] = "(R.houseAgeYear >= {$f})";
        }

        // houseage2
        if( $qf->has('QHOUSEAGE2') ){
            $f = $qf->get('QHOUSEAGE2');
            $where[] = "(R.houseAgeYear <= {$f})";
        }

        // var_dump($where);die();
        if( count($where) > 0 ){
            $sql.=" WHERE ".implode(" AND ", $where);
        }
    }


    /**
     * 使用類DataTable做法，個別搜尋處理
     *
     * */
    protected function _load_richitems()
    {
        $phql = "SELECT R.*, RI.*
                 FROM richitem R
                 LEFT JOIN richitem_information RI ON R.richitemId=RI.RichitemId ";
        $defaultphql = $phql;


        $this->_filter($defaultphql);

        $this->_order($defaultphql);

        // 過濾配置
        $R = new \Houserich\Models\Richitem();
        $houses = new \Phalcon\Mvc\Model\Resultset\Simple(null, $R , $R->getReadConnection()->query($defaultphql));

        $cPage = (int) $_GET["P"];
        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new PaginatorModel(
            [
                "data"  => $houses,
                "limit" => 10,
                "page"  => $cPage,
            ]
        );
        // Get the paginated results
        $this->view->houses = $paginator->getPaginate();
    }


    /**
     * @Route('')
     *
     * @volt(pageHeader:"一般條件搜尋",pageDesc:"一般條件搜尋模式",render:"ruleMCA")
     */
    public function phaseAction()
    {
        if( $this->session->get('SEARCH')['display']['fm'] == 'community' ){
            $this->assets->collection("headerStyle")->addCss('/misc/css/flexslider.css', true);

            $this->assets->collection("footerScript")->addJs('/misc/js/jquery.flexslider-min.js', true);
            $this->assets->collection("footerScript")->addJs('/misc/js/gallery.js', true);
        }elseif( $this->session->get('SEARCH')['display']['fm'] == 'mrt' ){
            $this->assets->collection("headerStyle")->addCss('/misc/css/jquery.mCustomScrollbar.css', true);
            $this->assets->collection("footerScript")->addJs('/misc/js/jquery.mCustomScrollbar.min.js', true);
        }


        // Session紀錄搜尋PHQL、筆數
        $this->_load_richitems();

        // Session紀錄物件項目(物件編號集合)
    }


    /**
     * @Route('/mrt')
     *
     * @volt(pageHeader:"捷運搜尋",pageDesc:"",render:"ruleMCA")
     */
    public function mrtAction()
    {

        $this->view->pick('house/search-default');
    }


    /**
     * @Route('/community')
     *
     * @volt(pageHeader:"社區搜尋",pageDesc:"",render:"ruleMCA")
     */
    public function communityAction()
    {

    }


    /**
     * @Route('/school')
     *
     * @volt(pageHeader:"學區搜尋",pageDesc:"",render:"ruleMCA")
     */
    public function schoolAction()
    {

    }


     /**
     * @Route('/map')
     *
     * @volt(pageHeader:"地圖搜尋",pageDesc:"",render:"ruleMCA")
     */
    public function mapAction()
    {

    }
}

