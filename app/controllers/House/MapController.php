<?php
/**
 * 地圖搜尋模式機制
 *
 * */

namespace Housefront\Controllers\House;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;


/**
 * @category("House")
 *
 * @RoutePrefix("/map")
 */
class MapController extends ControllerBase
{
    public function initialize() {
        if( $this->session->has('MAPSEARCH') ){
            $q = $this->session->get('MAPSEARCH');
            if( !empty($q['postdata']) ){
                $item = (object)$q['postdata'];
            }else{
                $item = null;
            }

            if( $this->request->getQuery('filter', 'string') ){
                foreach ($q['TAB'] as $key => $null) {
                    $q['TAB'][$key]['class']="toptab";
                }
                $q['ACTIVE']['TAB']=$this->request->getQuery('filter', 'string');
            }

            // 處理TAB與SORT設定目前設定項目
            $q['TAB'][$q['ACTIVE']['TAB']]['class'] = "toptab active";
            $q['SORT'][$q['ACTIVE']['SORT']]['class'] = "sortbtn current";
            if( $q['ACTIVE']['SORTMODE'] == 'ASC' ){
                $q['SORT'][$q['ACTIVE']['SORT']]['append'] = '&nbsp;<i class="icon-up-b"></i>';
            }elseif( $q['ACTIVE']['SORTMODE'] == 'DESC' ){
                $q['SORT'][$q['ACTIVE']['SORT']]['append'] = '&nbsp;<i class="icon-down-b"></i>';
            }
            $this->session->set('MAPSEARCH', $q);
        }else{
            // 預設篩選模式與排序模式
            $q = [
                    "TAB" => array(
                        'all' => [
                            'id'    =>  "tabAll",
                            'class' =>  "toptab",
                            'label' =>  "全部",
                            'data'  =>  'all'
                        ],
                        'new' => [
                            'id'    =>  "tabNew",
                            'class' =>  "toptab",
                            'label' =>  "新上架",
                            'data'  =>  'new'
                        ],
                        'down' => [
                            'id'    =>  "tabDown",
                            'class' =>  "toptab",
                            'label' =>  "有降價",
                            'data'  =>  'down'
                        ]
                    ),
                    "SORT" => array(
                        'priceTotal' => [
                            'id'    =>  "sortPrice",
                            'class' =>  "sortbtn",
                            'label' =>  "總價",
                            'append'=>  '&nbsp;<i></i>',
                            'field'  =>  'stprice'
                        ],
                        'areaMain' => [
                            'id'    =>  "sortArea",
                            'class' =>  "sortbtn",
                            'label' =>  "坪數",
                            'append'=>  '&nbsp;<i></i>',
                            'field'  =>  'starea'
                        ],
                        'houseage' => [
                            'id'    =>  "sortHouseage",
                            'class' =>  "sortbtn",
                            'label' =>  "屋齡",
                            'append'=>  '&nbsp;<i></i>',
                            'field'  =>  'sthouseage'
                        ]
                    ),
                    "ACTIVE"=> [
                        // form mode
                        "fm"=> 'map',
                        // display mode
                        "TAB"=> 'all',
                        // order sort
                        "SORT"=> 'priceTotal',
                        // order sort mode
                        "SORTMODE"=> 'ASC'
                    ],
                    "PAGER" => [
                        "limit"     => 10,
                        "current"   => 1
                    ],
                    "postdata" => null
                 ];

            if( $this->request->getQuery('filter', 'string') ){
                foreach ($q['TAB'] as $key => $item) {
                    $q['TAB'][$key]['class']="toptab";
                }
                $q['ACTIVE']['TAB']=$this->request->getQuery('filter', 'string');
            }

            // tab default
            $q['TAB'][$q['ACTIVE']['TAB']]['class'] = "toptab active";
            // sort default
            $q['SORT'][$q['ACTIVE']['SORT']]['class'] = "sortbtn current";
            if( $q['ACTIVE']['SORTMODE'] == 'ASC' ){
                $q['SORT'][$q['ACTIVE']['SORT']]['append'] = '&nbsp;<i class="icon-up-b"></i>';
            }elseif( $q['ACTIVE']['SORTMODE'] == 'DESC' ){
                $q['SORT'][$q['ACTIVE']['SORT']]['append'] = '&nbsp;<i class="icon-down-b"></i>';
            }
            $this->session->set('MAPSEARCH', $q);

            $item = null;
        }

        $this->view->mapform = new \Housefront\Forms\MapForm(@$item);

        parent::initialize();
    }

    /**
     * 選擇城市後切換行政區
     * @Route('/formelms')
     * */
    public function formelmsAction()
    {
        // 載入行政區
        if( isset($_POST['elm']) && $_POST['elm']== 'districts' ){
            if( empty($_POST['city']) || $_POST['city']=='縣市' ){
                echo '請先選擇城市！';
            }else{
                $districts = \Houserich\Models\Districts::find(["city=:C:","bind"=>['C'=>$_POST['city']]]);
                $html = null;
                foreach ($districts as $i => $item) {
                $html .= '<label for="district'.$i.'"><input type="checkbox" id="district'.$i.'" name="district[]" value="'.$item->district.'" /><span>'.$item->district.'</span></label>';
                }
                echo $html;
            }
        }


        return false;
    }


    /**
     * @Route('/search')
     *
     * @todo : 改成ajax模式仿TAB與SORT寫入session結構內
     *
     * 處理根據參數配置Session並實際進行過濾後回傳Json！
     * marks座標點、contents右側列表結構、pages分頁結構
     */
    public function searchAction()
    {
        if( $this->request->isPost() ){
            $q=$this->session->get('MAPSEARCH');
            if( $_POST['rBtn'] ){
                $_POST=null;
            }else{
                unset($_POST['rBtn']);
            }
            $q['postdata']=$_POST;
            $this->session->set('MAPSEARCH', $q);
        }

        // $this->dispatcher->forward(['action'=>'phase']);
        $this->response->redirect('/map');
        // return false;
    }


    /**
     * 根據session來處理搜尋結果
     * @todo : model pagination部份加入cache機制。
     * @todo : 將TAB/SORT/POSTDATA內容組合轉換為唯一識別字串，進行資料比對若比對相同則不再重新處理一次。
     * @todo : 紀錄當前分頁數，若頁面重新整理仍可一次顯示至該分頁數。
     * @todo : 針對前一項目，研究使用clone物件後調整部份參數方式處理。
     * */
    protected function _set_houseitem() {
        $ms = $this->session->get('MAPSEARCH');

        $houses = \Houserich\Models\Crawldata::query()->columns('*');

        $this->hasfilter=false;

        // filter
        if( count($ms['postdata']) > 0 ){
            // 縣市
            if( !empty($ms['postdata']['city']) && $ms['postdata']['city'] != '縣市' ){
                $houses->andWhere("addressCity='{$ms['postdata']['city']}'");
                $this->hasfilter=true;
            }

            // 行政區
            if( !empty($ms['postdata']['district']) ){
                $houses->inWhere('addressDistrict', $ms['postdata']['district']);
                $this->hasfilter=true;
            }

            // 總價
            if( !empty($ms['postdata']['totalprice'][0]) ){
                $houses->andWhere("priceTotal >= :p1:", ['p1'=>intval($ms['postdata']['totalprice'][0])]);
                $this->hasfilter=true;
            }
            if( !empty($ms['postdata']['totalprice'][1]) ){
                $houses->andWhere("priceTotal <= :p2:", ['p2'=>$ms['postdata']['totalprice'][1]]);
                $this->hasfilter=true;
            }

            // 型態
            if( !empty($ms['postdata']['type']) ){
                $houses->inWhere('type', $ms['postdata']['type']);
                $this->hasfilter=true;
            }

            // 格局
            if( !empty($ms['postdata']['petternroom']) ){
                if( $ms['postdata']['petternroom'] == 'others' ){
                    if( !empty($ms['postdata']['patternrooms'][0]) ){
                        $houses->andWhere("patternRoom >= :f1:", ['f1'=>$ms['postdata']['patternrooms'][0]]);
                        $this->hasfilter=true;
                    }
                    if( !empty($ms['postdata']['patternrooms'][1]) ){
                        $houses->andWhere("patternRoom <= :f2:", ['f2'=>$ms['postdata']['patternrooms'][1]]);
                        $this->hasfilter=true;
                    }
                }else{
                    $houses->andWhere("patternRoom = :f0:", ['f0'=>$ms['postdata']['petternroom']]);
                    $this->hasfilter=true;
                }
            }

            // 樓層(@todo : 整棟判斷)
            if( !empty($ms['postdata']['floor']) ){
                if( $ms['postdata']['floor'] == 'others' ){
                    if( !empty($ms['postdata']['flooro'][0]) ){
                        $houses->andWhere("floorAt >= :f1:", ['f1'=>$ms['postdata']['flooro'][0]]);
                        $this->hasfilter=true;
                    }
                    if( !empty($ms['postdata']['flooro'][1]) ){
                        $houses->andWhere("floorAt <= :f2:", ['f2'=>$ms['postdata']['flooro'][1]]);
                        $this->hasfilter=true;
                    }
                }else{
                    $houses->andWhere("floorAt = :f0:", ['f0'=>$ms['postdata']['floor']]);
                    $this->hasfilter=true;
                }
            }

            // 屋齡
            if( !empty($ms['postdata']['houseage']) ){
                if( $ms['postdata']['houseage'] == 'others' ){
                    if( !empty($ms['postdata']['houseageo'][0]) ){
                        $houses->andWhere("houseage >= :h1:", ['h1'=>$ms['postdata']['houseageo'][0]]);
                        $this->hasfilter=true;
                    }
                    if( !empty($ms['postdata']['houseageo'][1]) ){
                        $houses->andWhere("houseage <= :h2:", ['h2'=>$ms['postdata']['houseageo'][1]]);
                        $this->hasfilter=true;
                    }
                }else{
                    $houses->andWhere("houseage <= :h0:", ['h0'=>$ms['postdata']['houseage']]);
                    $this->hasfilter=true;
                }
            }

            // 坪數(@todo : 下一階段加入兩種坪數分開處理機制)
            if( !empty($ms['postdata']['area'][0]) ){
                $houses->andWhere("areaTotal >= :a1:", ['a1'=>intval($ms['postdata']['area'][0])]);
                $this->hasfilter=true;
            }
            if( !empty($ms['postdata']['area'][1]) ){
                $houses->andWhere("areaTotal <= :a2:", ['a2'=>$ms['postdata']['area'][1]]);
                $this->hasfilter=true;
            }

            // 車位
            if( !empty($ms['postdata']['parking']) ){
                // $houses->andWhere("parking = :a2:", ['a2'=>$ms['postdata']['parking']]);
                $houses->andWhere("parking = {$ms['postdata']['parking']}");
                $this->hasfilter=true;
            }

            // 關鍵字
            if( !empty($ms['postdata']['title']) ){
                $houses->andWhere("title LIKE '%{$ms['postdata']['title']}%'");
                $this->hasfilter=true;
            }

        }

        /**
         * 針對新上架設定條件
         * */
        if( $ms['ACTIVE']['TAB'] == 'new' ){
            $houses->andWhere("initTime >= :ts:",['ts'=> (time()-7*86400)]);
            $this->hasfilter=true;
        }elseif( $ms['ACTIVE']['TAB'] == 'down' ){
            $houses->andWhere("priceDown IS NOT NULL");
            $this->hasfilter=true;
        }

        // sort
        $houses->orderBy("{$ms['ACTIVE']['SORT']} {$ms['ACTIVE']['SORTMODE']}");

        // limit
        $houses->limit(2000);

        // 快取過濾過的分頁以便ajax load!
        // $houses = \Houserich\Models\Crawldata::find([]);
        $this->session->set("RESULTSET", $houses->execute());

        if( !empty($_GET["page"]) ){
            $currentPage = (int)$_GET["page"];
        }else{
            $currentPage = 1;
        }

        // The data set to paginate
        $paginator = new PaginatorModel(
            [
                "data"  => $this->session->get('RESULTSET'),
                "limit" => 10,
                "page"  => $currentPage,
            ]
        );

        // 加入分頁樣式設定
        $pager = new \Phalcon\Paginator\Pager( $paginator, [
                        'layoutClass' => 'Phalcon\Paginator\Pager\Layout\Bootstrap',
                        'rangeLength' => 5,
                        'urlMask'     => '?page={%page_number}',
                 ]);
        $this->view->pager = $pager;

        return $paginator->getPaginate();
    }


    /**
     * 預設地圖模式介面
     * @Route('/')
     *
     * @Jslibrary("maps")
     *
     * @volt(pageHeader:"一般條件搜尋",pageDesc:"一般條件搜尋模式",render:"")
     */
    public function phaseAction()
    {
        if( !$this->request->getQuery('page') ){
            $this->assets->collection("headerStyle")->addCss('/misc/css/jquery.mCustomScrollbar.css', true);
            $this->assets->collection("headerStyle")->addCss('/misc/bower_component/jquery-loading/dist/jquery.loading.min.css', true);

            $this->assets->collection("headerScript")->addJs('/misc/js/jquery.mCustomScrollbar.min.js', true);
            $this->assets->collection("headerStyle")->addCss('/misc/bower_component/jquery-loading/dist/jquery.loading.min.js', true);
            $this->assets->collection("headerScript")->addJs('/misc/js/reserve.js', true);

            $this->view->container_parent_class = 'map-container';
        }else{
            $this->view->setMainView("map/list");
        }

        // Get the paginated results
        $this->view->houseitems = $this->_set_houseitem();

        // 取得總計數值(Instantiate the Query)
        if( !$this->hasfilter ){
            $sql = "SELECT COUNT(*) AS amt FROM crawldata";
            $result_set = $this->dbCrawler->query($sql);
            $result_set->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
            $res = $result_set->fetchAll($result_set);
            $this->view->total_houseitems=intval($res[0]['amt']);
        }else{
            $this->view->total_houseitems=$this->view->houseitems->total_items;
        }

        // 載入預約看屋內容
        if( $this->session->has('USER') ){
            $reserve_plugin=new \Housefront\Plugins\StoreReserve();
            $reserves = $reserve_plugin->_get_reserve_from_cookie();
            $USER=$this->session->get('USER');
            $USER['RESERVES'] = $reserve_plugin->_get_reserve_structure($reserves);
            $this->session->set('USER', $USER);
        }
    }


    /**
     * 載入
     * @Route('/loadgeojson')
     *
     * @see https://storage.googleapis.com/maps-devrel/google.json
     * 單一座標點
     * {
     *  "type": "Feature",
        "geometry": {
            "type": "Point", "coordinates": [102.0, 0.5]
        },
        "properties": {"prop0": "value0"}
     * }
     * */
    public function loadgeojsonAction()
    {
        $houses = \Houserich\Models\Crawldata::query()->columns('*');

        $ms = $this->session->get('MAPSEARCH');
        if( empty($ms['postdata']) ){
            $location = '台北市';
            $houses->andWhere("addressCity='{$location}'");
        }else{
            // 縣市
            if( !empty($ms['postdata']['city']) && $ms['postdata']['city'] != '縣市' ){
                $houses->andWhere("addressCity='{$ms['postdata']['city']}'");
            }
            // 行政區
            if( !empty($ms['postdata']['district']) ){
                $houses->inWhere('addressDistrict', $ms['postdata']['district']);
            }
            if( count($ms['postdata']['district']) == 1){
                $location=$ms['postdata']['district'];
            }else{
                $location=$ms['postdata']['city'];
            }
        }
        $rows=$houses->execute();

        $features=array();
        foreach($rows as $id => $mark){
            $geometry['type'] = 'Feature';
            $geometry['id'] = intVal($id);
            $geometry['properties'] = ['id'=>$mark->crawldataId];
            /**
             * @todo : 經緯度數值取反了！故目前給google 必須反過來配置
             * */
            $geometry['geometry'] = ['type'=>'Point', 'coordinates' => [ $mark->lng, $mark->lat] ];

            array_push($features, $geometry);
        }
        $top["type"] = "FeatureCollection";
        $top["features"] = $features;

        header('Content-type: application/json');
        $response = new \Phalcon\Http\Response();
        $response->setContent(json_encode($top, JSON_NUMERIC_CHECK));
        return $response;
    }


    /**
     * 處理載入更多物件
     * @Route('/loadpage')
     */
    public function loadpageAction()
    {
        $q=$this->session->get('MAPSEARCH');

        if( (int)$_GET["page"] ){
            $q['PAGER']['current'] = (int)$_GET["page"];
        }

        $paginator = new PaginatorModel(
            [
                "data"  => $this->session->get('RESULTSET'),
                "limit" => $q['PAGER']['limit'],
                "page"  => $q['PAGER']['current'],
            ]
        );

        // 加入分頁設定
        $pager = new \Phalcon\Paginator\Pager( $paginator, [
                        'layoutClass' => 'Phalcon\Paginator\Pager\Layout\Bootstrap',
                        'rangeLength' => 5,
                        'urlMask'     => '/map/loadpage?page={%page_number}',
                 ]);

        $houses = $paginator->getPaginate();

        $html = '<div class="boxes">';
        foreach ($houses->items as $i => $item) {
            // 新上架
            $ex1 = null;
            if( $item->newinweek ){
                $ex1 = 'new';
            }
            // 有降價
            $ex2 = null;
            if( !empty($i->priceDown) ){
                $ex2 = 'down';
            }

            if($item->floorAt == 0){
                $fs = "整棟{$item->floorTotal}樓";
            }else{
                $fs = "{$item->floorAt} / {$item->floorTotal}樓";
            }

            $html .= "<div class='box all {$ex1} {$ex2}' data-stprice='{$item->priceTotal}' data-starea='{$item->areaTotal}' data-sthouseage='{$item->houseage}'>";
            $html .= '<div class="info">';
                $html .= "<h3>{$item->title}</h3>";
                $html .= "<p>{$item->addressShow}</p>";
                $html .= "<div class='detail'>";
                if( $item->areaParking ){
                    $html .= "<span>{$item->areaTotal}}坪（房{$item->areaMain}}坪+車{$item->areaParking}}坪）</span>";
                }else{
                    $html .= "<span>{$item->areaTotal}坪</span>";
                }
                  $html .= "<span>{$item->houseage}年</span>
                            <span>{$fs}</span>
                            <span>{$item->patterns}</span>
                          </div>";
            $html .= "</div>";
            $html .= "<div class='price'>{$item->priceTotal}萬<span class='icon-d'></span></div>";
            $html .= "<div class='btns'>";
                $html .= "<a target='_blank' href='{$item->sourcelink}' class='btn btn-white txt-ora'>查看詳情</a>";
                $html .= "<button type='button' class='btn btn-white txt-blue'>預約看屋</button>";
                $html .= "<button type='button' class='btn btn-white txt-white'>降價通知</button>";
            $html .= "<a target='_blank' href='{$item->sourceMap}' class='btn btn-white'>地圖街景</a>";
            $html .= "<button type='button' class='btn btn-like'><span>加入收藏</span></button>";
            $html .= "</div>";
            $html .= "<addr style='display:none;' data-name='{$item->title}' data-lat='{$item->lat}' data-lng='{$item->lng}'>{$item->addressCity}{$item->addressDistrict}</addr>";
            $html .= "</div>";
        }

        $html .= '<aside class="list-pager-nums">
                  <a class="next" href="'.$this->url->get('/map/loadpage?page='.$houses->next).'">載入更多</a>
                  </aside></div>';

        print($html);
        return false;
    }


    /**
     * 切換頁籤、排序處理session變化與資料設定
     * @Route('/updatesession')
     * */
    public function updatesessionAction()
    {
        $q=$this->session->get('MAPSEARCH');
        switch ($_POST['do']) {
            case 'tab':
                foreach ($q['TAB'] as $filter => $arr) {
                    $q['TAB'][$filter]['class']='toptab';
                }
                $q['ACTIVE'][strtoupper($_POST['do'])]=$_POST['filter'];
                break;
            case 'sort':
                foreach ($q['SORT'] as $sort => $arr) {
                    $q['SORT'][$sort]['class']='sortbtn';
                    $q['SORT'][$sort]['append']='&nbsp;<i></i>';
                }
                $q['ACTIVE']['SORT']=$_POST['sort'];
                $q['ACTIVE']['SORTMODE']=$_POST['sortmode'];
                break;
            // 之後處理表單ajaxload使用
            default:
                break;
        }
        $this->session->set('MAPSEARCH', $q);
        return false;
    }


    /**
     * 使用cookie處理機制(指定網域、過期時間)
     * 1. 每次點選預約看屋時時
     * 2. 處理增加或移除物件[add|sub]
     *
     * @Route('/setreserve')
     *
     * */
    public function setReserveAction()
    {
        if( !$this->session->has('USER') ){
            echo json_encode(['res'=>0, 'msg'=>'您必須先登入系統，才能使用預約看屋功能，請先點選右上角登入。'], true);
            return false;
        }

        // 判斷cookie是否存在與過期時間
        $reserve_plugin=new \Housefront\Plugins\StoreReserve();
        $reserves=$reserve_plugin->_get_reserve_from_cookie();
        // var_dump($reserves);die();

        $do=$this->request->getPost('do', 'string');
        $id=$this->request->getPost('itemid', 'int');
        // 更新設定值
        if( $do=='add' ){
            array_push($reserves['itemids'],intval($id));
            $reserves['itemids']=array_unique($reserves['itemids']);
        }elseif( $do=='sub' ){
            if(($key = array_search($id, $reserves['itemids'])) !== false) {
                unset($reserves['itemids'][$key]);
            }
        }

        // 更新個數
        $reserves['items']=count($reserves['itemids']);
        /**
         * 設定預約看屋使用的cookie結構
         * */
        $this->cookies->set('USER-RESERVE', serialize($reserves), time()+1800);
        $this->cookies->send();

        // 直接呼叫取得
        $reserve_with_html=$reserve_plugin->_get_reserve_structure();

        $reserve_with_html['res']=1;
        echo json_encode($reserve_with_html, true);
        return false;
    }
}