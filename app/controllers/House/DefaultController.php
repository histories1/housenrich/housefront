<?php
/**
 * 物件展示資訊格式機制
 * 1. 處理顯示查詢後列表所需物件資訊
 * 2. 處理顯示單一物件內容所需物件資訊
 * 3. 處理顯示地圖模式物件所需資訊
 * */

namespace Housefront\Controllers\House;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("House")
 *
 * @RoutePrefix("/demo")
 */
class DefaultController extends ControllerBase
{
    /**
     * @Route('/')
     *
     * 載入全部或指定縣市所有座標點
     */
    public function defaultAction()
    {
        $houses = \Houserich\Models\Crawldata::find(["addressCity='台北市'"]);

        // $data['type'] = 'FeatureCollection';
        foreach ($houses as $house) {
            // $d1['type'] = 'Feature';
            //     $d['type'] = 'Point';
            //     $d['coordinates'] = [floatval($house->lat), floatval($house->lng)];
            // $d1['geometry'] = $d;
            // $data['features'][] = $d1;
            $data[] = [floatval($house->lat), floatval($house->lng)];
        }

//         print('{
//     "type": "FeatureCollection",
//     "features": [{
//         "type": "Feature",
//         "geometry": {
//             "type": "Point",
//             "coordinates": [25.034157,121.554604]
//         }
//     },{
//         "type": "Feature",
//         "geometry": {
//             "type": "Point",
//             "coordinates": [25.032357,121.554621]
//         }
//     },{
//         "type": "Feature",
//         "geometry": {
//             "type": "Point",
//             "coordinates": [25.034157,121.554604]
//         }
//     },{
//         "type": "Feature",
//         "geometry": {
//             "type": "Point",
//             "coordinates": [25.034157,121.554604]
//         }
//     }]
// }');

        echo json_encode($data);
        return false;
    }
}