<?php
/**
 * 我要賣房選單功能
 *
 * */

namespace Housefront\Controllers\House;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("House")
 *
 * @RoutePrefix("/saler")
 */
class SaleController extends ControllerBase
{
    public function initialize() {
        if( $this->session->has('SALER') ){
            $item = (object)$this->session->get('SALER');
        }else{
            $item = null;
        }
        $this->view->form = new \Housefront\Forms\SalerForm($item);

        parent::initialize();
    }
    /**
     * @Route('')
     *
     * @volt(pageHeader:"我要賣房",pageDesc:"",render:"ruleMCA")
     */
    public function step1Action()
    {
        $this->view->current = 1;
    }


    protected function _set_session($fields) {
        if( $this->session->has('SALER') ){
            $saler = $this->session->get('SALER');
            foreach ($fields as $field) {
                $saler[$field] = $_POST[$field];
            }
        }else{
            $saler = array();
            foreach ($fields as $field) {
                $saler[$field] = $_POST[$field];
            }
        }
        $this->session->set('SALER', $saler);
    }


    /**
     * @Route('/step2')
     *
     * @volt(pageHeader:"我要賣房",pageDesc:"",render:"ruleMCA")
     */
    public function step2Action()
    {
        if( !$this->request->isPost() ){
            $this->flashSession->warning("偵測到錯誤的資料內容，請重新處理。");
            return $this->response->redirect('saler');
        }

        $item = new \Houserich\Models\Saler();
        // check csrf then session
        $fields = ['fullName', 'gender', 'cellphone'];
        $this->view->form->bind($_POST, $item);
        if (!$this->view->form->isValid($_POST) ){
            $messages = $this->view->form->getMessages();
            foreach ($this->view->form->getMessages(false) as $obj) {
                if( in_array($obj->getField(), $fields) ){
                    $msg[] = $obj->getMessage();
                }
            }
            if( isset($msg) && count($msg) > 0 ){
                $this->flashSession->warning("您提供的資料有誤，請根據以下訊息修正後再次送出：".implode("\n", $msg));
                return $this->response->redirect('saler');
            }
            $this->_set_session($fields);
        }else{
            $this->_set_session($fields);
        }

        $this->view->current = 2;
    }


    /**
     * @Route('/step3')
     *
     * @volt(pageHeader:"我要賣房",pageDesc:"",render:"ruleMCA")
     */
    public function step3Action()
    {
        if( !$this->request->isPost() ){
            $this->flashSession->warning("偵測到錯誤的資料內容，請重新處理。");
            return $this->response->redirect('saler');
        }

        $item = new \Houserich\Models\Saler();
        // check csrf then session
        $fields = ['type', 'addressCity', 'addressDistrict', 'addressRoad', 'addressLane', 'addressAlley', 'addressNo', 'addressNoEx', 'addressFloor', 'addressFloorEx', 'areaHouse', 'areaParking'];
        $this->view->form->bind($_POST, $item);
        if (!$this->view->form->isValid($_POST) ){
            $messages = $this->view->form->getMessages();
            foreach ($this->view->form->getMessages(false) as $obj) {
                if( in_array($obj->getField(), $fields) ){
                    $msg[] = $obj->getMessage();
                }
            }
            if( isset($msg) && count($msg) > 0 ){
                $this->flashSession->warning("您提供的資料有誤，請根據以下訊息修正後再次送出：".implode("\n", $msg));
                return $this->response->redirect('saler');
            }
            $this->_set_session($fields);
        }else{
            $this->_set_session($fields);
        }

        $this->view->current = 3;
    }


    /**
     * @Route('/step4')
     *
     * @volt(pageHeader:"我要賣房",pageDesc:"",render:"ruleMCA")
     */
    public function step4Action()
    {
        if( !$this->request->isPost() ){
            $this->flashSession->warning("偵測到錯誤的資料內容，請重新處理。");
            return $this->response->redirect('saler');
        }

        $item = new \Houserich\Models\Saler();
        // check csrf then session
        $fields = ['price', 'community'];
        $this->view->form->bind($_POST, $item);
        if (!$this->view->form->isValid($_POST) ){
            $messages = $this->view->form->getMessages();
            foreach ($this->view->form->getMessages(false) as $obj) {
                if( in_array($obj->getField(), $fields) ){
                    $msg[] = $obj->getMessage();
                }
            }
            if( isset($msg) && count($msg) > 0 ){
                $this->flashSession->warning("您提供的資料有誤，請根據以下訊息修正後再次送出：".implode("\n", $msg));
                return $this->response->redirect('saler');
            }
            $this->_set_session($fields);
        }else{
            $this->_set_session($fields);
        }

        // session轉成object透握form進行綁定
        $saler = $this->session->get('SALER');
        if( $this->session->has('USER') ){
            $saler['PeopleId'] = $this->session->get('USER')['People']['peopleId'];
        }
        $this->view->form->bind($saler, $item);
        if( !$item->save() ){
            $this->flashSession->danger("無法完成您的請求，訊息：".implode(",", $item->getMessages()));
            return $this->response->redirect('saler');
        }else{
            $this->session->remove('SALER');
        }

        $this->view->current = 4;
    }
}

