<?php
/**
 * 地圖預約機制
 *
 * */

namespace Housefront\Controllers\House;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("House")
 *
 * @RoutePrefix("/reserve")
 */
class ReserveController extends ControllerBase
{

    public function initialize() {

        parent::initialize();
    }

    /**
     * @Route('/1')
     *
     * @volt(pageHeader:"",pageDesc:"",render:"ruleMCA")
     */
    public function step1Action()
    {
        // 處理將cookie設定的清單轉到session!
        if( $this->session->has('USER') ){
            $reserve_plugin=new \Housefront\Plugins\StoreReserve();
            $reserves = $reserve_plugin->_get_reserve_from_cookie();
            $USER=$this->session->get('USER');
            $USER['RESERVES'] = $reserve_plugin->_get_reserve_structure($reserves);
            $this->session->set('USER', $USER);
        }else{
            $this->flashSession->warning("您已登出無法處理預約看屋紀錄，請重新回到地圖操作。");
            return $this->response->redirect('/map');
        }
    }

    /**
     * @Route('/2')
     *
     * @volt(pageHeader:"",pageDesc:"",render:"ruleMCA")
     */
    public function step2Action()
    {
        // 處理取得session資料
        if( !$this->session->has('USER') ) {
            $this->flashSession->warning("您已登出無法處理預約看屋紀錄，請重新回到地圖操作。");
            return $this->response->redirect('/map');
        }elseif ( !$this->request->isPost() ) {
            $this->flashSession->warning("資料判斷異常，請您透過預約表單重新操作。");
            return $this->response->redirect('/reserve/1');
        }

        $pid=$this->request->getPost('peopleId','int');
        $USER = $this->session->get('USER');
        if( $pid != $USER['People']['peopleId']){
            $this->flashSession->warning("表單資料有誤，請重新操作！");
            return $this->response->redirect('/reserve/1');
        }

        // $people=\Houserich\Models\People::findFirst($pid);
        // 寫入people_reserve紀錄
        foreach($USER['RESERVES']['itemids'] as $i => $crawldataId){
            $reserve[$i]=new \Houserich\Models\PeopleReserve();
            $reserve[$i]->PeopleId = $pid;
            $reserve[$i]->CrawdataId = $crawldataId;
            /**
             * @todo : 調整需要紀錄的物件資訊欄位
             * */
            $crawldata=\Houserich\Models\Crawldata::findFirst($crawldataId);
            if( $crawldata ){
                $tmp=$crawldata->toArray();
                $reserve[$i]->title = $tmp['title'];
            }else{
                $reserve[$i]->title = null;
            }

            // $people->PeopleReserve = $reserve;
            if( !$reserve[$i]->save() ){
                echo '寫入預約看屋紀錄發生錯誤，訊息:'.implode(",". $reserve[$i]->getMessages());
                var_dump($USER['RESERVES']['itemids']);
            }
        }

        // 清除cookie以及session reserve片段
        $this->cookies->get('USER-RESERVE')->delete();
        unset($USER['RESERVES']);
        $this->session->set('USER', $USER);
    }
}