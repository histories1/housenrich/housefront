<?php
/**
 * 好多房入口畫面與靜態頁面
 * */

namespace Housefront\Controllers;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Index")
 *
 * @RoutePrefix("/")
 */
class IndexController extends ControllerBase
{

    /**
     * @Route('')
     *
     * @Jslibrary("maps")
     */
    public function indexAction()
    {
        $this->assets->collection("headerStyle")->addCss('/misc/css/jquery.mCustomScrollbar.css', true);
        $this->assets->collection("headerScript")->addJs('/misc/js/jquery.mCustomScrollbar.min.js', true);

        $this->view->container_parent_class = 'map-container';

        $this->view->pick("index-default");
    }


    /**
     * @Route('file/access/{modelclass}/{uuid}')
     */
    public function fileAccessAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $uuid = $this->dispatcher->getParam('uuid');
        $model = "\\Houserich\\Models\\".$this->dispatcher->getParam('modelclass');

        $media = $model::findFirstByUUID( $uuid );

        if( $media ){
            $ex = explode('-',$uuid);
            // 從專案目錄起算路徑(前端)
            $prefix = realpath(getcwd()."/../../../house/public_html").DIRECTORY_SEPARATOR;
            $randDir = $ex[0];
            $targetDir = "uploads".DIRECTORY_SEPARATOR.$randDir.DIRECTORY_SEPARATOR;
            ini_set('open_basedir', "$prefix:{$prefix}{$targetDir}");
            $tmp_accesspath = str_replace("housefront", "house", $_SERVER['SCRIPT_URI']);
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-type: '.$media->mime);
            header('Content-length: '.$media->size);
            echo file_get_contents($tmp_accesspath);
        }

        return fasle;
    }


    /**
     * @Route('news')
     *
     * @volt(pageHeader:"房市新聞",pageDesc:"",render:"ruleCA")
     */
    public function newsAction()
    {
    }


    /**
     * @Route('about')
     *
     * @volt(pageHeader:"關於我們",pageDesc:"",render:"ruleCA")
     */
    public function aboutAction()
    {
    }



    /**
     * @Route('houserich-faq')
     *
     * @volt(pageHeader:"了解好多房",pageDesc:"問與答畫面",render:"ruleCA")
     */
    public function houseFaqAction()
    {
    }


    /**
     * @Route('terms')
     *
     * @volt(pageHeader:"使用者條款",pageDesc:"",render:"ruleCA")
     */
    public function termsAction()
    {
        $this->assets->collection("headerStyle")->addCss('/misc/css/term.css', true);
    }


    /**
     * @Route('privacy')
     *
     * @volt(pageHeader:"隱私權聲明",pageDesc:"",render:"ruleCA")
     */
    public function privacyAction()
    {
        $this->assets->collection("headerStyle")->addCss('/misc/css/term.css', true);
    }


    /**
     * @Route('salerstatement')
     *
     * @volt(pageHeader:"賣方議價服務聲明",pageDesc:"",render:"ruleCA")
     */
    public function salerstatementAction()
    {
        $this->assets->collection("headerStyle")->addCss('/misc/css/term.css', true);
    }


    /**
     * @Route('buyerstatement')
     *
     * @volt(pageHeader:"買方議價服務聲明",pageDesc:"",render:"ruleCA")
     */
    public function buyerstatementAction()
    {
        $this->assets->collection("headerStyle")->addCss('/misc/css/term.css', true);
    }


    /**
     * @Route('sitemap')
     *
     * @volt(pageHeader:"網站地圖",pageDesc:"",render:"ruleCA")
     */
    public function sitemapAction()
    {
    }


    /**
     * @volt(pageHeader:"找不到路徑！",pageDesc:"",render:"ruleMCA")
     * */
    public function notfoundAction()
    {
        // var_dump($this->router);
        // var_dump($this->dispatcher);

        $this->view->pick("notfound");
    }
}

