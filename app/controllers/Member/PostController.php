<?php
/**
 * 會員中心 - 刊登管理
 *
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/member/post")
 * */
class PostController extends ControllerBase
{
    public function initialize() {
        if( !$this->session->has('USER') ) {
            $this->flashSession->warning('您已登出會員，請重新登入。');
            return $this->response->redirect('/');
        }
        parent::initialize();
    }

    /**
     * @Route('/')
     *
     * @volt(pageHeader:"刊登管理", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/post",
        resource: ""
     * })
     * */
    public function phaseAction()
    {
    }
}