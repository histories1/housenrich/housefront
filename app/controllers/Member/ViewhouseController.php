<?php
/**
 * 會員中心 - 找房條件
 * 1. 找房條件設定
 * 2. 找房清單
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/member/viewhouse")
 * */
class ViewhouseController extends ControllerBase
{
    public function initialize() {
        if( !$this->session->has('USER') ) {
            $this->flashSession->warning('您已登出會員，請重新登入。');
            return $this->response->redirect('/');
        }
        parent::initialize();
    }

    /**
     * @Route('')
     *
     * @volt(pageHeader:"找房條件", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/findhouse",
        resource: ""
     * })
     * */
    public function phaseAction()
    {
    }
}