<?php
/**
 * 會員中心 - 通知中心
 *
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/member")
 * */
class NotificationController extends ControllerBase
{
    public function initialize() {
        if( !$this->session->has('USER') ) {
            $this->flashSession->warning('您已登出會員，請重新登入。');
            return $this->response->redirect('/');
        }
        parent::initialize();
    }

    /**
     * @Route('/notification')
     *
     * @volt(pageHeader:"通知中心", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/notification",
        resource: ""
     * })
     * */
    public function phaseAction()
    {
    }


    /**
     * @Route('/notification/message')
     *
     * @volt(pageHeader:"通知中心訊息", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/notification/message",
        resource: ""
     * })
     * */
    public function messageAction()
    {
    }

    /**
     * @Route('/reduction')
     *
     * @volt(pageHeader:"降價通知", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/notification/reduction",
        resource: ""
     * })
     * */
    public function reductionAction()
    {
    }
}