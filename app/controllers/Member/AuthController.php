<?php
/**
 * 會員驗證機制
 * 1. 註冊
 *      網站註冊
 *      Facebook註冊
 *      Line註冊
 *      Yahoo註冊
 * 2. 登入
 *
 * 3. 登出
 *
 * 4. 郵件驗證 / SMS驗證
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/auth")
 * */
class AuthController extends ControllerBase
{
    /**
     * @Route('/login')
     *
     * @volt(pageHeader:"登入介面", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/auth/login",
        resource: ""
     * })
     * */
    public function loginAction()
    {
        $this->view->container_parent_class = 'reg-bg';
    }


    /**
     * @Route('/login/post')
     * */
    public function loginPostAction()
    {
        $account = $this->request->getPost('account', 'string');
        $password = $this->request->getPost('password', 'string');

        $people = \Houserich\Models\PeopleInformation::findFirstByAccount( $account );
        if( !$people ){
            $this->view->pageMessage = array(
                                        'title' => '登入失敗',
                                        'msg'   => '您輸入的帳號錯誤，請重新操作。',
                                        'redirect' => false,
                                        'redirect_url' => null
                                       );
        }elseif( $this->security->checkHash($password, $people->password) ){
            // logger

            // session
            $data['PeopleInformation'] = $people->toArray();
            $data['People'] = $people->People->toArray();
            if( $people->People->PeopleInvoice ){
                $data['PeopleInvoice'] = $people->People->PeopleInvoice->toArray();
            }
            $this->session->set('USER', $data);

            $this->view->pageMessage = array(
                                        'title' => '登入成功',
                                        'msg'   => '系統將於2秒後引導至會員中心。',
                                        'redirect' => true,
                                        'redirect_url' => $this->url->get('member')
                                       );
        }else{
            // To protect against timing attacks. Regardless of whether a user exists or not, the script will take roughly the same amount as it will always be computing a hash.
            $this->security->hash(rand());

            $this->view->pageMessage = array(
                                        'title' => '登入失敗',
                                        'msg'   => '您輸入的密碼錯誤，請重新操作。',
                                        'redirect' => false,
                                        'redirect_url' => null
                                       );
        }

        $this->dispatcher->forward(["action" => "result"]);
    }


    /**
     * @Route('/forgetpasswd')
     *
     * @volt(pageHeader:"忘記密碼", pageDesc:"", render:"ruleMCA")
     * */
    public function forgetpasswdAction()
    {
        $this->view->container_parent_class = 'reg-bg';
    }


    /**
     * @Route('/logout')
     *
     * @volt(pageHeader:"登出", pageDesc:"", render:"ruleMCA")
     * */
    public function logoutAction()
    {
        // logger

        $this->session->remove('USER');

        return $this->response->redirect('/');
    }


    /**
     * @Route('/register')
     *
     * @volt(pageHeader:"註冊", pageDesc:"", render:"")
     *
     * @Jslibrary("formvalid")
     *
     * @acl({
        path: "/auth/register",
        resource: ""
     * })
     * */
    public function registerAction()
    {
        $this->view->container_parent_class = 'reg-bg';

        // 尚未通過手機認證
        if( !$this->session->has('REGI_VERTIFIED') ){

            $this->view->form = new \Housefront\Forms\PeopleForm();
            $this->view->pick('member/auth-registep1');
        // 註冊第二步驟欄位
        }elseif( !$this->session->has('REGI_FINISHED') ){
            $this->view->form = new \Housefront\Forms\PeopleInformationForm();
            $this->view->pick('member/auth-registep2');
        // 引導訊息並清除Session
        }else{
            // clear session
            $this->session->remove('REGI_PHONE');
            $this->session->remove('REGI_CODE');
            $this->session->remove('REGI_TIME');
            $this->session->remove('REGI_VERTIFIED');
            $this->session->remove('REGI_FINISHED');

            // 顯示訊息導向會員中心
            $this->view->pick('member/auth-result');
        }
    }


    /**
     * 處理註冊簡訊驗證流程
     *  @Route('/register/sms')
     *
     * @volt(pageHeader:"手機簡訊驗證", pageDesc:"", render:"")
     * */
    public function smsPostAction()
    {
        //
        if( $this->request->isAjax() ){
            // 產生smscode儲存session
            $sms = new \Housefront\Plugins\Smscode();
            $act = $this->request->getPost('do');
            // check_sms / send_sms / mapping_sms
            $method = "{$act}_sms";
            $sms->$method( $this->request->getPost('cellphone', 'string') );

            $data = ['res' => $sms->getResult(), 'msg' => implode("\n", $sms->getMessages())];
            switch( $sms->getResult() )
            {
                // 門號ok / input delay
                case 'pass_check':
                    $this->session->set('REGI_PHONE', $this->request->getPost('', 'string'));
                break;
                // 傳送驗證碼、計算時間 / ajax post
                case 'pass_send':
                    $code = $sms->getCode();

                    $this->session->set('REGI_CODE', $code);
                    $this->session->set('REGI_TIME', time());
                break;
                // 驗證通過寫入DB與session /
                case 'pass_mapping':
                    $people = new \Houserich\Models\People();
                    $form = new \Housefront\Forms\PeopleForm();
                    $form->bind($_POST, $people);

                    if (!$form->isValid($_POST)) {
                        $messages = $form->getMessages();
                        $msg = array('表單欄位檢查發生錯誤，請根據以下訊息修正後再次送出：');
                        foreach ($messages as $obj) {
                            $msg[] = $obj->getMessage();
                        }
                        $data["res"] = 0;
                        $data["msg"] = implode("<BR/>", $msg);
                    }else{
                        // 附加審核驗證成功紀錄
                        $people->vertifyCellphone = 1;

                        if( !$people->save() ){
                            $msg = array('儲存您的資料發生錯誤，請根據訊息修正後再次送出：');
                            $msgs= implode(" ", array_merge($msg, $people->getMessages() ));
                            // logger

                            $data["res"] = 0;
                            $data["msg"] = implode("<BR/>", $msgs);
                        }else{
                            $this->session->set('REGI_VERTIFIED', 1);
                            $this->session->set('REGI_PEOPLEID', $people->peopleId);
                        }
                    }
                break;
            }

            echo json_encode($data);
        }else{
            echo json_encode(["res"=> 0, "msg"=>"發生錯誤！請回到畫面重新處理。"]);
        }
        return false;
    }


    /**
     * @Route('/register/post')
     * */
    public function registerPostAction()
    {
        $form = new \Housefront\Forms\PeopleInformationForm();
        $people = new \Houserich\Models\PeopleInformation();
        $form->bind($_POST, $people);

        if (!$form->isValid($_POST)) {
            $messages = $form->getMessages();
            $msg = array('表單欄位檢查發生錯誤，請根據以下訊息修正後再次送出：');
            foreach ($messages as $obj) {
                $msg[] = $obj->getMessage();
            }
            $this->flashSession->warning( implode("<BR/>", $msg) );
            return $this->response->redirect("auth/register");
        }else{

            try{

                if( !empty($this->REPOST) ){
                    return $this->response->redirect("auth/register");
                }

                // 配置最後狀態
                $people->lastStatecode = \Houserich\Models\PeopleStatus::STAT_INIT;

                if( !$people->save() ){
                    $msg = array('註冊您的資料發生錯誤，請根據訊息修正後再次送出：');
                    $msgs= implode(" ", array_merge($msg, $people->getMessages() ));
                    // logger
                    $this->flashSession->warning( $msgs );
                    return $this->response->redirect("auth/register");
                }else{

                    // act1. peopleStatus
                    $status = new \Houserich\Models\PeopleStatus();
                    $status->statecode = \Houserich\Models\PeopleStatus::STAT_INIT;
                    $status->PeopleId = $people->PeopleId;
                    $status->SetterId = $people->PeopleId;
                    if( !$status->save() ){
                        // logger
                    }

                    // act2. session
                    $this->session->remove('REGI_PEOPLEID');
                    $this->session->set('REGI_FINISHED', 1);
                    $member = $people->toArray();
                    $member['statecode'] = $status->statecode;
                    $member['stateLabel'] = $status->getStatecodeLabel();
                    $this->session->set('USER', $member);

                    // act3. 導向簡訊發送接收頁面（預先發送一次！）
                    $this->view->pageMessage = array(
                                                'title' => '會員註冊完成',
                                                'msg' => '您已成功完成註冊，將於2秒後為您引導至會員中心。',
                                                'redirect' => true,
                                                'redirect_url' => $this->url->get('member')
                                               );
                    $this->dispatcher->forward(["action" => 'result']);
                }
            } catch (\PDOException $e) {
            } catch (\Exception $e) {
                $msg = array('寫入捷運資料發生錯誤，請根據以下訊息修正後再次送出：');
                $msg[] = $e->getMessage();
                $this->flashSession->danger( implode("\n", $msg) );
                return $this->response->redirect("auth/register");
            }
        }

    }


    /**
     * 處理註冊/登入結果畫面
     * */
    public function resultAction()
    {
        $this->view->container_parent_class = 'reg-bg';
        $this->view->pick('member/auth-result');
    }

    var $FB;

    protected function _getFacebookAccesstoken() {
        // load SDK
        $fb = new  \Facebook\Facebook( $this->config->auth->facebook->toArray() );
        // 處理交換Access Token
        $helper = $fb->getRedirectLoginHelper();
        try {
          $accessToken = $helper->getAccessToken();
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          echo 'Graph returned an error: ' . $e->getMessage();
          return false;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          return false;
        }

        if(isset($accessToken)) {
          // Logged in!
          $d = [
                "service" => "facebook",
                "access_token" =>(string) $accessToken,
               ];
          $this->session->set("OAUTH",$d);
        }

        return true;
    }


    /**
     * @Route('/facebook')
     * */
    public function facebookAction()
    {
        $this->FB = new \Facebook\Facebook( $this->config->auth->facebook->toArray() );

        if( @$_GET['to'] == "oauth") {
            // 處理導向FB登入
            $helper = $this->FB->getRedirectLoginHelper();
            $url = $helper->getLoginUrl( $this->config->auth->facebook->redirect_url, ['email','public_profile']);

            if( isset($_GET['state']) ) {
                $this->session->set('FBRLH_state', $_GET['state']);
            }

            header('Location : '.$url);
            return ;
        }

        // load SDK
        if( !$this->session->has("OAUTH") ||
            ($this->session->get("OAUTH")["service"] != "facebook") ){
                $this->_getFacebookAccesstoken();
        }else{
            $oAuth2Client = $this->FB->getOAuth2Client();
            $tokenMetadata = $oAuth2Client->debugToken($this->session->get("OAUTH")["access_token"]);
            if( !$tokenMetadata->getIsValid() ){
                $this->_getFacebookAccesstoken();
            }
        }

        try {
            $response = $this->FB->get('/me/?fields=id,name,gender,email,verified', $this->session->get("OAUTH")['access_token'] );
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // logger
            var_dump($e->getMessage());
            exit;
        }
        // 使用者資訊
        /**
         * array (size=5)
          'id' => string '10209438403051080' (length=17)
          'name' => string 'San Huang' (length=9)
          'gender' => string 'male' (length=4)
          'email' => string 'huangxianqin@gmail.com' (length=22)
          'verified' => boolean true
         * */
        $userArr = $response->getDecodedBody();

        $infos['fullName'] = $userArr['name'];
        $infos['account'] = str_replace(' ','', $userArr['name']);
        $sex = ['male'=>'男','female'=>'女'];
        $infos['gender'] = $sex[$userArr['gender']];
        $infos['email'] = $userArr['email'];
        $infos['password'] = 'nonpasswd';
        // 暫時配置
        $infos['age'] = 0;

        $res = $this->_proc_save_member($userArr['id'], 'facebook', $infos);

        //附加移除Session紀錄
        $this->session->remove("FBRLH_state");

        if( $res == 'register' ){
            $this->view->pageMessage = array(
                                        'title' => "Facebook帳號登入成功",
                                        'msg' => "您已登錄Facebook帳號，將於2秒後為您引導至會員中心。",
                                        'redirect' => true,
                                        'redirect_url' => $this->url->get('member')
                                       );
        }elseif($res == 'login') {
            $this->view->pageMessage = array(
                                        'title' => "Facebook帳號註冊完成",
                                        'msg' => "您已成功透過Facebook註冊，將於2秒後為您引導至會員中心。",
                                        'redirect' => true,
                                        'redirect_url' => $this->url->get('member')
                                       );
        }

        $this->dispatcher->forward(["action" => 'result']);

        return false;
    }


    /**
     * @Route('/line')
     * */
    public function lineAction()
    {
        $cline = $this->config->auth->line->toArray();
        if( @$_GET['to'] == "oauth") {
            $data['response_type'] = 'code';
            $data['client_id'] = $cline['Channel_ID'];
            $data['redirect_uri'] = urlencode($cline['Callback_URL']);
            // for scrf
            $data['state'] = $this->security->getToken();
            $this->session->set('LINE_state', $this->security->getTokenKey());
            $query_string=http_build_query($data, '', '&amp;');

            // 處理導向Line登入
            $url = "{$cline['AUTH_URL']}?{$query_string}";

            header('Location : '.$url);
            return ;
        }

        // if( $_GET['state'] != $this->session->get('LINE_state') || empty($_GET['code']) ){
        if( !$this->security->checkToken($this->session->get('LINE_state'), $_GET['state']) || empty($_GET['code']) ){
            // logger
            var_dump($_GET);
            die();
            return false;
        }else{
            // exchange access_token
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $cline['Endpoint_URL']);
            curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
            // curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type"=>"application/x-www-form-urlencoded"));
            $data['grant_type'] = 'authorization_code';
            $data['client_id'] = $cline['Channel_ID'];
            $data['client_secret'] = $cline['Channel_secret'];
            $data['code'] = $_GET['code'];
            $data['redirect_uri'] = urlencode($cline['Callback_URL']);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data, '', '&amp;'));
            $response = curl_exec($ch);
            // Then, after your curl_exec call:
            // $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            // $header = substr($response, 0, $header_size);
            // $body = substr($response, $header_size);
            curl_close($ch);
            $json = json_decode($response, true);
            var_dump($json);
            die();
        }

        return false;
    }


    /**
     * @Route('/yahoo')
     * */
    public function yahooAction()
    {
        $cyahoo = $this->config->auth->yahoo->toArray();
        $oauthapp = new \Yahoo\YahooOAuthApplication(
                            $cyahoo['OAUTH_CONSUMER_KEY'],
                            $cyahoo['OAUTH_CONSUMER_SECRET'],
                            $cyahoo['OAUTH_APP_ID'],
                            $cyahoo['OAUTH_CALLBACK_URL']
                        );

        // handle openid/oauth
        if( isset($_REQUEST['openid_mode']) ) {
          switch($_REQUEST['openid_mode']) {
            case 'oauth':
              // handle yahoo simpleauth popup + redirect to yahoo! open id with open app oauth request
              header('Location: '.$oauthapp->getOpenIDUrl($oauthapp->callback_url));
              return ;
            break;

            case 'id_res':
            // if( !$this->session->has("OAUTH") || empty($this->session->get("OAUTH")["YAHOO_access_token"]) ){
                // extract approved request token from open id response
                $request_token = new \Yahoo\YahooOAuthRequestToken($_REQUEST['openid_oauth_request_token'], '');
                // exchange request token for access token
                $oauthapp->token = $oauthapp->getAccessToken($request_token);
                // store access token for later
                $_SESSION['yahoo_oauth_access_token'] = $oauthapp->token->to_string();

                $session=array("YAHOO_request_token" => $request_token->to_string(), "YAHOO_access_token" =>$oauthapp->token->to_string());
                $this->session->set("OAUTH", $session);
            // }

            # fetch user profile
            $oauthapp->token = \Yahoo\YahooOAuthAccessToken::from_string( $this->session->get("OAUTH")["YAHOO_access_token"] );
            /**
             * object(stdClass)[107]
                  public 'profile' =>
                    object(stdClass)[104]
                      public 'guid' => string 'GOCEYFEYLH4QUA33EL5EJDB2SQ' (length=26)
                      public 'ageCategory' => string 'A' (length=1)
                      public 'created' => string '2016-09-30T08:48:47Z' (length=20)
                      public 'image' =>
                        object(stdClass)[105]
                          public 'height' => string '192' (length=3)
                          public 'imageUrl' => string 'https://s.yimg.com/dh/ap/social/profile/profile_b192.png' (length=56)
                          public 'size' => string '192x192' (length=7)
                          public 'width' => string '192' (length=3)
                      public 'lang' => string 'en-US' (length=5)
                      public 'memberSince' => string '2016-09-24T15:32:53Z' (length=20)
                      public 'nickname' => string 'personalwork' (length=12)
                      public 'profileUrl' => string 'http://profile.yahoo.com/GOCEYFEYLH4QUA33EL5EJDB2SQ' (length=51)
                      public 'isConnected' => string 'false' (length=5)
                      public 'bdRestricted' => string 'true' (length=4)
             * */
            $profile = $oauthapp->getProfile();
            // var_dump($profile);die();

            $infos['fullName'] = $profile->profile->nickname;
            $infos['account'] = str_replace(' ','', $profile->profile->nickname);
            $sex = ['male'=>'男','female'=>'女'];
            $gender = ( !empty($profile->profile->gender) )?$profile->profile->gender:'male';
            $infos['gender'] = $sex[$gender];
            // Yahoo資料似乎不提供Email
            // $infos['email'] = $profile->profile->email;
            $infos['password'] = 'nonpasswd';
            // 暫時配置
            $infos['age'] = 0;
            $res = $this->_proc_save_member($profile->profile->guid, 'yahoo', $infos);

            if( $res == 'register' ){
                $this->view->pageMessage = array(
                                            'title' => "Yahoo帳號登入成功",
                                            'msg' => "您已登錄Yahoo帳號，將於2秒後為您引導至會員中心。",
                                            'redirect' => true,
                                            'redirect_url' => $this->url->get('member')
                                           );
            }elseif($res == 'login') {
                $this->view->pageMessage = array(
                                            'title' => "Yahoo帳號註冊完成",
                                            'msg' => "您已成功透過Yahoo註冊，將於2秒後為您引導至會員中心。",
                                            'redirect' => true,
                                            'redirect_url' => $this->url->get('member')
                                           );
            }else{
                $this->view->pageMessage = array(
                                            'title' => "Yahoo帳號註冊發生錯誤",
                                            'msg' => $res,
                                            'redirect' => false
                                           );
            }

            $this->dispatcher->forward(["action" => 'result']);
            break;
          }
        } else {

        }

        return false;
    }


    /**
     * 處理第三方驗證返回後儲存people model紀錄流程
     *
     * */
    protected function _proc_save_member( $ExternalID, $type, $infos ){
        $member = \Houserich\Models\People::findFirstByExternalID($ExternalID);

        if( !$member ){
            $member = new \Houserich\Models\People();
            $member->type = $type;
            $member->externalID = $ExternalID;
            $member->vertifyCellphone = 0;

                $mInfo = new \Houserich\Models\PeopleInformation();
                foreach ($infos as $field => $value) {
                    $mInfo->{$field} = $value;
                }

                $status = new \Houserich\Models\PeopleStatus();
                $status->statecode = \Houserich\Models\PeopleStatus::STAT_INIT;
            $member->PeopleInformation = $mInfo;
            $member->PeopleStatus = $status;

            if( !$member->save() ) {
                // loger

                return implode(',', $member->getMessages());
            }else{
                $mInfos = $mInfo->toArray();
                $mInfos['statecode'] = $status->statecode;
                $mInfos['stateLabel'] = $status->getStatecodeLabel();

                $proc='register';
            }
        }else{
            $mInfos = $member->PeopleInformation->toArray();
            // @todo : 附加最後狀態處理

            $proc='login';

        }

        // session and clear
        $this->session->remove("OAUTH");
        $this->session->set("USER", $mInfos);

        return $proc;
    }
}

