<?php
/**
 * 好顧問幫幫我（客服）
 * 1. 顯示區塊
 * 2. 客服表單處理
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/service")
 * */
class ServiceController extends ControllerBase
{
    /**
     * @Route('/')
     *
     * @volt(pageHeader:"好顧問幫幫我", pageDesc:"描述", render:"")
     *
     * @acl({
        path: "/service",
        resource: ""
     * })
     * */
    public function phaseAction()
    {
    }
}