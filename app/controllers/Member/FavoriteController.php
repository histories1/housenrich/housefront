<?php
/**
 * 會員中心 - 收藏管理
 *
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/member/favorite")
 * */
class FavoriteController extends ControllerBase
{
    public function initialize() {
        if( !$this->session->has('USER') ) {
            $this->flashSession->warning('您已登出會員，請重新登入。');
            return $this->response->redirect('/');
        }
        parent::initialize();
    }

    /**
     * @Route('/')
     *
     * @volt(pageHeader:"收藏管理", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/favorite",
        resource: ""
     * })
     * */
    public function phaseAction()
    {
    }
}