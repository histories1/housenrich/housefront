<?php
/**
 * 會員中心 - 找房條件
 * 1. 找房條件設定
 * 2. 找房清單
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/member/findhouse")
 * */
class FindhouseController extends ControllerBase
{
    public function initialize() {
        if( !$this->session->has('USER') ) {
            $this->flashSession->warning('您已登出會員，請重新登入。');
            return $this->response->redirect('/');
        }
        parent::initialize();
    }

    /**
     * @Route('/list')
     *
     * @volt(pageHeader:"理想清單", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/findhouse/list",
        resource: ""
     * })
     * */
    public function listAction()
    {

    }


    /**
     * @Route('/setting')
     *
     * @volt(pageHeader:"理想好房設定", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/findhouse/setting",
        resource: ""
     * })
     * */
    public function settingAction()
    {

    }
}