<?php
/**
 * 會員中心 - 帳戶管理
 * 1. 保證金帳戶
 * 2. 退款帳戶
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/member/account")
 * */
class AccountController extends ControllerBase
{
    public function initialize() {
        if( !$this->session->has('USER') ) {
            $this->flashSession->warning('您已登出會員，請重新登入。');
            return $this->response->redirect('/');
        }
        parent::initialize();
    }

    /**
     * @Route("(/security)?")
     *
     * @volt(pageHeader:"保證金帳戶", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/account/security",
        resource: ""
     * })
     * */
    public function securityAction()
    {
    }


    /**
     * @Route("/refund")
     *
     * @volt(pageHeader:"退款帳戶", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/account/refund",
        resource: ""
     * })
     * */
    public function refundAction()
    {
    }
}