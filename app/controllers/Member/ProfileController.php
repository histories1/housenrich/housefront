<?php
/**
 * 會員中心 - 基本資料
 *
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/member")
 * */
class ProfileController extends ControllerBase
{
    public function initialize() {
        if( !$this->session->has('USER') ) {
            $this->flashSession->warning('您已登出會員，請重新登入。');
            return $this->response->redirect('/');
        }
        parent::initialize();
    }

    /**
     * @Route(
            "(/profile)?",
            methods={"POST", "GET"},
            name="profile-phase"
        )
     *
     * @volt(
            pageHeader:"基本資料",
            pageDesc:"會員中心預設畫面，顯示會員基本欄位。",
            render:"ruleMCA"
        )
     * @acl({
        path: "/member(/profile)?",
        resource: ""
     * })
     * */
    public function phaseAction()
    {
        // var_dump($this->session->get('USER')['PeopleInformation']);die();
        // 載入會員表單欄位
        $this->view->form = new \Housefront\Forms\PeopleInformationForm((object) $this->session->get('USER')['PeopleInformation'], ['membercenter'=>1]);

        // 發票欄位
        $PIN = $this->session->get('USER')['PeopleInvoice'];
        $this->view->formI = new \Housefront\Forms\PeopleInvoiceForm((object) $PIN);
    }


    /**
     * @Route("/profile/editPassword")
     *
     * @volt(pageHeader:"更改密碼", pageDesc:"會員中心預設畫面，顯示會員基本欄位。", render:"ruleMCA")
     */
    public function editPasswordAction()
    {
        if( $this->request->isPost() && $this->request->isAjax() ){
            $PI = \Houserich\Models\PeopleInformation::findFirst( $this->session->get("USER")['PeopleInformation']['PeopleId'] );
            if( !$PI ){
                echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>'無法取得您的資料，請重新登入處理。']);
                return false;
            }elseif( !$this->security->checkHash($this->request->getPost('oldpassword','string'), $PI->password) ){
                echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>'您輸入的原始密碼錯誤，請重新處理。']);
                return false;
            }else{
                $PI->password = $this->request->getPost('password','string');
                if( !$PI->save() ){
                    echo json_encode(["res"=>0, "msg"=>'偵測到錯誤的資料內容，請重新處理。']);
                    return false;
                }else{
                    echo json_encode(["res"=>1, "msg"=>'已完成變更您的密碼，下次登入請使用新密碼。']);
                    return false;
                }
            }
        }
    }



    /**
     * @Route("/profile/save/{model:[a-zA-Z\_\-]+}")
     *
     * @volt(pageHeader:"修改資料動作", pageDesc:"處理修改會員資料欄位，包含密碼。")
     * @acl({
        path: "/member/save/*",
        resource: ""
     * })
     * */
    public function saveAction()
    {
        if( !$this->request->isPost() || !$this->request->isAjax() ){
            echo json_encode(["res"=>0, "msg"=>'偵測到錯誤的資料內容，請重新處理。']);
            return false;
        }

        if( $this->dispatcher->getParam('model') == "PeopleInformation" ){
            $item = new \Houserich\Models\PeopleInformation();
            $form = new \Housefront\Forms\PeopleInformationForm(null, ['membercenter'=>1]);
            $label = "基本資料";
        }else{
            $item = new \Houserich\Models\PeopleInvoice();
            $form = new \Housefront\Forms\PeopleInvoiceForm();
            $label = "發票資料";
        }
        $form->bind($_POST, $item);

        if (!$form->isValid($_POST) ){
            $msg[] = $label.'欄位檢查發生錯誤，請根據以下訊息修正後再次送出：';
            $messages = $form->getMessages();
            foreach ($messages as $obj) {
                $msg[] = $obj->getMessage();
            }
            echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>implode("\n", $msg)]);
        }else{

            try{
                if( !$item->save() ){
                    echo json_encode(["res"=>0, "msg"=>"更新您的{$label}發生錯誤，請根據以下訊息修正後再次送出：".implode("\n", $item->getMessages()) ]);
                }else{
                    // update session
                    $USER=$this->session->get('USER');
                    $USER[$this->dispatcher->getParam('model')] = $_POST;
                    $this->session->set('USER', $USER);
                    echo json_encode(["res"=>1, "display"=>"#alertSuccess", "msg"=> "已完成編輯您的資料"]);
                }
            } catch (\PDOException $e) {
            } catch (\Exception $e) {
                echo json_encode(["res"=>0, "alerts"=>"popupAlert", "msg"=>"寫入{$label}資料發生錯誤，請根據以下訊息修正後再次送出：".implode("\n", $e->getMessage())]);
            }

        }

        return false;
    }


    /**
     * 處理手機簡訊驗證流程
     *  @Route('/smscheck')
     *
     * @volt(pageHeader:"手機簡訊驗證", pageDesc:"", render:"")
     * */
    public function smsCheckAction()
    {
        if( $this->request->isAjax() ){
            // 產生smscode儲存session
            $sms = new \Housefront\Plugins\Smscode();
            $act = $this->request->getPost('do');
            // setphone_sms / send_sms / mapping_sms
            $method = "{$act}_sms";
            $sms->$method( $this->request->getPost('cellphone', 'string') );

            $data = ['res' => $sms->getResult(), 'msg' => implode("\n", $sms->getMessages())];
            switch( $sms->getResult() )
            {
                // 傳送驗證碼、計算時間 / ajax post
                case 'pass_send':
                    $code = $sms->getCode();

                    $this->session->set('REGI_CODE', $code);
                    $this->session->set('REGI_TIME', time());
                break;
                // 驗證通過寫入DB與session /
                case 'pass_mapping':
                    $people = \Houserich\Models\People::findFirst( $this->session->get('USER')['People']['peopleId'] );
                    $people->cellphone = $this->request->getPost('cellphone', 'string');
                    $people->vertifyCellphone = 1;
                    if( !$people->save() ){
                        $msg = array('儲存您的資料發生錯誤，請根據訊息修正後再次送出：');
                        $msgs= implode(" ", array_merge($msg, $people->getMessages() ));
                        // logger

                        $data["res"] = 0;
                        $data["msg"] = implode("<BR/>", $msgs);
                    }
                break;
            }

            echo json_encode($data);
        }else{
            echo json_encode(["res"=> 0, "alters"=>"popupAlert", "msg"=>"發生錯誤！請回到畫面重新處理。"]);
        }
        return false;
    }
}