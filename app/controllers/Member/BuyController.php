<?php
/**
 * 會員中心 - 看屋管理
 *
 * */

namespace Housefront\Controllers\Member;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Member")
 *
 * @RoutePrefix("/member/buy")
 * */
class BuyController extends ControllerBase
{
    public function initialize() {
        if( !$this->session->has('USER') ) {
            $this->flashSession->warning('您已登出會員，請重新登入。');
            return $this->response->redirect('/');
        }
        parent::initialize();
    }

    /**
     * @Route('/')
     *
     * @volt(pageHeader:"看屋管理", pageDesc:"描述", render:"ruleMCA")
     *
     * @acl({
        path: "/member/buy",
        resource: ""
     * })
     * */
    public function phaseAction()
    {
    }
}