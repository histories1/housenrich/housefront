<?php
/**
 * 估價系統模組
 * */

namespace Housefront\Controllers\Assess;

use \Personalwork\Mvc\Controller\Base\Application as ControllerBase,
    \Personalwork\Logger\Adapter\Database as Logger;

/**
 * @category("Assess")
 *
 * @RoutePrefix("/assess")
 */
class DefaultController extends ControllerBase
{
    /**
     * @Route('')
     *
     * @volt(pageHeader:"估價系統",pageDesc:"",render:"ruleMCA")
     *
     */
    public function phaseAction()
    {
    }
}

