<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('PPS_APP_APPSPATH', realpath('../app'));

define('PPS_APP_PROJECTNAME', 'housefront');


try {

    /**
     * Read services
     */
    include PPS_APP_APPSPATH . "/../config/services.php";

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
