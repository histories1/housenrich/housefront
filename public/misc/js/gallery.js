// For Gallery
$(function(){
     var _width = $(window).width(); 
     if(_width < 480){
			  $(window).load(function(){
				$('#carousel').flexslider({
				  animation: "slide",
				  controlNav: false,
				  animationLoop: false,
				  slideshow: false,
				  itemWidth: 80,
				  itemMargin: 1,
				  asNavFor: '#slider'
				});
		  
				$('#slider').flexslider({
				  animation: "fade",
				  controlNav: false,
				  animationLoop: false,
				  slideshow: false,
				  sync: "#carousel",
				  start: function(slider){
					$('body').removeClass('loading');
				  }
				});
			  });
		 
     }
	 else if(_width > 480){

			  $(window).load(function(){
				$('#carousel').flexslider({
				  animation: "slide",
				  controlNav: false,
				  animationLoop: false,
				  slideshow: false,
				  itemWidth: 100,
				  itemMargin: 1,
				  asNavFor: '#slider'
				});
		  
				$('#slider').flexslider({
				  animation: "fade",
				  controlNav: false,
				  animationLoop: false,
				  slideshow: false,
				  sync: "#carousel",
				  start: function(slider){
					$('body').removeClass('loading');
				  }
				});
			  });
		
	 }
});

// for object
lightbox.option({
  'resizeDuration': 200,
  'wrapAround': true,
  'fadeDuration':300,
  'imageFadeDuration':300,
  'albumLabel':"%1 / %2"
})
