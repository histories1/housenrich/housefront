/**
 * 處理點選「預約看屋」機制
 *
 * 1. optReserve_click() 地圖模式點選預約看屋展示街景畫面
 * called by
 * /map/list
 *
 * 2. optReserveClose_click() 加入預約看屋紀錄列表項目移除處理機制
 * called by
 * /map/list
 * /reserve-step1
 * */
function optReserve_click( AJAX_URL )
{
    $(document)
    .on("click", ".opt-reserve", function(){
        nowBtn=$(this);
        crawldataId=$(this).data('itemid');
        $.ajax({
            url: AJAX_URL,
            type: 'post',
            data: {
                itemid : crawldataId, do: 'add'
            },
            dataType: 'json',
            // 按鈕樣式變化處理(xxx-inact)
            beforeSend: function(){
                nowBtn.hide();
            },
            success: function(resp){
                nowBtn.show();
                if( resp.res==1 ){
                    // 更新reserve物件清單、統計個數
                    $("#reserveItems").text(resp.items);
                    $(".reserve-wrap").html(resp.html);
                    $("#reserveContrl").fadeIn();
                    if( parseInt($(".reserve-panel").css("left"),10) < 0 ){
                        $(".reserve-panel").animate({
                            left:  0
                        });
                    }
                }else{
                    alert(resp.msg);
                }

            }
        });
        return false;
    });
}

function optReserveClose_click( AJAX_URL )
{
    $(document)
    .on("click", ".opt-reserve-close", function(){
        nowBlock=$(this).parents('.box');
        crawldataId=$(this).data('itemid');
        $.ajax({
            url: AJAX_URL,
            type: 'post',
            data: {
                itemid : crawldataId, do: 'sub'
            },
            dataType: 'json',
            // 按鈕樣式變化處理(xxx-inact)
            beforeSend: function(){
            },
            success: function(resp){
                if( resp.res==1 ){
                    // 更新reserve物件清單、統計個數
                    $("#reserveItems").text(resp.items);
                    nowBlock.fadeOut();
                }else{
                    alert(resp.msg);
                }

            }
        });
    })
}

// hide reserve-panel
function optReservePanelClose_click()
{
    $(document)
    .on("click", ".reserve-btn", function(){
        var $lefty = $(".reserve-panel");
            $lefty.animate({
              left: parseInt($lefty.css("left"),10) == 0 ? -$lefty.outerWidth() : 0
        });
    });
}