
$(document).ready(function() {
	

	// login popup
	$('#loginpop').popup({
		transition: 'all 0.3s'
	 });
    $('#regpop').popup({
        transition: 'all 0.3s'
     });
	 
	 //go top
	$("#gotop").hide();
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#gotop').fadeIn();
			} else {
				$('#gotop').fadeOut();
			}
		});
		$('#gotop').click(function () {
			$('body,html').animate({
				scrollTop:0
			}, 800);
			return false;
		});
	});
	
	// 好顧問幫幫我
	$(".onlineser").mCustomScrollbar();

});


// 好顧問幫幫我
$('.help-btn').click(function() {
    $(this).toggleClass("current");
	$(".help-inner").toggle();
});   
$('ul.tabs2 li').click(function(){
	var tab_id = $(this).attr('data-tab');
	$('ul.tabs2 li').removeClass('current');
	$('.tab2-content').removeClass('current');
	$(this).addClass('current');
	$("#"+tab_id).addClass('current');
})


//--------------------- for search

// search imglist slideshow
var swiper = new Swiper('.swiper-container', {
	pagination: '.swiper-pagination',
	paginationClickable: true,
	nextButton: '.swiper-button-next',
	prevButton: '.swiper-button-prev',
	spaceBetween: 30
});

// search-item btn and search panel
$('.search-option .option-panel').hide();
$('.search-option a.option-btn').click(function(event){

	//$(this).next('.option-panel').toggleClass('openthis');
	event.stopPropagation();
	//e.preventDefault();
    var $this = $(this).parent().find('.option-panel');
    $(".search-option .option-panel").not($this).slideUp();
    $this.slideToggle();

});
$(document).click(function(e){
    var targetbox = $('.option-panel');
    if(!targetbox.is(e.target) && targetbox.has(e.target).length === 0){
        $('.option-panel').slideUp('fast');
    }
});



// select
$('.forcombo select').comboSelect()

/**
* on Change
*/

$('.js-select').change(function(e, v){
  $('.idx').text(e.target.selectedIndex)
  $('.val').text(e.target.value)
})

/**
* Open select
*/

$('.js-select-open').click(function(e){
  $('.js-select').focus()
  e.preventDefault();
})

/**
* Open select
*/

$('.js-select-close').click(function(e){
  $('.js-select').trigger('comboselect:close')
  e.preventDefault();
})


//--------------------- for member

//ps popup
$('.btn-pop').hover(function () {
    $(this).next('.ps-pop').fadeToggle();
})
	
//member nav
var Membernav = function(el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;
 
    // Variables privadas
    var links = this.el.find('.link');
    // Evento
    links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
}
Membernav.prototype.dropdown = function(e) {
    var $el = e.data.el;
        $this = $(this),
        $next = $this.next();
  
    $next.slideToggle();
    $this.parent().toggleClass('open');
  
    if (!e.data.multiple) {
        $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
    };
}
var membernav = new Membernav($('#membernav'), false);
