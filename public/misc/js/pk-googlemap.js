/**
 * Google Map v3
 * 官方 javascript api
 * pk-googlemap.js
 * */

/**
 * Map物件
 * */
var gMapObj;

/**
 * geocoder物件:處理查詢地點地址
 * */
var geocoder;

/**
 * 座標點集合
 * */
var markers = [];


/**
 * 預設根據指定地點做為地圖中心點位置
 * @param function callback 處理將取得的經緯度透過匿名函式傳遞
 * */
function _map_center_withaddr(assignAddress, toZoom, callback){
  var queryLatLng=null;

  if( assignAddress == '' ){
    assignAddress = '台北市';
  }

  // console.log( assignAddress );
  geocoder.geocode({address:assignAddress}, function(results, status){
      if (status === google.maps.GeocoderStatus.OK) {
          gMapObj.setCenter(results[0].geometry.location);

          if( toZoom ){
            gMapObj.setZoom( toZoom );
          }

          if( typeof callback === 'function' ){
            // console.log( results[0] );
            callback(results[0].geometry.location, results[0].formatted_address );
          }
      }else {
        callback(status, null);
      }
  });
}

/**
 * 根據已配置經緯度設定地圖中心點位置並直接將該座標標記顯示
 * */
function _map_center_withlatlng(lat, lng, toZoom){
  var latlng = new google.maps.LatLng( lat, lng);
  gMapObj.setCenter(latlng);
  if( toZoom ){
    gMapObj.setZoom( toZoom );
  }

  _map_add_marker(latlng, 1);
}

/**
 * 於地圖上增加一個座標點
 * @param bool clear 是否清除地圖已標示座標點
 * */
function _map_add_marker(latLng, clear) {
  if( clear ){
    for(var i=0;i<markers.length;i++){
        markers[i].setMap(null);
    }
  }

  var marker = new google.maps.Marker({
                        map: gMapObj,
                        position: latLng
                    });
  markers.push(marker);
}

/**
 * 將預設物件名稱欄位對應到座標標籤欄位
 * */
function _map_field_mapping( toField, fieldVal ) {
  toField.val( fieldVal ).prop('value', fieldVal);
}

/**
 * 更新表單記錄經緯度欄位值
 * */
function _map_update_latlng(latLng) {
  // console.log( latLng );
  $("#latitude").attr('value', latLng.lat() );
  $("#longitude").attr('value', latLng.lng() );
}

function _map_display_streetview()
{
    $(document)
    .on("click", ".opt-streetview", function(){
        crawldataId=$(this).data('itemid');
        lat=$(this).data('lat');
        lng=$(this).data('lng');

        // 主圖切換
        _map_center_withlatlng(lat, lng, 16);
        // 街景切換
        latLng=new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
        streetViewService.getPanoramaByLocation( latLng , 100, function (streetViewPanoramaData, status) {
            pano = gMapObj.getStreetView();
            if (status === google.maps.StreetViewStatus.OK) {
                // 原地圖直接改成街景
                pano.setPosition( { lat: latLng.lat(), lng: latLng.lng() } );
                pano.setVisible(true);
                $("#mapStreetView").show();
                google.maps.event.trigger(gMapPanorama, "resize");
                // 顯示另一個地圖區塊並將街景物件指定為原地圖來源
                gMapPanorama.setCenter(latLng, 17);
                gMapPanorama.setStreetView(pano);
                gMapPanorama.setVisible(true);
                return ;
            }else{
                $("#mapStreetView").hide();
                console.log('無法載入該筆街景！');
            }
        });
        // console.log(latLng);

        return false;
    })
}

/**
 * 組合表單個別地址欄位功能
 * */
function _map_combine_address( addresses ) {
  var res, newVal = new Array();
  $.each(addresses, function(inx, elm){
    prefix = '';postfix = '';
    if( elm == '#addressLane' && $(elm).val() != ""){
      postfix = '巷';
    }else if( elm == '#addressAlley' && $(elm).val() != ""){
      postfix = '弄';
    }else if( elm == '#addressNo' && $(elm).val() != "" ){
      postfix = '號';
    }else if( elm == '#addressNoEx' && $(elm).val() != "" ){
      prefix = '之';
    }else if( elm == '#addressFloor' && $(elm).val() != "" ){
      postfix = '樓';
    }else if( elm == '#addressFloorEx' && $(elm).val() != "" ){
      prefix = '之';
    }

    newVal[inx] = prefix+$(elm).val()+postfix;
    res=newVal.join('');
  });

  return res;
}

/**
 * 儲存地標、地址、經緯度等欄位到geomarkers
 * */
function _map_save_geomarker( URL ) {
  $("#mapsaveBtn").click(function(){
    markdata = $("#GeomarkersForm").find("input").serialize();
    $.ajax({
      url: URL,
      type: 'POST',
      data: markdata,
      dataType: 'json',
      beforeSend: function() {
        $("#mapsaveBtn").attr('disabled',true);
      },
      success: function( resp ) {
        if( resp.res ){
          // $("#GeomarkersForm").remove();
          // 設定頁面上geomarkId
          $("#geomarkersId").val(resp.id).prop('value', resp.id);
        }
        $(resp.display).pkalert(resp);
      },
      complete: function() {
        if( $("#mapsaveBtn").length ){
          $("#mapsaveBtn").attr('disabled',false);
        }
      }
    });
  });
}


/**
 * 指定縣市或單一行政區載入該區內所有座標點儲存到GeoMarker變數
 * (使用GeoJson格式處理)
 * example : 'https://storage.googleapis.com/maps-devrel/google.json'
 *
 * */
function _map_load_geoJson( LocCenter , AJAX_URL )
{
  var queryLatLng=null;

  if( LocCenter == '' ){
    LocCenter = '台北市';
  }

  // console.log( assignAddress );
  geocoder.geocode({address:LocCenter}, function(results, status){
      if (status === google.maps.GeocoderStatus.OK) {
          gMapObj.setCenter(results[0].geometry.location);
          //   gMapObj.setZoom( toZoom );
          // callback(results[0].geometry.location, results[0].formatted_address );
          /**
           * 呼叫ajax取得geoJson格式座標
           * */
          // Load GeoJSON.
          gMapObj.data.loadGeoJson(AJAX_URL);
      }
  });
}