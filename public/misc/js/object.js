
//close map window
$(".xxbtn").click(function(){
  $(".c-box").hide();
});

// maybe like
$(window).load(function() {
  $('.object-img-slider').flexslider({
    animation: "slide",
    animationLoop: false,
  	slideshow: false,
  	controlNav: false,
    itemWidth: 260,
    minItems: 2,
    maxItems: 4
  });
});

// object lightbox
lightbox.option({
  'resizeDuration': 200,
  'wrapAround': true,
  'fadeDuration':300,
  'imageFadeDuration':300,
  'albumLabel':"%1 / %2"
})

// object nav
$('#object-menu').onePageNav();

$(function(){
	var $obmemu = $('.object-nav'),
		_top = $obmemu.offset().top;
	var $win = $(window).scroll(function(){
		if($win.scrollTop() >= _top){
			if($obmemu.css('position')!='fixed'){
				$obmemu.css({
					position: 'fixed',
					top: 0,
				});
				$("#object-funcbtn").fadeIn();
			}
		}else{
			$obmemu.css({
				position: 'static'
			});
			$("#object-funcbtn").fadeOut();
		}
	});
});

(function($){
	$(window).on("load",function(){
		//object map scroll
		$(".life-map .side").mCustomScrollbar();
	});
})(jQuery);

//object loan
var selector = '[data-rangeSlider]',
		elements = document.querySelectorAll(selector);
// Example functionality to demonstrate a value feedback
function valueOutput(element) {
	var value = element.value,
			output = element.parentNode.getElementsByTagName('output')[0];
	output.innerHTML = value;
}
for (var i = elements.length - 1; i >= 0; i--) {
	valueOutput(elements[i]);
}
Array.prototype.slice.call(document.querySelectorAll('input[type="range"]')).forEach(function (el) {
	el.addEventListener('input', function (e) {
		valueOutput(e.target);
	}, false);
});
var slider = document.querySelectorAll('input[type="range"]');
rangeSlider.create(slider, {
    polyfill: true,     // Boolean, if true, custom markup will be created
    rangeClass: 'rangeSlider',
    disabledClass: 'rangeSlider--disabled',
    fillClass: 'rangeSlider__fill',
    bufferClass: 'rangeSlider__buffer',
    handleClass: 'rangeSlider__handle',
    startEvent: ['mousedown', 'touchstart', 'pointerdown'],
    moveEvent: ['mousemove', 'touchmove', 'pointermove'],
    endEvent: ['mouseup', 'touchend', 'pointerup'],
    min: null,          // Number , 0
    max: null,          // Number, 100
    step: null,         // Number, 1
    value: null,        // Number, center of slider
    buffer: null,       // Number, in percent, 0 by default
    stick: null,        // [Number stickTo, Number stickRadius] : use it if handle should stick to stickTo-th value in stickRadius
    borderRadius: 10,    // Number, if you use buffer + border-radius in css for looks good,
//    onInit: function () {
//        console.info('onInit')
//    },
    onSlideStart: function (position, value) {
        console.info('onSlideStart', 'position: ' + position, 'value: ' + value);
    },
    onSlide: function (position, value) {
        console.log('onSlide', 'position: ' + position, 'value: ' + value);
    },
    onSlideEnd: function (position, value) {
        console.warn('onSlideEnd', 'position: ' + position, 'value: ' + value);
    }
});
var giveMeSomeEvents = true; // or false
//slider.rangeSlider.update({min : 0, max : 20, step : 0.5, value : 1.5, buffer : 70}, giveMeSomeEvents);

