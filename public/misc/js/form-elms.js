function _depend_city_withdistrict(citySelect, initial) {
    targetObj = $( citySelect.data('target') );
    city = citySelect.find(":selected").text();

    if( targetObj.length ){
        targetObj.removeClass('hide');
        if( !initial ){
            targetObj.find("option").removeAttr("selected")
        }
        targetObj.find("option").each(function(){
            if( $(this).data('city') != null ){
                if($.trim($(this).data('city'))==$.trim(city)){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            }
        });
    }
}

$(function(){
    // 切換縣市地區、職業
    $(document).on("change", ".depend-city", function(){
        _depend_city_withdistrict( $(this) );
    });
});