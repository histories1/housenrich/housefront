function _depend_city_withdistrict(citySelect, initial) {
    targetObj = $( citySelect.data('target') );
    city = citySelect.find(":checked").text();

    if( targetObj.length ){
        targetObj.removeClass('hide');
        if( !initial ){
            targetObj.find("option").removeAttr("selected")
        }
        targetObj.find("option").each(function(){
            if( $(this).data('city') != null ){
                if($.trim($(this).data('city'))==$.trim(city)){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            }
        });
    }
}

// 切換縣市地區、職業
$(document)
.on("change", ".depend-city", function(e){
    e.preventDefault();
    _depend_city_withdistrict( $(this) );
})
.on("click", ".btn-ajaxsubmit", function(e){
    e.preventDefault();

    selfBtn=$(this);

    $.ajax({
        url : selfBtn.parents('form').attr('action'),
        type : 'POST',
        data : selfBtn.parents('form').serialize(),
        dataType : 'json',
        beforeSend: function() {
            selfBtn.hide();
        },
        success: function( resp ){
            alert(resp.msg);
        },
        complete: function() {
            selfBtn.show();
        }
    });
});
// .on("click", ":checkbox[name^='mrt']", function(e){
//     e.preventDefault();
//     if( $(this).is(":checked") ){
//         $("div.mrtline."+this.id).removeClass('hide').next(".dash-line").removeClass('hide');
//     }else{
//         $("div.mrtline."+this.id).addClass('hide').next(".dash-line").addClass('hide');
//     }
// });
