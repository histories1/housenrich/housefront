//search-item btn
$('.search-option').each(function() {
    var $dropdown = $(this);

    $("a.option-btn", $dropdown).click(function(e) {
      e.preventDefault();
      $div = $("div.option-panel", $dropdown);
      $div.toggle();
      $("div.option-panel").not($div).hide();
      return false;
    });

});

// img slideshow
var swiper = new Swiper('.swiper-container', {
	pagination: '.swiper-pagination',
	paginationClickable: true,
	nextButton: '.swiper-button-next',
	prevButton: '.swiper-button-prev',
	spaceBetween: 30
});


// ----------combo select
$('.forcombo select')
.comboSelect()

/**
* on Change
*/

$('.js-select').change(function(e, v){
  $('.idx').text(e.target.selectedIndex)
  $('.val').text(e.target.value)
})

/**
* Open select
*/

$('.js-select-open').click(function(e){
  $('.js-select').focus()
  e.preventDefault();
})

/**
* Open select
*/

$('.js-select-close').click(function(e){
  $('.js-select').trigger('comboselect:close')
  e.preventDefault();
})


